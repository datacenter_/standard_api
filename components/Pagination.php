<?php
namespace app\components;

use yii\base\BaseObject;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Pagination
 * @package app\components
 */
class Pagination extends Model
{
    /**
     * 总数据量
     * @var int
     */
    public $total;

    /**
     * 总页数
     * @var float
     */
    public $totalPage;

    /**
     * @var int 开始索引
     */
    private $limit;

    /**
     * @var int 偏移量
     */
    private $offset;

    /**
     * @var int 页码
     */
    public $page;

    /**
     * @var int 分页大小
     */
    public $pageSize;

    /**
     * @var \yii\data\Pagination  分页对象
     */
    private $pagination;

    /**
     * 结果集
     * @var array
     */
    public $list;

    /**
     * ActionQuery对象
     * @var ActiveQuery
     */
    public $activeQuery;

    public function __construct(array $options = [])
    {
        $this->pagination = new \yii\data\Pagination($options);
        $this->page = $this->pagination->page+1;
        $this->pageSize = $this->pagination->pageSize;
        $this->offset = $this->pagination->offset;
        $this->limit = $this->pagination->limit;
        $this->total = $this->pagination->totalCount;
        $this->totalPage = ceil($this->total/$this->pageSize);
    }

    /**
     * 数据为空
     * @return bool
     */
    public function isEmpty()
    {
        return count($this->getList()) <= 0 ? true : false;
    }

    /**
     * 数据不为空
     * @return bool
     */
    public function notEmpty()
    {
        return !$this->isEmpty();
    }

    /**
     * 分页列表数据
     * @param bool $asArray
     * @return array|\ArrayAccess|ActiveRecord[]
     */
    public function getList($asArray = false)
    {
        if (is_null($this->list)) {
            if ($this->activeQuery instanceof ActiveQuery) {
                if ($asArray) {
                    $this->activeQuery->asArray();
                }

                return $this->activeQuery->all();
            }
        }

        return $this->list;
    }

    /**
     * @param ActiveQuery $activeQuery
     */
    public function setActiveQuery(ActiveQuery $activeQuery)
    {
        $this->activeQuery = $activeQuery;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

}