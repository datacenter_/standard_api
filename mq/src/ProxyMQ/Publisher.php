<?php
namespace WBY\MQ\SDK\ProxyMQ;

use WBY\MQ\SDK\AbstractPublisher;

/**
 * @deprecated
 * 针对PHP生产JAVA消费的特殊情况使用，由消费方自主选择消息的存储
 * Class PublisherProxy
 * @package WBY\MQ\SDK\RocketMQ
 */
class Publisher extends AbstractPublisher
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * 发布消息
     * @param Message $message
     * @return array
     */
    protected function publishMessage($message)
    {
        $params = [
            "messageBody" => $message->getMessageBody(),
            'key' => $message->getKey()
        ];

        return $this->client->sendRequest($params);
    }
}