<?php
namespace WBY\MQ\SDK\ProxyMQ;

use GuzzleHttp\Psr7\Request;

/**
 * @deprecated
 * Class Client
 * @package WBY\MQ\SDK\ProxyMQ
 */
class Client
{
    private $client;

    private $requestTimeout;

    private $connectTimeout;

    private $endPoint;

    /**
     * Client constructor.
     * @param $endPoint
     * @param $requestTimeout
     * @param $connectTimeout
     */
    public function __construct($endPoint, $requestTimeout = 0, $connectTimeout = 60)
    {
        $this->client = new \GuzzleHttp\Client();

        $this->endPoint = $endPoint;

        $this->requestTimeout = $requestTimeout;

        $this->connectTimeout = $connectTimeout;
    }

    public function sendRequest($params)
    {
        $promise = $this->sendRequestAsync($params);

        return $promise->wait();
    }

    public function sendrequestAsync($params)
    {
        $promise = $this->sendRequestAsyncInternal($params);
        $response = new Response();
        return new ProxyPromise($promise, $response);
    }

    private function sendRequestAsyncInternal($params)
    {
        $request = new Request("POST", $this->endPoint, ["Content-Type" => "application/json; charset=UTF-8"]);
        $parameters['timeout'] = $this->requestTimeout;
        $parameters['connect_timeout'] = $this->connectTimeout;
        $parameters['body'] = json_encode($params);

        return $this->client->sendAsync($request, $parameters);
    }
}