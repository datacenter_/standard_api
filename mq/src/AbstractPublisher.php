<?php
namespace WBY\MQ\SDK;

/**
 * Class AbstractProducer
 * 发布消息
 * @package WBY\MQ\SDK
 */
abstract class AbstractPublisher
{
    /**
     * 发布入口
     * 控制流程
     * @param array $messages
     * @param null|int $startDeliverTime 单位:秒 投递时间
     * @return array
     * @throws \Exception
     */
    public function produce(array $messages, $startDeliverTime = null)
    {
        $result = [];
        try {
            foreach ($messages as $message) {
                $result[] = $this->publishMessage($message, $startDeliverTime);
            }

            return $result;
        } catch (\Exception $e) {
            $this->handleException($e);
        } finally {
            $this->afterPublish($result);
        }
    }

    /**
     * 发布消息
     * @param string $message
     */
    protected abstract function publishMessage($message);

    /**
     * 异常处理
     * @param \Exception $e
     * @return mixed
     * @throws \Exception
     */
    protected function handleException(\Exception $e)
    {
        throw $e;
    }

    /**
     * 发布后处理逻辑
     * @param mixed $result
     * @return mixed
     */
    protected function afterPublish($result){}

}