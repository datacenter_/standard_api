<?php
namespace WBY\MQ\SDK;

use app\modules\common\models\CommonLog;
use app\modules\common\services\CommonLogService;
use MQ\Exception\AckMessageException;
use WBY\MQ\SDK\Interfaces\DeamonInterface;

/**
 * 订阅者消费消息
 * Class AbstractConsumer
 * @package WBY\MQ\SDK
 */
abstract class AbstractSubscriber implements DeamonInterface
{
    /**
     * 消费者
     * 客户端提供的消费接口
     * @var
     */
    protected $consumer;

    /**
     * 消费消息
     * @return array
     */
    protected abstract function consumeMessage();

    /**
     * 消费消息的业务逻辑
     * @param \MQ\Model\Message $message 消息数组对象
     * @return mixed
     */
    protected abstract function businessProcess($message);

    /**
     * 消费成功后回执消息已消费
     * @param array $receiptHandles ACK传参
     * @return mixed
     */
    protected abstract function ackMessage(array $receiptHandles);

    /**
     * 消费时异常处理
     * @return mixed
     * @param AckMessageException $e
     * @throws \Exception
     */
    protected function handleAckException(AckMessageException $e)
    {
        CommonLogService::recordException($e);
    }

    /**
     * 处理异常
     * @param \Excetpion $e
     */
    protected function handleExcetpion(\Exception $e) {
        CommonLogService::recordException($e);
    }

}
