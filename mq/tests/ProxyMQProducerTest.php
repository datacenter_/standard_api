<?php
namespace WBY\MQ\SDK\Test;

use PHPUnit\Framework\TestCase;
use WBY\MQ\SDK\MQClientFactory;
use WBY\MQ\SDK\ProxyMQ\Publisher;
use WBY\MQ\SDK\ProxyMQ\Message;

/**
 * @deprecated
 * Class ProxyMQProducerTest
 * @package WBY\MQ\SDK\Test
 */
class ProxyMQProducerTest extends TestCase
{
    public function testPublisher()
    {
        $client = MQClientFactory::getProxyMQClient("http://localhost:7777/v1/auth/sign-in");
        $publisher = new Publisher($client);
        $message = new Message("aa", "bbb");
        $result = $publisher->produce([$message]);

        $this->assertNotEmpty($result);
        $r = $result[0];
        $this->assertNotEmpty($r);
        $this->assertArrayHasKey('code', $r);
        $this->assertArrayHasKey('result', $r);
        $this->assertArrayHasKey('content', $r);
//        $this->assertEquals(200, $r['code']);
//        $this->assertEquals(1000, $r['code']);
    }
}