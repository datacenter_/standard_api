<?php
define('YII_ENV', 'test');
defined('YII_DEBUG') or define('YII_DEBUG', true);

$loader=require __DIR__ .'/../vendor/autoload.php';
require __DIR__ . '/../helpers/functions.php';
require __DIR__ . '/../env.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
//$loader->setPsr4("tests", __DIR__);
//print_r(__DIR__);
$config = require __DIR__ . '/../config/console.php';

(new yii\web\Application($config)) ;
