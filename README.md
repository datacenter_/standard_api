##### 环境要求
 - 通过环境变量{RUNTIME_ENVIROMENT}区分环境，通过此变量自动加载.env.{RUNTIME_ENVIROMENT}文件中配置
   - 开发环境nginx配置环境变量(可通过Dockerfile注入运行时环境变量) RUNTIME_ENVIROMENT dev
   - 如果未定义默认为dev
 ```
 开发环境:
 fastcgi_param  RUNTIME_ENVIROMENT dev;
 
 生产环境:
 fastcgi_param  RUNTIME_ENVIROMENT prd;
 
 ```
 - 容器内nginx启动用户(nginx用户)需要/var/tmp/nginx读写权限
 
 ##### 项目约定
  - admin和mini模块分别处理后台和小程序的路由、用户授权鉴权，分别对应不同的用户体系
  - common模块为字典通用模块不做权限限制
  - 非以上3个模块的其他模块models可相互引用，比如联表查询等；services可供以上3个模块调用，相互之间不可相互调用
  - forms下为表单验证和请求数据处理功能，forms->validate()验证通过则默认所有数据没有问题，service层不再做数据校验
 