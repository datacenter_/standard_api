<?php


namespace app\commands\crontab;


interface CrontabInterface
{
    public function run($args);
}