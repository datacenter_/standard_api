<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace app\commands;

use app\modules\admin\models\Admin;
use app\modules\common\models\ApiSetmeal;
use app\modules\common\models\ApiSetmealGeme;
use app\modules\common\models\ApiUserSetMeal;
use app\rest\exceptions\BusinessException;
use yii\console\Controller;
use yii\console\ExitCode;

class SetMealController extends Controller
{
    // 刷新正常数量Api
    public function actionGrabSetMeal()
    {
        $userIds = ApiUserSetMeal::find()->select(['user_id'])->asArray()->all();
        $nowTime = time();
        foreach ($userIds as $uidInfo) {
            $uid = $uidInfo['user_id'];
            print("正在查询用户Id为：$uid...")."\n";
            $userSetMealModel = ApiUserSetMeal::find()->where(['user_id' => $uid])->one();
            if (empty($userSetMealModel)) {
                $userSetMealModel = new ApiUserSetMeal();
                $userSetMealModel->setAttribute('user_id',$uid);
            } else {

            }
            // 套餐
            print("正在查询用户Id为：{$uid}，的套餐.")."\n";
            $userSetMeals = ApiSetmeal::find()->select(['id','opening_at','expiration_at'])
                ->where(['user_id' => $uid])->andWhere(['>=', "expiration_at", date("Y-m-d H:i:s",$nowTime)])
                ->orderBy('opening_at')->limit(2)->asArray()->all();
//            if (!empty($userSetMeals)) {
                // 当前套餐
                $userCurrentSetMeals = [];
                // 未来套餐
                $userRenewSetMeals = [];
                foreach ($userSetMeals as $k => $smInfo) {
                    if (!$k) {
                        $openingTime = strtotime($smInfo['opening_at']);
                        // 判断是否未来 或者 当前
                        if ($openingTime > $nowTime) {
                            // 未来套餐
                            $userRenewSetMeals = $smInfo;
                            break;
                        } else {
                            // 当前套餐
                            $userCurrentSetMeals = $smInfo;
                        }
                    } else {
                        // 未来套餐
                        $userRenewSetMeals = $smInfo;
                    }
                }
                // 当前套餐
                if (!empty($userCurrentSetMeals)) {
                    print("正在刷新当前套餐1.") . "\n";
                    $userSetMealModel->setAttribute('set_meal_id', $userCurrentSetMeals['id']);
                } else {
                    print("正在刷新当前套餐2.") . "\n";
                    $userSetMealModel->setAttribute('set_meal_id', null);
                }
                // 未来套餐
                if (!empty($userRenewSetMeals)) {
                    print("正在刷新未来套餐1.") . "\n";
                    $userSetMealModel->setAttribute('renew_set_meal_id', $userRenewSetMeals['id']);
                } else {
                    print("正在刷新未来套餐2.") . "\n";
                    $userSetMealModel->setAttribute('renew_set_meal_id', null);
                }
                if (!$userSetMealModel->save()) {
                    throw new BusinessException($userSetMealModel->getErrors(), "操作失败,请检查提交或联系管理员");
                }
//            }
            // 套餐游戏
            print("正在查询用户Id为：{$uid}，的套餐游戏.")."\n";
            $userSetMealGames = ApiSetmealGeme::find()->select(['id'])
                ->where(['user_id' => $uid])->andWhere(['in','data_level',[1,2]])
                ->andWhere(['and',['<=', "opening_at", date("Y-m-d H:i:s",$nowTime)],['>=', "expiration_at", date("Y-m-d H:i:s",$nowTime)]])
                ->asArray()->all();
//            if (!empty($userSetMealGames)) {
                print("正在刷新当前套餐游戏.")."\n";
                $userSetMealGameIds = array_column($userSetMealGames, 'id');
                if (!empty($userSetMealGameIds)) {
                    $userSetMealGameIdsJson = json_encode($userSetMealGameIds);
                } else {
                    $userSetMealGameIdsJson = null;
                }
                if (empty($userSetMealModel)) {
                    $userSetMealModel = new ApiUserSetMeal();
                    $userSetMealModel->setAttribute('user_id',$uid);
                }
                $userSetMealModel->setAttribute('set_meal_game_ids',$userSetMealGameIdsJson);
                if (!$userSetMealModel->save()) {
                    throw new BusinessException($userSetMealModel->getErrors(), "操作失败,请检查提交或联系管理员");
                }
//            }
            print("\n");
        }


//        print_r($setMeals);

        print("刷新完成")."\n";
        return ExitCode::OK;
    }
}
