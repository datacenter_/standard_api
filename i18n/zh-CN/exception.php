<?php
use app\rest\Code;

return [
    // 操作异常
    Code::$exceptionDefaultMessages[Code::BUSINESS_EXCEPTION] => 'Operation exception',
    // 没有操作权限
    Code::$exceptionDefaultMessages[Code::UNAUTHORIZATION_EXCEPTION] => "No operation authority",
    // 用户名密码错误
    Code::$exceptionDefaultMessages[Code::AUTHENTICATION_FAILED_EXCEPTION] => 'Incorrect email or password',
    // 登录超时
    Code::$exceptionDefaultMessages[Code::NO_LOGGED_IN_EXCEPTION] => "Login timeout",
    // 参数不合法
    Code::$exceptionDefaultMessages[Code::REQUEST_PARAMS_UNVALIDATED_EXCEPTION] => 'Illegal parameter',
    // 没有操作权限
    Code::$exceptionDefaultMessages[Code::NO_OPERATION_PERMISSION_EXCEPTION] => "No operation authority",
    // 操作失败
    Code::$exceptionDefaultMessages[Code::RUNTIME_EXCEPTION] => 'Operation failed',
    // 没有找到数据
    Code::$exceptionDefaultMessages[Code::ENTITY_NOT_EXIST_EXCEPTION] => 'Data not found',
    // 远程调用异常
    Code::$exceptionDefaultMessages[Code::REMOTE_CALL_EXCEPTION] => 'Remote call exception',
    // 操作失败
    'default exception message' => 'Operation failed',
];