<?php
return [
    'double input passwords compared failed' => '两次输入密码不相同',
    '{attribute} is invalid' => '{attribute}格式不正确',
    '{attribute} only chinese and word, max length {max}' => '{attribute}仅可输入汉字和英文字母, 最多可输入{max}个字',
    '{attribute} only chinese and word, min length {min}' => '{attribute}仅可输入汉字和英文字母, 最少可输入{min}个字',
    '{attribute} only chinese and word, min length {min}, max length {max}' => '{attribute}仅可输入汉字和英文字母, 可输入{min}-{max}个字',
    '{attribute} only chinese and word' => '{attribute}仅可输入汉字和英文字母',
    '{attribute} only accept word and number' => '{attribute}仅可输入字母和数字，请重新输入',
    'mobile phone is invalid' => '手机号格式错误，请重新输入',
    'register failed' => '注册失败',
    "success" => "success",
];