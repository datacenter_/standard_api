<?php
namespace app\validators;


use yii\validators\Validator;

/**
 * 手机号验证
 * Class MobilPhoneValidator
 * @package app\validators
 */
class MobilePhoneValidator extends Validator
{
    /**
     * @var bool
     */
    public $strict = false;

    public function init()
    {
        parent::init();

        if ($this->message === null) {
            $this->message = \Yii::t('app', 'mobile phone is invalid');
        }
    }

    /**
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if ($this->strict) {
            $pattern = '/^1[3,4,5,7,8,9]{1}[\d]{9}$/';
        } else {
            $pattern = '/^1\d{10}$/';
        }

        if (!preg_match($pattern, $model->{$attribute})) {
            $this->addError($model, $attribute, $this->message);
        }
    }
}