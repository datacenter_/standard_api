<?php

//爬虫数据源
return [
    'class' => \yii\db\Connection::class,
    'dsn' => sprintf("mysql:host=%s;port=%s;dbname=%s", env('STREAM_DB_HOST'), env('STREAM_DB_PORT'),env('STREAM_DB_DATABASE')),
    'username' => env('STREAM_DB_USERNAME'),
    'password' => env('STREAM_DB_PASSWORD'),
    'charset' => env('STREAM_DB_CHARSET'),
];
