<?php
return [
    'class' => \yii\db\Connection::class,
    'dsn' => sprintf("mysql:host=%s;port=%s;dbname=%s", env("ELS_DB_HOST"), 3306, env('ELS_DB_DATABASE')),
    'username' => env("ELS_DB_USERNAME"),
    'password' => env("ELS_DB_PASSWORD"),
    'charset' => 'utf8mb4',
];
