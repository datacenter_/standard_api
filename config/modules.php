<?php

use yii\helpers\ArrayHelper;

\Yii::$classMap['yii\base\ArrayableTrait'] = __DIR__ . '/../helpers/ArrayableTrait.php';
\Yii::$classMap['yii\base\ArrayHelper'] = __DIR__ . '/../helpers/ArrayHelper.php';

$modules = [
    __DIR__ . '/../modules/common/config.php',
    __DIR__ . '/../modules/admin/config.php',
    __DIR__ . '/../modules/v1/config.php', //通用路由放最后匹配
    __DIR__ . '/../modules/api/config.php', //通用路由放最后匹配
];

$config = [];
foreach ($modules as $module) {
    if (file_exists($module)) {
        $config = ArrayHelper::merge($config, require $module);
    }
}

return $config;
