FROM registry.cn-beijing.aliyuncs.com/bcbase/backend_base:1.7.0
COPY . /var/www/gamecenter
## init auth
RUN chmod -R 777 /var/www/gamecenter/public
RUN chmod -R 777 /var/www/gamecenter/runtime
RUN apk update && \
    apk upgrade && \
    apk add --no-cache bash git openssh
RUN cd /var/www/gamecenter \
&& rm -f composer.lock \
&& composer install
COPY .dockerconfig/nginx_conf/gamecenter.conf /etc/nginx/conf.d/gamecenter.conf
RUN rm -f /etc/nginx/conf.d/default.conf
EXPOSE 80
