<?php
namespace app\constants;

/**
 * 常量类
 * Class Contract
 * @package app\constants
 */
class Contract
{
    /**
     * 默认免费版 group_id 为4 新用户版
     */
    const NEW_USER_GROUP_ADMISSION = 5;

    /**
     * 是、真、存在、启用、未删除等
     */
    const COMMON_DB_YES = 1;

    /**
     * 否、假、不存在、禁用、删除等
     */
    const COMMON_DB_NO = -1;

    /**
     * 管理员Form验证--创建场景 系统设定的场景,需要验证数据
     */
    const FORM_SCENARIO_ADMIN_CREATE = 'create';

    /**
     * 管理员Form验证--修改场景 系统设定的场景，需要验证数据
     */
    const FORM_SCENARIO_ADMIN_UPDATE = 'update';

    /**
     * 用户Form验证--创建场景 系统设定的场景，需要验证数据
     */
    const FORM_SCENARIO_OWNER_CREATE = 'create';

    /**
     * 用户Form验证--修改场景 系统设定的场景，需要验证数据
     */
    const FORM_SCENARIO_OWNER_UPDATE = 'update';

    /**
     * 通用Form验证--创建场景 系统设定的场景,需要验证数据
     */
    const FORM_SCENARIO_CREATE = 'create';

    /**
     * 通用Form验证--修改场景 系统设定的场景，需要验证数据
     */
    const FORM_SCENARIO_UPDATE = 'update';

    /**
     * 修改时都需要验证数据的场景
     * @var array
     */
    public static $allUpdateScenarios = [
        self::FORM_SCENARIO_ADMIN_UPDATE,
        self::FORM_SCENARIO_OWNER_UPDATE,
        self::FORM_SCENARIO_UPDATE
        ];

    /**
     * 创建时都需要验证数据的场景
     * @var array
     */
    public static $allCreateScenarios = [
        self::FORM_SCENARIO_ADMIN_CREATE,
        self::FORM_SCENARIO_OWNER_CREATE,
        self::FORM_SCENARIO_CREATE
    ];

    /**
     * 用户所有场景
     * @var array
     */
    public static $allOwnerScenarios = [
        self::FORM_SCENARIO_OWNER_UPDATE,
        self::FORM_SCENARIO_OWNER_CREATE
    ];

    /**
     * 管理员所有场景
     * @var array
     */
    public static $allAdminScenarios = [
        self::FORM_SCENARIO_ADMIN_UPDATE,
        self::FORM_SCENARIO_ADMIN_CREATE
    ];
}