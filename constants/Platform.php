<?php
namespace app\constants;

/**
 * 自媒体平台
 * Class Platform
 * @package app\constants
 */
class Platform
{
    /**
     * 新浪微博
     */
    const WEIBO = 1;

    /**
     * 微信
     */
    const WEIXIN  = 9;

    /**
     * 抖音
     */
    const DOUYIN = 115;

    /**
     * 快手
     */
    const KUAISHOU = 103;

    /**
     * 小红书
     */
    const XIAOHONGSHU = 93;

    /**
     * 秒拍
     */
    const MIAOPAI = 24;

    /**
     * 美拍
     */
    const MEIPAI = 25;

    /**
     * B站
     */
    const BILIBILI = 110;

    /**
     * 火山小视频
     */
    const HUOSHAN = 116;

    /**
     * 西瓜视频
     */
    const XIGUA  = 118;

    /**
     * 平台名字
     * @var array
     */
    public static $platformLabels = [
        self::WEIXIN => '微信公众号',
        self::WEIBO => '新浪微博',
        self::DOUYIN => '抖音',
        self::KUAISHOU => '快手',
        self::XIAOHONGSHU => '小红书',
        self::BILIBILI => 'B站',
        self::MEIPAI => '美拍',
        self::MIAOPAI => '秒拍',
        self::XIGUA => '西瓜视频',
        self::HUOSHAN => '火山小视频',
    ];

}