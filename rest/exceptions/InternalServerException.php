<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 内部服务器错误
 * Class InternalServerException
 * @package app\rest\exceptions
 */
class InternalServerException extends RestException
{
    public function getErrCode(): int
    {
        return Code::INTERNAL_SERVER_CODE;
    }
}