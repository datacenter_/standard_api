<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 错误的请求
 * Class BadRequestException
 * @package app\rest\exceptions
 */
class BadRequestException extends RestException
{
    public function getErrCode(): int
    {
        return Code::BADREQUEST_CODE;
    }
}