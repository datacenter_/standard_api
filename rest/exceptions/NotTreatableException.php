<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 不可处理的实体
 * Class NotTreatableException
 * @package app\rest\exceptions
 */
class NotTreatableException extends RestException
{
    public function getErrCode(): int
    {
        return Code::NOT_TREATABLE_CODE;
    }
}