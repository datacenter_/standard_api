<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 未找到
 * Class NotFoundException
 * @package app\rest\exceptions
 */
class NotFoundException extends RestException
{
    public function getErrCode(): int
    {
        return Code::NOT_FOUND_CODE;
    }
}