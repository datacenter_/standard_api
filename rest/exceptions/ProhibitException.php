<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 禁止的
 * Class ProhibitException
 * @package app\rest\exceptions
 */
class ProhibitException extends RestException
{
    public function getErrCode(): int
    {
        return Code::PROHIBIT_CODE;
    }
}