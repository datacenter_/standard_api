<?php
namespace app\rest\exceptions;

use app\rest\Code;

/**
 * 未经授权
 * Class UnauthorizedException
 * @package app\rest\exceptions
 */
class UnauthorizedException extends RestException
{
    public function getErrCode(): int
    {
        return Code::UNAUTHORIZED_CODE;
    }
}