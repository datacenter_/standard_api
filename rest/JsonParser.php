<?php
namespace app\rest;

use app\rest\exceptions\RequestParamsUnvalidatedException;
use yii\base\InvalidArgumentException;
use yii\helpers\Json;
use yii\web\RequestParserInterface;

class JsonParser implements RequestParserInterface
{
    /**
     * @var bool whether to return objects in terms of associative arrays.
     */
    public $asArray = true;
    /**
     * @var bool whether to throw a [[BadRequestHttpException]] if the body is invalid json
     */
    public $throwException = true;

    /**
     * Parses a HTTP request body.
     * @param string $rawBody the raw HTTP request body.
     * @param string $contentType the content type specified for the request body.
     * @return array parameters parsed from the request body
     * @throws RequestParamsUnvalidatedException
     */
    public function parse($rawBody, $contentType)
    {
        try {
            $parameters = Json::decode($rawBody, $this->asArray);
            return $parameters === null ? [] : $parameters;
        } catch (InvalidArgumentException $e) {
            \Yii::error($e->getMessage());
            \Yii::error((string)$rawBody);

            if ($this->throwException) {
                throw new RequestParamsUnvalidatedException([$rawBody]);
            }

            return [];
        }
    }

}