<?php
namespace yii\base;

use yii\helpers\BaseArrayHelper;

class ArrayHelper extends BaseArrayHelper
{
    /**
     * @inheritdoc
     */
    public static function toArray($object, $properties = [], $recursive = true, $expands = [])
    {
        if (is_array($object)) {
            if ($recursive) {
                foreach ($object as $key => $value) {
                    if ((is_array($value) || is_object($value))) {
                        if (is_int($key)) {
                            $expand = $expands;
                        } elseif (isset($expands[$key])) {
                            $expand = $expands[$key];
                        } else {
                            $expand = [];
                        }
                        $object[$key] = static::toArray($value, [], true, $expand);
                    }
                }
            }

            return $object;
        } elseif (is_object($object)) {
            if (!empty($properties)) {
                $className = get_class($object);
                if (!empty($properties[$className])) {
                    $result = [];
                    foreach ($properties[$className] as $key => $name) {
                        if (is_int($key)) {
                            $result[$name] = $object->$name;
                        } else {
                            $result[$key] = static::getValue($object, $name);
                        }
                    }

                    return $recursive ? static::toArray($result, $properties, true, $expands) : $result;
                }
            }

            if ($object instanceof Arrayable) {
                $result = $object->toArray($expands, $expands, $recursive);
            } else {
                $result = [];
                foreach ($object as $key => $value) {
                    $result[$key] = $value;
                }
            }

            return $recursive ? static::toArray($result, [], true, $expands) : $result;
        } else {
            return [$object];
        }
    }

    public static function getValues($array, $keys = [])
    {
        $result = [];

        foreach ($keys as $key) {
            if (array_key_exists($key, $array)) {
                $result[$key] = $array[$key];
            }
        }

        return $result;
    }

    /**
     * @param array $fromArray
     * @param array $targetArray
     * @param array $keys
     * @param bool $replace
     */
    public static function copySpecificKeys(array $fromArray = [], array &$targetArray = [], array $keys = [], $replace = true)
    {
        foreach ($keys as $key) {
            if (isset($fromArray[$key])) {
                if ($replace || !isset($targetArray[$key])) {
                    $targetArray[$key] = $fromArray[$key];
                }
            }
        }
    }
}
