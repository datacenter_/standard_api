<?php

namespace app\modules\file\controllers;

use app\controllers\RestController;
use app\modules\file\services\UploadService;

/**
 * Default controller for the `file` module
 */
class FileController extends RestController
{
    public function actionUpload()
    {
        $scr = $_FILES['file']['tmp_name'];
        $ext = substr($_FILES['file']['name'], strrpos($_FILES['file']['name'], '.') + 1); // 上传文件后缀
        $dst = 'type_logo/' . md5(time()) . '/'  . md5($_FILES['file']['name']).'.'.$ext;
        $url = UploadService::upload($dst,$scr);
        return ["url"=>$url];
    }
}
