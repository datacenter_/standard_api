<?php
/**
 *
 */

namespace app\modules\file\services;

use OSS\OssClient;
use OSS\Core\OssException;

class UploadService
{
    public static function upload($dst, $getFile)
    {
        $config = array(
            'KeyId' => env('ONS_accessId'),
            'KeySecret' => env('ONS_accessKey'),
            'Endpoint' => env('OSS_Endpoint'),
            'Bucket' => env('OSS_Bucket'),
        );
        $ossClient = new OssClient(
            $config['KeyId'],
            $config['KeySecret'],
            $config['Endpoint']);
        #执行阿里云上传
        $result = $ossClient->uploadFile(
            $config['Bucket'],
            $dst,
            $getFile);
        #返回
        return $result["info"]["url"];
    }

    public static function uploadTo($form,$to)
    {
        $config = array(
            'KeyId' => env('ONS_accessId'),
            'KeySecret' => env('ONS_accessKey'),
            'Endpoint' => env('OSS_Endpoint'),
            'Bucket' => env('OSS_Bucket'),
        );
        $ossClient = new OssClient(
            $config['KeyId'],
            $config['KeySecret'],
            $config['Endpoint']);
        #执行阿里云上传
        $result = $ossClient->uploadFile(
            $config['Bucket'],
            $to,
            $form);
        #返回
        return $result["info"]["url"];
    }
}