<?php
namespace app\modules\statsteam\controllers;

use app\controllers\RestController;
use app\modules\statsteam\services\StatsTeamService;

class TeamsController extends RestController
{
    // 战队统计数据详情
    public function actionDetail()
    {
        $params = $this->pGet();
        $info = StatsTeamService::getTeamDetail($params);
        return $info;
    }
}
