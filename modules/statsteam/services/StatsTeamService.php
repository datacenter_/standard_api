<?php
namespace app\modules\statsteam\services;

use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\RedisBase;

class StatsTeamService
{
    public static function getTeamDetail($params)
    {
        if(!isset($params['team_id']) && !$params['team_id']){
            return [];
        }
        if(!isset($params['number_of_matches']) || !$params['number_of_matches']){
            $params['number_of_matches'] = 10;
        }
        $number_of_matches = strtolower($params['number_of_matches']);
        $index_info = "stats-v2-common-team-{$number_of_matches}";
        $index = self::issetIndexKey($index_info);
        if (!$index) {
            return [];
        }
        $conditions = [];
        $columns = [];//['match_id', 'description'];
        // 筛选
        if (isset($params['team_id']) && $params['team_id']){
            $conditions[] = ['field' => 'content.team.team_id', 'type' => '=', 'value' => $params['team_id']];
        }
        $order = ['field' => "content.begin_time", 'sort' => "desc"];
        $EsService = EsService::initConnect();
        $res = $EsService->search($index_info,$conditions, 0, 2, $columns, $order);
        $resData = [];
        if(!empty($res['list'])){
            $teamStatsInfo = $res['list'][0]['content'];
//            foreach ($res['list'] as $key=>&$team) {
                // 初始化
                $info = [];
                // 待处理的数据
//                $teamStatsInfo = $team['content'];
                // redis取出team结构
                if ($teamStatsInfo['team']['team_id']) {
                    $teamJsonRes = RedisBase::valueGet([ApiConstant::PREFIX_API,'Teams',$teamStatsInfo['team']['team_id']]);
                    if ($teamJsonRes) {
                        $teamInfo = json_decode($teamJsonRes,true);
                        // unset不需要的字段
                        unset($teamInfo['region']);
                        unset($teamInfo['world_ranking']);
                        unset($teamInfo['total_earnings']);
                        unset($teamInfo['average_player_age']);
                        unset($teamInfo['history_players']);
                        unset($teamInfo['introduction']);
                        unset($teamInfo['introduction_cn']);
                        unset($teamInfo['players']);
                        $info['team'] = $teamInfo;
                    } else {
                        $info['team'] = null;
                    }
                } else {
                    $info['team'] = null;
                }
                $info['total_number_of_matches'] = $teamStatsInfo['total_number_of_matches'];
                $info['total_number_of_battles'] = $teamStatsInfo['total_number_of_battles'];
                $info['number_of_matches'] = $teamStatsInfo['number_of_matches'];
                $info['match_stats']['matches_count'] = $teamStatsInfo['match_stats']['matches_count'];
                $info['match_stats']['matches_win_count'] = $teamStatsInfo['match_stats']['matches_win_count'];
                $info['match_stats']['matches_win_rate'] = $teamStatsInfo['match_stats']['matches_win_rate'];
                $info['battle_stats']['battles_count'] = $teamStatsInfo['battle_stats']['battles_count'];
                $info['battle_stats']['battles_win_count'] = $teamStatsInfo['battle_stats']['battles_win_count'];
                $info['battle_stats']['battles_win_rate'] = $teamStatsInfo['battle_stats']['battles_win_rate'];
                $resData = $info;
            }
//        }
        return $resData;
    }

    // 检测索引
    public static function issetIndexKey($index_info)
    {
        $client = EsService::initConnect();
        return $client->searchIndex($index_info);
    }
}
