<?php

namespace app\modules\mysqls\models;

use Yii;

class DbOnline extends \yii\db\ActiveRecord
{
    public static function getDb() {
        return Yii::$app->gy_db;
    }
}
