<?php

namespace app\modules\backstage\controllers;

use app\modules\admin\services\AuthService;
use app\modules\backstage\services\RenewAdministration;
use app\controllers\WithTokenAuthController;

use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class LoginController
 * @package app\modules\backstage\controllers
 * @property \app\modules\backstage\Module $module
 */
class RenewCustomController extends WithTokenAuthController
{
//    public function behaviors()
//    {
//        return [
//            'contentNegotiator' => [
//                'class' => ContentNegotiator::className(),
//                'formats' => [
//                    'application/json' => Response::FORMAT_JSON,
//                ],
//            ]
//        ];
//    }
    /**
     * 用户账号套餐列表（新版）
     */
    public function actionUserSetMealList()
    {
        $params = \Yii::$app->getRequest()->get();
        return RenewAdministration::getUserSetMealList($params);
    }
    /**
     * 用户账号套餐详情
     */
    public function actionUserSetMealDetail()
    {
        $params = \Yii::$app->getRequest()->get();
        return RenewAdministration::getUserSetMealDetail($params);
    }
    /**
     * 用户账号套餐Edit
     */
    public function actionUserSetMealEdit()
    {
        $params = \Yii::$app->getRequest()->post();
        return RenewAdministration::getUserSetMealEdit($params);
    }
    /**
     * 用户账号套餐历史记录
     */
    public function actionUserSetMealHistory()
    {
        $params = \Yii::$app->getRequest()->get();
        return RenewAdministration::getUserSetMealHistory($params);
    }
    /**
     * 用户账号套餐删除
     */
    public function actionUserSetMealDel()
    {
        $params = \Yii::$app->getRequest()->post();
        return RenewAdministration::getUserSetMealDel($params);
    }


    /**
     * 用户账号游戏套餐列表
     */
    public function actionUserSetMealGameList()
    {
        $params = \Yii::$app->getRequest()->get();
        return RenewAdministration::getUserSetMealGameList($params);
    }
    /**
     * 用户账号游戏套餐Edit
     */
    public function actionUserSetMealGameEdit()
    {
        $params = \Yii::$app->getRequest()->post();
        return RenewAdministration::getUserSetMealGameEdit($params);
    }
    /**
     * 用户账号游戏套餐删除
     */
    public function actionUserSetMealGameDel()
    {
        $params = \Yii::$app->getRequest()->post();
        return RenewAdministration::getUserSetMealGameDel($params);
    }
    /**
     * 用户账号游戏套餐历史记录
     */
    public function actionUserSetMealGameHistory()
    {
        $params = \Yii::$app->getRequest()->get();
        return RenewAdministration::getUserSetMealGameHistory($params);
    }

    // 地区数据
    public function actionRegions()
    {
        return RenewAdministration::getRegions();
    }

    // 赛事状态码表
    public function actionMatchStatus()
    {
        return RenewAdministration::getMatchStatus();
    }

    /**
     * 单个用户操作日志
     */
    public function actionUserOperationLog()
    {
        $params = \Yii::$app->getRequest()->get();
        return RenewAdministration::getUserOperationLog($params);
    }

    /**
     * 用户私有赛事操作日志
     */
    public function actionUserTournamentsLog()
    {
        $params = \Yii::$app->getRequest()->get();
        return RenewAdministration::getUserTournamentsLog($params);
    }

    /**
     * 商务后台-忘记密码
     */
    public function actionBusinessModifyPassword()
    {
        $attributes = \Yii::$app->getRequest()->post();
        return AuthService::setBusinessModifyPassword($attributes);
    }
}