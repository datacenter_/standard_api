<?php

namespace app\modules\backstage\controllers;

use app\modules\backstage\services\Administration;
use app\controllers\WithTokenAuthController;

use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Class LoginController
 * @package app\modules\backstage\controllers
 * @property \app\modules\backstage\Module $module
 */
class CustomController extends WithTokenAuthController
{
//    public function behaviors()
//    {
//        return [
//            'contentNegotiator' => [
//                'class' => ContentNegotiator::className(),
//                'formats' => [
//                    'application/json' => Response::FORMAT_JSON,
//                ],
//            ]
//        ];
//    }
    /**
     * 客户管理权限（账号列表）
     */
    public function actionCustomList()
    {
        $params = \Yii::$app->getRequest()->get();
        return Administration::getCustomList($params);
    }
    /**
     * 用户详情数据
     */
    public function actionUserDetail()
    {
        $params = \Yii::$app->getRequest()->get();
        return Administration::getUserDetail($params);
    }

    /**
     * 私有赛事（单个用户的私有赛事列表）
     */
    public function actionUserPrivateTournamentsList()
    {
        $params = \Yii::$app->getRequest()->get();
        return Administration::getUserPrivateTournamentsList($params);
    }
    /**
     * 私有赛事（单个用户的私有赛事删除）
     */
    public function actionUserPrivateTournamentDel()
    {
        $params = \Yii::$app->getRequest()->post();
        return Administration::getUserPrivateTournamentDel($params);
    }
    /**
     * 新增私有赛事（搜索赛事信息）
     */
    public function actionTournamentsList()
    {
        $params = \Yii::$app->getRequest()->get();
        return Administration::getTournamentsList($params);
    }
    /**
     * 新增私有赛事（某条赛事详细信息）
     */
    public function actionTournamentInfo()
    {
        $params = \Yii::$app->getRequest()->get();
        return Administration::getTournamentInfo($params);
    }
    /**
     * 修改用户私有赛事（修改用户私有赛事详细信息）
     */
    public function actionUserEditTournamentInfo()
    {
        $params = \Yii::$app->getRequest()->get();
        return Administration::getUserEditTournamentInfo($params);
    }
    /**
     * 新增/修改用户私有赛事（入库操作）
     */
    public function actionUserTournamentEdit()
    {
        $attributes = \Yii::$app->getRequest()->post();
        return Administration::putUserTournamentEdit($attributes);
    }

    /**
     * 默认权限
     */
    public function actionDefaultRole()
    {
        $params = \Yii::$app->getRequest()->get();
        return Administration::getRoleList($params);
    }
    /**
     * 用户账号套餐对应的权限（旧版）
     */
    public function actionUserRole()
    {
        $params = \Yii::$app->getRequest()->get();
        return Administration::getUserRoleList($params);
    }
    /**
     * 用户账号套餐对应的权限记录 (历史记录)
     */
    public function actionUserRoleHistory()
    {
        $params = \Yii::$app->getRequest()->get();
        return Administration::getUserRoleHistoryList($params);
    }
    /**
     * 用户账号套餐对应的权限记录 添加/修改/删除 分情况而定
     */
    public function actionUserRoleEdit()
    {
        $params = \Yii::$app->getRequest()->post();
        return Administration::putUserRoleEdit($params);
    }
    /**
     * 用户账号游戏权限
     */
    public function actionUserGameRole()
    {
        $params = \Yii::$app->getRequest()->get();
        return Administration::getUserGameRoleList($params);
    }
    /**
     * 用户账号游戏权限 (历史记录)
     */
    public function actionUserGameRoleHistory()
    {
        $params = \Yii::$app->getRequest()->get();
        return Administration::getUserGameRoleHistoryList($params);
    }
    /**
     * 用户账号游戏权限 添加/修改
     */
    public function actionUserGameRoleEdit()
    {
        $params = \Yii::$app->getRequest()->post();
        return Administration::getUserGameRoleEdit($params);
    }
    /**
     * 白名单列表
     */
    public function actionUserWhiteList()
    {
        $params = \Yii::$app->getRequest()->get();
        return Administration::getUserWhiteList($params);
    }
    /**
     * 白名单添加
     */
    public function actionUserWhiteListEdit()
    {
        $params = \Yii::$app->getRequest()->post();
        return Administration::getUserWhiteListEdit($params);
    }
    /**
     * 版本默认数据 (码表数据)
     */
    public function actionGroupCenter()
    {
        return Administration::getUserGroupList();
    }
    /**
     * 请求限制码表数据
     */
    public function actionRequestRestrictionList()
    {
        return Administration::getRequestRestrictionList();
    }
    /**
     * 数据级别码表数据
     */
    public function actionDataLevelsList()
    {
        return [
            [
                'id' => 3,
                'name' => 'Unsupported',
                'name_cn' => '不提供',
            ],
            [
                'id' => 1,
                'name' => 'Basic',
                'name_cn' => '基础',
            ],
            [
                'id' => 2,
                'name' => 'Advanced',
                'name_cn' => '高级',
            ]
        ];
//        return Administration::getDataLevelsList();
    }
    /**
     * 游戏码表数据
     */
    public function actionGamesList()
    {
        return Administration::getGamesList();
    }
}