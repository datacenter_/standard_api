<?php
return [
    'components' => [
        'urlManager' => [
            'rules' => [
                [
                    'class' => 'yii\web\GroupUrlRule',
                    'prefix' => 'api/backstage',
                    'rules' => [
                        'GET,POST platforms' => 'currency/get-platform'
                    ]
                ]
            ]
        ],
    ],
];

