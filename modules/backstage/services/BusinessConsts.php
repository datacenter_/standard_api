<?php
/**
 * super
 */

namespace app\modules\backstage\services;

class BusinessConsts
{
    const QUEUE_TYPE_ADD = "add";
    const QUEUE_TYPE_CHANGE = "change";
    const QUEUE_TYPE_UPDATE = "update";
    const CHANGE_TYPE_EQUAL = "equal";
    const CHANGE_TYPE_DELETE = "delete";
    const CHANGE_TYPE_RECOVERY = "recovery";
}