<?php
/**
 * super
 */

namespace app\modules\backstage\services;

use app\modules\common\models\ApiCountry;
use app\modules\common\models\ApiGame;
use app\modules\common\models\ApiRule;
use app\modules\common\models\ApiSetmeal;
use app\modules\common\models\ApiUserSetMeal;
use app\modules\common\models\OperationLog;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\RedisBase;
use app\modules\mysqls\models\DbOnline;
use app\modules\admin\models\Admin;
use app\modules\common\models\ApiSetmealGeme;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RequestParamsUnvalidatedException;
use yii\data\Pagination;

class RenewAdministration
{
    // 账号套餐列表（新版）
    public static function getUserSetMealList($params)
    {
        $nowDate = date("Y-m-d H:i:s",time());
        return ApiSetmeal::find()->alias('s')
            ->select(['s.id','r.request_num','r.hour','r.hour_cn','g.group_name','g.group_e_name','s.opening_at','s.expiration_at','s.created_at',
                '(case when s.is_interface=1 then "Default" when s.is_interface=2 then "Customize" else null end) as interface_name',
                '(case when s.is_interface=1 then "默认" when s.is_interface=2 then "自定义" else null end) as interface_name_cn'])
            ->leftJoin('enum_request_restriction as r', "r.id = s.request_restriction")
            ->leftJoin("api_group as g","g.id = s.group_id")
            ->where(['s.user_id'=>$params['user_id']])
            ->andWhere(['>=', "s.expiration_at", $nowDate])
            ->asArray()->all();
//        $sql=$infos->createCommand()->getRawSql();
//        echo "<pre>";
//        print_r($infos);die;
    }
    // 用户账号套餐详情（新版）
    public static function getUserSetMealDetail($params)
    {
        // $params['id'] 套餐id

        $userSetMealInfo = ApiSetmeal::find()->alias("s")
            ->select(['s.user_id','u.email','s.group_id','s.request_restriction','s.is_interface','s.opening_at','s.expiration_at','s.user_rights'])
            ->leftJoin("admin as u","u.id = s.user_id")
            ->where(['s.id'=>$params['id']])->asArray()->one();
        $userInfo = [
            'user_id' => $userSetMealInfo['user_id'],
            'email' => $userSetMealInfo['email'],
        ];
        unset($userSetMealInfo['user_id']);
        unset($userSetMealInfo['email']);
        $userRightsArray = json_decode($userSetMealInfo['user_rights'],true);
        unset($userSetMealInfo['user_rights']);

        // 所有权限数据
        $ruleArray = ApiRule::find()->select(['id', 'r_name', 'r_name_cn', 'pid', 'order'])->where(['status'=>1])->asArray()->all();
        foreach ($ruleArray as $k=>&$ruleInfo) {
            if ($ruleInfo['pid']) {
                if (in_array($ruleInfo['id'], $userRightsArray)) {
                    $ruleArray[$k]['is_tower'] = 1;
                } else {
                    $ruleArray[$k]['is_tower'] = 2;
                }
            }
        }
        $userRoleList = Administration::recursiveRule($ruleArray,0);
        $field = array_column($userRoleList, 'order');
        array_multisort($field, SORT_ASC, $userRoleList);
        return [
            'user_info' => $userInfo,
            'user_setmeal_info' => $userSetMealInfo,
            'user_rule_list' => $userRoleList
        ];

    }

    // 用户账号游戏套餐权限列表
    public static function getUserSetMealGameList($params)
    {
        // 游戏表数据
        $games=ApiGame::find()->select(['id as game_id','name','e_name','e_short','image'])->asArray()->all();
        // 当前套餐游戏
        $currentGamesArray=[];
        foreach ($games as $gameInfo) {
            $gameInfo['set_meal_games'] = [];
            $currentGamesArray[$gameInfo['game_id']] = $gameInfo;
        }
        $setMealGames = ApiSetmealGeme::find()->alias("sg")
            ->select(['sg.id','sg.game_id','sg.opening_at','sg.expiration_at','sg.data_level','sg.user_id'])
            ->leftJoin("api_game as g","g.id = sg.game_id")
            ->where(['sg.user_id'=>$params['user_id']])
            ->andWhere(['>=', "sg.expiration_at", date("Y-m-d H:i:s",time())])
            ->asArray()->all();
//        echo $setMealGames->createCommand()->getRawSql();die;
        foreach ($setMealGames as $info) {
            // 当前套餐游戏
            if (isset($currentGamesArray[$info['game_id']])) {
                $currentGamesArray[$info['game_id']]['set_meal_games'][]=$info;
            }
        }
//                        echo "<pre>";
//        print_r($currentGamesArray);die;
        return $currentGamesArray;

    }

    // 用户账号套餐添加修改
    public static function getUserSetMealEdit($params)
    {
        if (!$params['user_id']) {
            throw new RequestParamsUnvalidatedException([], "Illegal parameter");
        }
        // id 套餐id
        // user_id 用户id
        // group_id 版本id
        // opening_at 开通套餐时间
        // expiration_at 到期时间
        // user_rights 用户权限
        // request_restriction 请求限制
        // is_interface 接口（1-默认 2-自定义）

        $nowTime = time();
        // 套餐id
        $setMealId = $params['id']??null;
        // 处理时间格式
        if (preg_match('/^(\d{4})(-|\/)(\d{1,2})\2(\d{1,2})$/',$params['opening_at'])) {
            $params['opening_at'] = $params['opening_at']." 00:00:00";
        }
        if (preg_match('/^(\d{4})(-|\/)(\d{1,2})\2(\d{1,2})$/',$params['expiration_at'])) {
            $params['expiration_at'] = $params['expiration_at']." 23:59:59";
        }
        $openingAtTime = strtotime($params['opening_at']);
        $expirationAtTime = strtotime($params['expiration_at']);
        if ($openingAtTime > $expirationAtTime) {
            //开通时间不能大于过期时间
            throw new BusinessException([], "Plan date illegal！");
        }
        // 记录log数据
        $oldInfo = $newInfo = [];
        // 开启事物处理
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($setMealId) {
                // update
                $ApiSetMealModel = ApiSetmeal::find()->where(['id' => $setMealId])->one();
                $oldInfo = [
                    'group_id' => $ApiSetMealModel->group_id,
                    'opening_at' => $ApiSetMealModel->opening_at,
                    'expiration_at' => $ApiSetMealModel->expiration_at,
                    'user_rights' => $ApiSetMealModel->user_rights,
                    'request_restriction' => $ApiSetMealModel->request_restriction,
                    'is_interface' => $ApiSetMealModel->is_interface,
                ];
                // 验证时间是否冲突
                $setMeals = ApiSetmeal::find()->where(['user_id' => $params['user_id']])
                    ->andWhere(["!=","id",$setMealId])
                    ->asArray()->all();
            } else {
                // add
                $ApiSetMealModel = new ApiSetmeal();
                // 验证时间是否冲突
                $setMeals = ApiSetmeal::find()->where(['user_id' => $params['user_id']])->asArray()->all();
            }
            // 验证时间是否冲突
            foreach ($setMeals as $sInfo) {
                // 开通时间
                $openingTime = strtotime($sInfo['opening_at']);
                // 过期时间
                $expirationTime = strtotime($sInfo['expiration_at']);
                $isNext = self::is_time_cross($openingTime,$expirationTime,$openingAtTime,$expirationAtTime);
                if ($isNext) {
                    // 请更换开通时间或过期时间
                    throw new BusinessException([], "Plan date conflict！");
                }
            }
            // 变更user表变更时间
            $userModel = Admin::find()->where(['id'=>$params['user_id']])->one();
            $userModel->setAttribute('modified_at',date('Y-m-d H:i:s',$nowTime));
            if (!$userModel->save()) {
                throw new BusinessException($userModel->getErrors(), "Operation failed");
            }
            $ApiSetMealModel->setAttributes($params);
            if (!$ApiSetMealModel->save()) {
                throw new BusinessException($ApiSetMealModel->getErrors(), "Operation failed");
            }
            // 记录log
            $newInfo = $params;
            if ($setMealId) {
                $oldInfo = array_merge($params,$oldInfo);
            }
            $diffInfo = ContrastServices::getDiffInfo($oldInfo,$newInfo);
            if ($diffInfo["changed"]) {
                OperationLogService::addLog(
                    $diffInfo['change_type'],
                    1,
                    "set_meal",
                    $params['user_id'],
                    $diffInfo
                );
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            \Yii::warning($e->getMessage());
            throw $e;
        }
        // 套餐区分逻辑
        self::setUserSetMeal($params['user_id'],$nowTime);
        // 删除Redis key
        RedisBase::keyDel([ApiConstant::PREFIX_WEB_API, 'SetMeal', $params['user_id']]);
        RedisBase::keyDel([ApiConstant::PREFIX_WEB_API, 'Group', $params['user_id']]);
        return [
            'msg' => "操作成功！"
        ];
    }
    // 用户账号套餐更新
    public static function setUserSetMeal($userId,$nowTime)
    {
        // 套餐区分逻辑
        $userSetMeals = ApiSetmeal::find()->select(['id','opening_at','expiration_at'])
            ->where(['user_id' => $userId])->andWhere(['>=', "expiration_at", date("Y-m-d H:i:s",$nowTime)])
            ->orderBy('opening_at')->limit(2)->asArray()->all();
//        if (!empty($userSetMeals)) {
            $userSetMealModel = ApiUserSetMeal::find()->where(['user_id' => $userId])->one();
            if (empty($userSetMealModel)) {
                $userSetMealModel = new ApiUserSetMeal();
                $userSetMealModel->setAttribute('user_id',$userId);
            }
            // 当前套餐
            $userCurrentSetMeals = [];
            // 未来套餐
            $userRenewSetMeals = [];
            foreach ($userSetMeals as $k => $smInfo) {
                if (!$k) {
                    $openingTime = strtotime($smInfo['opening_at']);
                    // 判断是否未来 或者 当前
                    if ($openingTime > $nowTime) {
                        // 未来套餐
                        $userRenewSetMeals = $smInfo;
                        break;
                    } else {
                        // 当前套餐
                        $userCurrentSetMeals = $smInfo;
                    }
                } else {
                    // 未来套餐
                    $userRenewSetMeals = $smInfo;
                }
            }
            // 当前套餐
            if (!empty($userCurrentSetMeals)) {
                $userSetMealModel->setAttribute('set_meal_id', $userCurrentSetMeals['id']);
            } else {
                $userSetMealModel->setAttribute('set_meal_id', null);
            }
            // 未来套餐
            if (!empty($userRenewSetMeals)) {
                $userSetMealModel->setAttribute('renew_set_meal_id', $userRenewSetMeals['id']);
            } else {
                $userSetMealModel->setAttribute('renew_set_meal_id', null);
            }
            if (!$userSetMealModel->save()) {
                throw new BusinessException($userSetMealModel->getErrors(), "Operation failed");
            }
//        }
    }
    // 用户账号套餐删除
    public static function getUserSetMealDel($params)
    {
        $nowTime = time();
        $id = $params['id'];
        if ($id) {
            $ApiSetMeal=ApiSetmeal::find()->where(['id'=>$id])->one();
            if (!empty($ApiSetMeal)) {
                // 开启事物处理
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $userId = $ApiSetMeal->user_id;
                    if ($userId) {
                        if (!$ApiSetMeal->delete()) {
                            throw new BusinessException($ApiSetMeal->getErrors(), "Operation failed");
                        }
                        // 变更user表变更时间
                        $userModel = Admin::find()->where(['id' => $userId])->one();
                        $userModel->setAttribute('modified_at', date('Y-m-d H:i:s', $nowTime));
                        if (!$userModel->save()) {
                            throw new BusinessException($userModel->getErrors(), "Operation failed");
                        }
                        // 记录log
                        OperationLogService::addLog(
                            BusinessConsts::CHANGE_TYPE_DELETE,
                            1,
                            'set_meal',
                            $userId,
                            ["diff" => [], "new" => []]
                        );
                    }
                    $transaction->commit();
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    \Yii::warning($e->getMessage());
                    throw $e;
                }

                // 套餐区分逻辑
                self::setUserSetMeal($userId,$nowTime);
                // 删除Redis key
                RedisBase::keyDel([ApiConstant::PREFIX_WEB_API, 'SetMeal', $userId]);
                RedisBase::keyDel([ApiConstant::PREFIX_WEB_API, 'Group', $userId]);
                return true;
            }
        }
        throw new BusinessException([], "Operation failed");
    }
    // 用户账号套餐历史
    public static function getUserSetMealHistory($params)
    {
        // 第一组
//        $sTime = strtotime("2021-05-10 00:00:00");
//        $eTime = strtotime("2021-05-24 23:59:59");
        // 第二组
//        $ssTime = strtotime("2021-05-25 00:00:00");
//        $eeTime = strtotime("2021-05-27 23:59:59");

//        // 第一组
//        $sTime1 = strtotime("2021-05-10 00:00:00");
//        $eTime1 = strtotime("2021-05-10 23:59:59");
//        // 第二组
//        $ssTime1 = strtotime("2021-05-01 00:00:00");
//        $eeTime1 = strtotime("2021-05-31 23:59:59");
//        echo self::is_time_cross($sTime,$eTime,$ssTime,$eeTime);die;
//        echo self::is_time_cross($sTime1,$eTime1,$ssTime1,$eeTime1);die;

        $nowDate = date("Y-m-d H:i:s",time());
        return ApiSetmeal::find()->alias('s')
            ->select(['s.id','r.request_num','r.hour','r.hour_cn','g.group_name','g.group_e_name','s.opening_at','s.expiration_at','s.created_at',
                '(case when s.is_interface=1 then "Default" when s.is_interface=2 then "Customize" else null end) as interface_name',
                '(case when s.is_interface=1 then "默认" when s.is_interface=2 then "自定义" else null end) as interface_name_cn'])
            ->leftJoin('enum_request_restriction as r', "r.id = s.request_restriction")
            ->leftJoin("api_group as g","g.id = s.group_id")
            ->where(['s.user_id'=>$params['user_id']])
            ->andWhere(['<=', "s.expiration_at", $nowDate])
            ->asArray()->all();
//        $sql=$infos->createCommand()->getRawSql();
    }

    // 用户账号游戏套餐添加修改
    public static function getUserSetMealGameEdit($params)
    {
        if (!$params['user_id']) {
            throw new RequestParamsUnvalidatedException([], "Illegal parameter");
        }
        // 参数

        // id 套餐游戏id
        // user_id 用户id
        // game_id 游戏id
        // data_level 游戏数据级别（数据级别 1-基础 2-高级 3-不提供）
        // opening_at 开通游戏套餐时间
        // expiration_at 到期时间

        // 处理时间格式
        if (preg_match('/^(\d{4})(-|\/)(\d{1,2})\2(\d{1,2})$/',$params['opening_at'])){
            $params['opening_at'] = $params['opening_at']." 00:00:00";
        }
        if (preg_match('/^(\d{4})(-|\/)(\d{1,2})\2(\d{1,2})$/',$params['expiration_at'])){
            $params['expiration_at'] = $params['expiration_at']." 23:59:59";
        }
        $openingAtTime = strtotime($params['opening_at']);
        $expirationAtTime = strtotime($params['expiration_at']);
        if ($openingAtTime > $expirationAtTime) {
            // 开通时间不能大于过期时间
            throw new BusinessException([], "Game authority date illegal！");
        }
        // 记录log数据
        $oldInfo = $newInfo = [];
        $nowDate = date("Y-m-d H:i:s",time());
        // 开启事物处理
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if (!empty($params['id'])) {
                $ApiSetMealGame=ApiSetmealGeme::find()->where(['id'=>$params['id']])->one();
                $oldInfo = [
                    'data_level' => $ApiSetMealGame->data_level,
                    'opening_at' => $ApiSetMealGame->opening_at,
                    'expiration_at' => $ApiSetMealGame->expiration_at,
                ];
                $oldInfo = array_merge($params,$oldInfo);
                // 验证时间是否冲突
                $setMealGames = ApiSetmealGeme::find()->where(['user_id'=>$params['user_id'],'game_id'=>$params['game_id']])
                    ->andWhere(["!=","id",$params['id']])
                    ->asArray()->all();
            } else {
                $ApiSetMealGame = new ApiSetmealGeme();
                // 验证时间是否冲突
                $setMealGames = ApiSetmealGeme::find()->where(['user_id'=>$params['user_id'],'game_id'=>$params['game_id']])->asArray()->all();
            }
            // 验证时间是否冲突
            foreach ($setMealGames as $sInfo) {
                // 开通时间
                $openingTime = strtotime($sInfo['opening_at']);
                // 过期时间
                $expirationTime = strtotime($sInfo['expiration_at']);
                $isNext = self::is_time_cross($openingTime,$expirationTime,$openingAtTime,$expirationAtTime);
                if ($isNext) {
                    //请更换开通时间或过期时间
                    throw new BusinessException([], "Game authority date conflict！");
                }
            }
            // 变更user表变更时间
            $userModel = Admin::find()->where(['id'=>$params['user_id']])->one();
            $userModel->setAttribute('modified_at',$nowDate);
            if (!$userModel->save()) {
                throw new BusinessException($userModel->getErrors(), "Operation failed");
            }
            $ApiSetMealGame->setAttributes($params);
            if (!$ApiSetMealGame->save()) {
                throw new BusinessException($ApiSetMealGame->getErrors(), "Operation failed");
            }
            // 更新用户的套餐游戏
            $setMealGameIds = ApiSetmealGeme::find()
                ->select(['id'])
                ->where(['user_id'=>$params['user_id']])
                ->andWhere(['in','data_level',[1,2]])
                ->andWhere(['and',['<=', "opening_at", $nowDate],['>=', "expiration_at", $nowDate]])
                ->asArray()->all();
            // 记录套餐游戏关联表
            $userSetMealModel = ApiUserSetMeal::find()->where(['user_id'=>$params['user_id']])->one();
            if (empty($userSetMealModel)) {
                $userSetMealModel = new ApiUserSetMeal();
                $userSetMealModel->setAttribute('user_id',$params['user_id']);
            }
            if (!empty($setMealGameIds)) {
                $setMealGameIdsJson = json_encode(array_unique(array_column($setMealGameIds,'id')));
                $userSetMealModel->setAttribute('set_meal_game_ids',$setMealGameIdsJson);
            } else {
                $userSetMealModel->setAttribute('set_meal_game_ids',null);
            }
            if (!$userSetMealModel->save()) {
                throw new BusinessException($userSetMealModel->getErrors(), "Operation failed");
            }
            // 记录log
            $diffInfo = ContrastServices::getDiffInfo($oldInfo,$newInfo);
            if ($diffInfo["changed"]) {
                OperationLogService::addLog(
                    $diffInfo['change_type'],
                    1,
                    "set_meal_game",
                    $params['user_id'],
                    $diffInfo
                );
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            \Yii::warning($e->getMessage());
            throw $e;
        }

        // 删除Redis key
        RedisBase::keyDel([ApiConstant::PREFIX_WEB_API, 'SetMeal', $params['user_id'], 'Games']);

        return true;
    }
    // 用户账号游戏套餐删除
    public static function getUserSetMealGameDel($params)
    {
        $nowTime = time();
        $id = $params['id'];
        if ($id) {
            $ApiSetMealGame=ApiSetmealGeme::find()->where(['id'=>$id])->one();
            if (!empty($ApiSetMealGame)) {
                // 开启事物处理
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $openingAtTime = strtotime($ApiSetMealGame->opening_at);
                    $expirationAtTime = strtotime($ApiSetMealGame->expiration_at);
                    $userId = $ApiSetMealGame->user_id;
                    if ($userId) {
                        if (!$ApiSetMealGame->delete()) {
                            throw new BusinessException($ApiSetMealGame->getErrors(), "Operation failed");
                        }
                        if ($nowTime >= $openingAtTime && $nowTime <= $expirationAtTime) {
                            $ApiUserSetMealModel = ApiUserSetMeal::find()->where(['user_id' => $userId])->one();
                            if (!empty($ApiUserSetMealModel)) {
                                $setMealGameIdsJson = $ApiUserSetMealModel->set_meal_game_ids;
                                if ($setMealGameIdsJson) {
                                    $setMealGameIds = json_decode($setMealGameIdsJson, true);
                                    foreach ($setMealGameIds as $k => $sgId) {
                                        if ($sgId == $id) {
                                            unset($setMealGameIds[$k]);
                                            break;
                                        }
                                    }
                                    if (!empty($setMealGameIds)) {
                                        $ApiUserSetMealModel->setAttribute('set_meal_game_ids', json_encode($setMealGameIds));
                                    } else {
                                        $ApiUserSetMealModel->setAttribute('set_meal_game_ids', null);
                                    }
                                    if (!$ApiUserSetMealModel->save()) {
                                        throw new BusinessException($ApiUserSetMealModel->getErrors(), "Operation failed");
                                    }
                                }
                            }
                        }
                        // 变更user表变更时间
                        $userModel = Admin::find()->where(['id' => $userId])->one();
                        $userModel->setAttribute('modified_at', date('Y-m-d H:i:s', time()));
                        if (!$userModel->save()) {
                            throw new BusinessException($userModel->getErrors(), "Operation failed");
                        }
                        // 记录log
                        OperationLogService::addLog(
                            BusinessConsts::CHANGE_TYPE_DELETE,
                            1,
                            'set_meal_game',
                            $userId,
                            ["diff" => [], "new" => []]
                        );
                    }
                    $transaction->commit();
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    \Yii::warning($e->getMessage());
                    throw $e;
                }
                // 删除Redis key
                RedisBase::keyDel([ApiConstant::PREFIX_WEB_API, 'SetMeal', $userId, 'Games']);
                return true;
            }
        }
        throw new BusinessException([], "Operation failed");
    }
    // 用户账号游戏套餐历史记录
    public static function getUserSetMealGameHistory($params)
    {
        // $params['user_id']
        $nowDate = date("Y-m-d H:i:s",time());
        return ApiSetmealGeme::find()->alias("s")
            ->select(['s.id','s.opening_at','s.expiration_at','s.data_level','g.name','g.e_name','g.e_short','g.image'])
            ->leftJoin("api_game as g","g.id=s.game_id")
            ->where(['s.user_id'=>$params['user_id']])
            ->andWhere(['<=', "s.expiration_at", $nowDate])
            ->asArray()->all();
    }
    // 判断时间冲突
    public static function is_time_cross($beginTime1, $endTime1, $beginTime2, $endTime2) {
        $status = $beginTime2 - $beginTime1;
        if ($status > 0) {
            $status2 = $beginTime2 - $endTime1;
            if ($status2 >= 0) {
                return false;
            } else {
                return true;
            }
        } else {
            $status2 = $endTime2 - $beginTime1;
            if ($status2 > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    // 地区数据
    public static function getRegions()
    {
//        $json_string = file_get_contents(\Yii::$app->basePath.'/data/country.json');
//        return json_decode($json_string,true);
        $q = ApiCountry::find()->select('id,name,e_name,two,image');
        $zhCountry = $q->orderBy("CONVERT( name USING gbk ) COLLATE gbk_chinese_ci ASC")->asArray()->all();
        $enCountry = $q->orderBy('e_name asc')->asArray()->all();
        return [
            'zh' => $zhCountry,
            'en' => $enCountry
        ];
    }

    // 赛事状态码表
    public static function getMatchStatus()
    {
        $sql = "select * from %s";
        $q = DbOnline::getDb()->createCommand(sprintf($sql,"enum_match_state"));
        $statusArray = $q->queryAll();
        return $statusArray;
    }


    // 用户列表操作日志
    public static function getUserOperationLog($params)
    {
        // $params['user_id']
        $q = OperationLog::find()->alias('l')
            ->select(['l.id','l.type','l.resource_type','u.name','l.info','l.created_time',
                '(case when user_type=1 then "Admin" when user_type=2 then "Robot" else null end) as user_name'])
            ->leftJoin("admin as u","u.id = l.user_id")
            ->where(['log_type'=>1,'resource_id'=>$params['user_id']]);

        // count计数
        $count = $q->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $logs = $q->offset($pages->offset)->limit($pages->limit)->orderBy("l.id desc")->asArray()->all();
//        echo "<pre>";
//        print_r($logs);die;
        return [
            'list' => $logs,
            'count' => $count
        ];
    }

    // 用户私有赛事操作日志
    public static function getUserTournamentsLog($params)
    {
        // $params['user_id']
        $q = OperationLog::find()->alias('l')
            ->select(['l.id','l.type','l.resource_type','u.name','l.info','l.created_time',
                '(case when user_type=1 then "Admin" when user_type=2 then "Robot" else null end) as user_name'])
            ->leftJoin("admin as u","u.id = l.user_id")
            ->where(['log_type'=>2,'resource_id'=>$params['tournament_id'],'resource_ext_id'=>$params['user_id']]);
        // count计数
        $count = $q->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $logs = $q->offset($pages->offset)->limit($pages->limit)->orderBy("l.id desc")->asArray()->all();
        return [
            'list' => $logs,
            'count' => $count
        ];
    }
}