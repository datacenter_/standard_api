<?php
/**
 * super
 */

namespace app\modules\backstage\services;

class ContrastServices
{
    public static function getDiffInfo($oldInfo, $newInfo, $filterKeys = [], $arrayOpen = true)
    {
        $diff = [];
        $changeType = BusinessConsts::QUEUE_TYPE_ADD;
        if ($oldInfo) {
            $diff = self::diffParams($oldInfo, $newInfo, $filterKeys, $arrayOpen);
            $changeType = BusinessConsts::QUEUE_TYPE_UPDATE;
            if (!count($diff)) {
                return ["changed" => false, 'change_type' => BusinessConsts::CHANGE_TYPE_EQUAL];
            }
        }
        return [
            'changed' => true,
            'change_type' => $changeType,
            'diff' => $diff,
            'old' => $oldInfo,
            'new' => $newInfo
        ];
    }

    /**
     * @param $oldInfo
     * @param $newInfo
     * @param $filterKeys
     * @return array
     * 比较两个一维数组差异，获取增量，如果第二个比第一个key多，比较不出来
     */
    public static function diffParams($oldInfo, $newInfo, $filterKeys = [], $arrayOpen = true)
    {
        // 获取array数据变化
        if ($arrayOpen) {
            $oldInfo = self::openArray($oldInfo);
            $newInfo = self::openArray($newInfo);
        }
        if ($filterKeys && count($filterKeys)) {
            $arrayFilter = array_fill_keys($filterKeys, "");
            $interset = array_intersect_key($oldInfo, $arrayFilter);
            $oldInfo = array_merge($arrayFilter, $interset);
        }
        $di = array_diff_assoc($oldInfo,$newInfo);
        $diff = [];
        foreach ($di as $key => $val) {
            $diff[$key] = [
                "key" => $key,
                "before" => isset($oldInfo[$key]) ? $oldInfo[$key] : "",
                "after" => isset($newInfo[$key]) ? $newInfo[$key] : "",
            ];
        }
        return $diff;
    }

    /**
     * @param $array
     * @param string $split
     * @return array
     * 展开数组
     */
    public static function openArray($array, $split = "|")
    {
        return self::_openArray($array, $split);
    }

    private static function _openArray($array, $split, $keyPrefix = "", $deep = 0)
    {
        $list = [];
        foreach ($array as $key => $val) {
            $keyP = $keyPrefix ? $keyPrefix . "|" : "";
            $keyTemp = $keyP . $key;
            if (is_array($val) && count($val)) {
                $list = array_merge($list, self::_openArray($val, $split, $keyTemp, $deep + 1));
            } else if (is_array($val) && !count($val)) {
                $list[$keyTemp] = json_encode($val);;
            } else {
                $list[$keyTemp] = $val;
            }
        }
        return $list;
    }
}