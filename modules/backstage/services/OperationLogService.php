<?php
/**
 *
 */

namespace app\modules\backstage\services;

use app\modules\common\models\OperationLog;
use app\rest\exceptions\BusinessException;

class OperationLogService
{
    const USER_TYPE_ADMIN = 1;
    const USER_TYPE_ROBOT = 2;

    // 这个值 用来划分批次
    private $versionKey = null;
    private static $instance = null;

    // 构造方法私有化，防止外部创建实例
    private function __construct()
    {

    }

    private function __clone()
    {

    }

    public static function getInstance()
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function addLog($operationType, $logType, $resourceType, $resourceId, $info, $resourceExtId=null, $userType=self::USER_TYPE_ADMIN, $userId=null)
    {
        // 添加主表增量
        $opt = new OperationLog();
        $opt->setAttributes([
            'type' => $operationType,
            'log_type' => $logType,
            'version_key' => self::getVersionKey(),
            'user_type' => $userType,
            'user_id' => $userId ? $userId : self::getLoginUserId(),
            'resource_type' => $resourceType,
            'resource_id' => $resourceId,
            'resource_ext_id' => $resourceExtId,
            'info' => json_encode($info),
        ]);
        if (!$opt->save()) {
            throw new BusinessException($opt->getErrors(), "添加log失败");
        }
    }

    public static function getLoginUserId()
    {
        try {
            $id = \Yii::$app->getUser()->getId();
            return $id;
        } catch (\Exception $e){
            return 0;
        }
    }

    public static function getVersionKey()
    {
        return self::getInstance()->getVersion();
    }

    public static function resetVersionKey()
    {
        return self::getInstance()->setVersion();
    }

    public function setVersion()
    {
        // 用来区分操作，可能一次操作修改了多个记录，这里标识出来
        $this->versionKey = md5(uniqid(md5(microtime(true)), true));
    }

    public function getVersion()
    {
        if (!$this->versionKey) {
            $this->setVersion();
        }
        return $this->versionKey;
    }
}