<?php
/**
 * super
 */

namespace app\modules\backstage\services;

use app\modules\common\models\ApiCompetitionAuthority;
use app\modules\common\models\ApiGame;
use app\modules\common\models\ApiGroup;
use app\modules\common\models\ApiRule;
use app\modules\common\models\ApiSetmeal;
use app\modules\common\models\EnumRequestRestriction;
use app\modules\common\models\OperationLog;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\AuthorityDetection;
use app\modules\common\services\RedisBase;
use app\modules\mysqls\models\DbOnline;
use app\modules\admin\models\Admin;
use app\modules\common\models\ApiSetmealGeme;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RequestParamsUnvalidatedException;
use yii\data\Pagination;

class Administration
{
    // 接口基本数据
    const INTERFACE_ARRAY=[
        [
            'id' => 1,
            'name' => 'Default',
            'name_cn' => '默认',
        ],
        [
            'id' => 2,
            'name' => 'Customize',
            'name_cn' => '自定义',
        ]
    ];
    // 客户管理权限（账号列表）
    public static function getCustomList($params)
    {
        // 变更日志 没有数据

        // 账号列表
        $q = Admin::find()->alias('u');
        $q->select(['u.id as user_id', 'u.email', 'p.group_name', 'p.group_e_name', 'm.opening_at', 'm.expiration_at',
            'p2.group_name as renew_group_name', 'p2.group_e_name as renew_group_e_name', 'm2.opening_at as renew_opening_at', 'm2.expiration_at as renew_expiration_at',
            'r.request_num', 'r.hour', 'r.hour_cn', 'u.created_at', 'u.modified_at', 'um.set_meal_game_ids']);
        $q->leftJoin('api_user_set_meal as um', "um.user_id = u.id");
        $q->leftJoin('api_setmeal as m', "m.id = um.set_meal_id");
        $q->leftJoin('api_group as p', "p.id = m.group_id");
        $q->leftJoin('api_setmeal as m2', "m2.id = um.renew_set_meal_id");
        $q->leftJoin('api_group as p2', "p2.id = m2.group_id");
        $q->leftJoin('enum_request_restriction as r', "r.id = m.request_restriction");
        $q->where(['u.role_type'=>3]);
        // 创建时间筛选
        if (isset($params['created_at_start']) && !empty($params['created_at_start'])) {
            $q->andWhere(['>=', "u.created_at", $params['created_at_start']]);
        }
        if (isset($params['created_at_end']) && !empty($params['created_at_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['created_at_end'])));
            $q->andWhere(['<', "u.created_at", $end]);
        }
        // 变更时间筛选
        if (isset($params['modified_at_start']) && !empty($params['modified_at_start'])) {
            $q->andWhere(['>=', "u.modified_at", $params['modified_at_start']]);
        }
        if (isset($params['modified_at_end']) && !empty($params['modified_at_end'])) {
            $end = date('Y-m-d', strtotime('+1 day', strtotime($params['modified_at_end'])));
            $q->andWhere(['<', "u.modified_at", $end]);
        }
        // 邮件筛选
        if (isset($params['email']) && !empty($params['email'])) {
            $q->andWhere(['like', 'u.email', $params['email']]);
        }
        // 用户id筛选
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $q->andWhere(['u.id' => $params['user_id']]);
        }
        // count计数
        $count = $q->count();
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $customArray = $q->offset($pages->offset)->limit($pages->limit)->orderBy("u.id asc")->asArray()->all();
        if (!empty($customArray)) {
            // 日志数量
            $logInfoArray = [];
            $userIds = array_column($customArray, 'user_id');
            $logInfos = OperationLog::find()->select(['resource_id', 'count(id) as log_num'])
                ->where(['in', 'resource_id', $userIds])->andWhere(['log_type' => 1])
                ->groupBy('resource_id')->asArray()->all();
            foreach ($logInfos as $info) {
                $userId = $info['resource_id'];
                $logInfoArray[$userId] = $info;
            }
            // 套餐游戏id处理
            $set_meal_game_ids = [];
            foreach ($customArray as &$info) {
                if ($info['set_meal_game_ids']) {
                    if (empty($set_meal_game_ids)) {
                        $set_meal_game_ids = json_decode($info['set_meal_game_ids'], true);
                    } else {
                        $set_meal_game_ids = array_merge($set_meal_game_ids, json_decode($info['set_meal_game_ids'], true));
                    }
                }
            }
            $set_meal_game_ids = array_unique($set_meal_game_ids);
            // 游戏数据
            $gamesData = ApiGame::find()->select(['image', 'simple_image', 'id as game_id'])->asArray()->all();
            $gamesDataArray = [];
            foreach ($gamesData as $game) {
                $game['opening_at'] = null;
                $game['expiration_at'] = null;
                $game['data_level'] = null;
                $gamesDataArray[$game['game_id']] = $game;
            }
            // 游戏套餐数据
            $sq = ApiSetmealGeme::find()->alias('sg');
            $sq->select(['g.image', 'g.simple_image', 'sg.opening_at', 'sg.expiration_at', 'sg.id', 'sg.game_id', 'sg.data_level']);
            $sq->leftJoin('api_game as g', "g.id = sg.game_id");
            $sq->where(['in', 'sg.id', $set_meal_game_ids]);
            $games = $sq->asArray()->all();
            $gamesArray = [];
            foreach ($games as $gameInfo) {
                $sgId = $gameInfo['id'];
                unset($gameInfo['id']);
                $gamesArray[$sgId] = $gameInfo;
            }
            // 游戏套餐
            foreach ($customArray as $key => &$userCustom) {
                $setMealGameIdsJson = $userCustom['set_meal_game_ids'];
                unset($customArray[$key]['set_meal_game_ids']);
                if ($setMealGameIdsJson) {
                    $currentGamesDataArray = $gamesDataArray;
                    $currentSetMealGameIds = json_decode($setMealGameIdsJson, true);
                    foreach ($currentSetMealGameIds as $gId) {
                        if (isset($gamesArray[$gId])) {
                            $currentGamesDataArray[$gamesArray[$gId]['game_id']] = array_merge($currentGamesDataArray[$gamesArray[$gId]['game_id']], $gamesArray[$gId]);
                        }
                    }
                    $customArray[$key]['game_setmeal'] = $currentGamesDataArray;
                } else {
                    $customArray[$key]['game_setmeal'] = $gamesDataArray;
                }
                if (isset($logInfoArray[$userCustom['user_id']])) {
                    $customArray[$key]['log_num'] = $logInfoArray[$userCustom['user_id']]['log_num'];
                } else {
                    $customArray[$key]['log_num'] = 0;
                }
            }
        }
//        echo "<pre>";
//        print_r($customArray);die;
        return [
            'list' => $customArray,
            'count' => $count
        ];
    }

    // 用户详情数据
    public static function getUserDetail($params)
    {
        $q=Admin::find()->select(['id as user_id','email','username','cell_phone','tele_phone','country','time_zone']);
        $q->where(['id'=>$params['user_id'] ?? null]);
        return $q->asArray()->one();
    }

    // 私有赛事（单个用户的私有赛事列表）
    public static function getUserPrivateTournamentsList($params)
    {
        // $params['user_id']

        // 当前用户私有赛事
        $q=ApiCompetitionAuthority::find()->select(['id','sour_id','delay_type as private_delay_type','data_level as private_data_level','deleted','created_at','modified_at']);
        $q->orderBy("created_at desc");
        $q->where(['user_id'=>$params['user_id']]);
        $userTourList=$q->asArray()->all();

        $sourIds=array_column($userTourList,'sour_id');
        $where = "1=1";
        // 当前用户所有赛事id
        if (!empty($sourIds)) {
            $where .= " AND t.id in (".implode($sourIds,',').")";
        } else {
            return [
                'count' => 0,
                'list'  => []
            ];
        }
        // 处理结果数据
        $userTourArray=[];
        // 私有延迟类型
        $private_delay_type = [
            1=>'公网',
            2=>'优势',
            3=>'私有',
            4=>'无延迟',
        ];
        $private_delay_type_e = [
            1=>'Public',
            2=>'Advantage',
            3=>'Private',
            4=>'Unlimited',
        ];
        // 私有数据等级
        $private_data_level = [
            1=>'基础',
            2=>'高级',
        ];
        $private_data_level_e = [
            1=>'Basic',
            2=>'Advanced',
        ];
        foreach ($userTourList as $userTour) {
            $private_delay_type_e_field = [
                'private_delay_type_e' => null
            ];
            $private_data_level_e_filed = [
                'private_data_level_e' => null
            ];
            // 私有延迟类型
            if (isset($private_delay_type[$userTour['private_delay_type']])) {
                $userTour['private_delay_type']=$private_delay_type[$userTour['private_delay_type']];
                $private_delay_type_e_field['private_delay_type_e'] = $private_delay_type_e[$userTour['private_delay_type']];
                $userTour = array_merge($userTour,$private_delay_type_e_field);
            } else {
                $userTour = array_merge($userTour,$private_delay_type_e_field);
            }
            // 私有数据等级
            if (isset($private_data_level[$userTour['private_data_level']])) {
                $userTour['private_data_level']=$private_data_level[$userTour['private_data_level']];
                $private_data_level_e_filed['private_data_level_e'] = $private_data_level_e[$userTour['private_data_level']];
                $userTour = array_merge($userTour,$private_data_level_e_filed);
            } else {
                $userTour = array_merge($userTour,$private_data_level_e_filed);
            }
            $sid=$userTour['sour_id'];
            unset($userTour['sour_id']);
            $userTourArray[$sid]=$userTour;
        }
        // 赛事id筛选
        if (isset($params['tid']) && !empty($params['tid'])) {
            if (preg_match("/^\d*$/",$params['tid'])) {
                $where .= " AND t.id={$params['tid']}";
            } else {
                $where .= " AND t.id=0";
            }
        }
        // 游戏筛选
        if (isset($params['game_id']) && !empty($params['game_id'])) {
            $where .= " AND t.game={$params['game_id']}";
        }
        // 状态筛选
        if (isset($params['status']) && !empty($params['status'])) {
            $where .= " AND t.status={$params['status']}";
        }
        // 赛事名称筛选
        if (isset($params['name']) && !empty($params['name'])) {
            $where .= " AND (t.name LIKE '%{$params['name']}%' OR t.name_cn LIKE '%{$params['name']}%')";
        }
        // 计划开始时间筛选
        if (isset($params['scheduled_begin_at_start']) && !empty($params['scheduled_begin_at_start']) &&
            isset($params['scheduled_begin_at_end']) && !empty($params['scheduled_begin_at_end'])) {
            $where .= " AND (t.scheduled_begin_at >= '{$params['scheduled_begin_at_start']}' AND t.scheduled_begin_at <= '{$params['scheduled_begin_at_end']}')";
        }
        // 计划结束时间筛选
        if (isset($params['scheduled_end_at_start']) && !empty($params['scheduled_end_at_start']) &&
            isset($params['scheduled_end_at_end']) && !empty($params['scheduled_end_at_end'])) {
            $where .= " AND (t.scheduled_end_at >= '{$params['scheduled_end_at_start']}' AND t.scheduled_end_at <= '{$params['scheduled_end_at_end']}')";
        }
        $countSql = "select count(t.id) as num from tournament as t where {$where}";
        $countArray = DbOnline::getDb()->createCommand($countSql)->queryAll();
        $count = isset($countArray[0]['num']) ? $countArray[0]['num']:0;
        // 分页
        $page = \Yii::$app->request->get('page', 1) - 1;
        $pageSize = \Yii::$app->request->get('per_page', 20);
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => $pageSize,  // 分页默认条数是20条
            'page' => $page,
        ]);
        $sql = "select %s from %s as t left join enum_game as g on t.game=g.id left join enum_match_state as s on t.status=s.id where %s order by t.id asc limit %s,%s";
        $field = 't.id as t_id,g.name as g_name,g.e_name as g_e_name,g.image as g_image,t.name as t_name,t.image,t.scheduled_begin_at,t.scheduled_end_at,s.c_name as status,
        case when t.public_delay=1 then "公网" when t.public_delay=2 then "优势" when t.public_delay=3 then "无延迟" when t.public_delay=4 then "平台默认" else null end as public_delay,
        case when t.public_delay=1 then "Public" when t.public_delay=2 then "Advantage" when t.public_delay=3 then "Unlimited" when t.public_delay=4 then "Default" else null end as public_delay_e,
        case when t.public_data_devel=1 then "基础" when t.public_data_devel=2 then "高级" when t.public_data_devel=3 then "不提供" when t.public_data_devel=4 then "平台默认" else null end as public_data_devel,
        case when t.public_data_devel=1 then "Basic" when t.public_data_devel=2 then "Advanced" when t.public_data_devel=3 then "Unsupported" when t.public_data_devel=4 then "Default" else null end as public_data_devel_e';
        $sql = sprintf($sql, $field, "tournament", $where, $pages->offset, $pages->limit);
        $q = DbOnline::getDb()->createCommand($sql);
        $tournaments = $q->queryAll();
        if (!empty($tournaments)) {
            // 日志数量
            $logInfoArray = [];
            $tIds = array_column($tournaments, 't_id');
            $logInfos = OperationLog::find()->select(['resource_id', 'count(id) as log_num'])
                ->where(['in', 'resource_id', $tIds])->andWhere(['resource_ext_id' => $params['user_id'], 'log_type' => 2])
                ->groupBy('resource_id')->asArray()->all();
            foreach ($logInfos as $info) {
                $tId = $info['resource_id'];
                $logInfoArray[$tId] = $info;
            }
            foreach ($tournaments as $k => $tournamentInfo) {
                if (isset($userTourArray[$tournamentInfo['t_id']])) {
                    $tournaments[$k] = array_merge($tournaments[$k], $userTourArray[$tournamentInfo['t_id']]);
                }
                if (isset($logInfoArray[$tournamentInfo['t_id']])) {
                    $tournaments[$k]['log_num'] = $logInfoArray[$tournamentInfo['t_id']]['log_num'];
                } else {
                    $tournaments[$k]['log_num'] = 0;
                }
            }
        }
        return [
            'count' => $count,
            'list'  => $tournaments
        ];
    }
    // 私有赛事（单个用户的私有赛事删除/恢复）
    public static function getUserPrivateTournamentDel($params)
    {
        if (empty($params['id']) || empty($params['deleted'])) {
            throw new RequestParamsUnvalidatedException([], "Illegal parameter");
        }
        $deleted = $params['deleted'];
        $CompetitionAuthorityModel=ApiCompetitionAuthority::find()->where(['id'=>$params['id']])->one();
        if (!empty($CompetitionAuthorityModel)) {
            // 开启事物处理
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $userId = $CompetitionAuthorityModel->user_id;
                $CompetitionAuthorityModel->setAttribute('deleted', $deleted);
                if ($deleted == 1) {
                    // 删除
                    $CompetitionAuthorityModel->setAttribute('deleted_at', date('Y-m-d H:i:s'));
                    // 记录log
                    OperationLogService::addLog(
                        BusinessConsts::CHANGE_TYPE_DELETE,
                        2,
                        'tournament',
                        $CompetitionAuthorityModel->sour_id,
                        ["diff" => [], "new" => []],
                        $userId
                    );
                } else {
                    $newInfo = [
                        'id' => $params['id'],
                        'deleted' => $deleted,
                        'deleted_at' => null,
                    ];
                    $oldInfo = [
                        'id' => $params['id'],
                        'deleted' => $CompetitionAuthorityModel->deleted,
                        'deleted_at' => $CompetitionAuthorityModel->deleted_at,
                    ];
                    // 恢复
                    $CompetitionAuthorityModel->setAttribute('deleted_at', null);
                    // 记录log
                    $diffInfo = ContrastServices::getDiffInfo($oldInfo,$newInfo);
                    if ($diffInfo["changed"]) {
                        OperationLogService::addLog(
                            BusinessConsts::CHANGE_TYPE_RECOVERY,
                            2,
                            'tournament',
                            $CompetitionAuthorityModel->sour_id,
                            $diffInfo,
                            $userId
                        );
                    }
                }
                if (!$CompetitionAuthorityModel->save()) {
                    throw new BusinessException($CompetitionAuthorityModel->getErrors(), 'Operation failed');
                }
                if ($userId) {
                    // 变更user表变更时间
                    $userModel = Admin::find()->where(['id' => $userId])->one();
                    $userModel->setAttribute('modified_at', date('Y-m-d H:i:s', time()));
                    if (!$userModel->save()) {
                        throw new BusinessException($userModel->getErrors(), "Operation failed");
                    }
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                \Yii::warning($e->getMessage());
                throw $e;
            }

            if ($deleted==1) {
                // 删除Redis key
                RedisBase::keyDel([ApiConstant::PREFIX_WEB_API, 'Private-Tournament', $userId]);
            }
        }

        return ['id' => $params['id'], 'msg' => '删除成功'];
    }
    // 新增私有赛事（搜索赛事信息）
    public static function getTournamentsList($params,$isPrivate = true)
    {
        $where = "1=1";
        // 游戏筛选
        if (isset($params['game_id']) && !empty($params['game_id'])) {
            $where .= " AND t.game={$params['game_id']}";
        }
        // 赛事名称筛选
        if (isset($params['name']) && !empty($params['name'])) {
            $where .= " AND (t.name LIKE '%{$params['name']}%' OR t.name_cn LIKE '%{$params['name']}%')";
        }
        // 赛事ID筛选
        if (isset($params['tournament_id']) && !empty($params['tournament_id'])) {
            $where .= " AND t.id={$params['tournament_id']}";
        }
        // 没有where不返回数据
        if ($where == '1=1') {
            return [];
        }
        if ($isPrivate) {
            $where .= " AND t.is_private=1";
        }
        $sql = "select %s from %s as t where %s";
        $field = 't.id,t.name,t.name_cn';
        $q = DbOnline::getDb()->createCommand(sprintf($sql, $field, "tournament", $where));
        $tournamentArray = $q->queryAll();
        return $tournamentArray;
    }
    // 新增私有赛事（某条赛事详细信息）
    public static function getTournamentInfo($params)
    {
        $tid = $params['tournament_id'];
        $sql = "select %s from %s as t left join enum_game as g on t.game=g.id left join enum_match_state as s on t.status=s.id where %s";
        $field = 't.id,g.name as g_name,g.e_name as g_e_name,t.name as t_name,t.image,t.scheduled_begin_at,t.scheduled_end_at,s.c_name as status,
        case when t.is_private=1 then "是"  else "否" end as is_private,case when t.is_private_delay=1 then "是"  else "否" end as is_private_delay,
        case when t.is_private=1 then "True"  else "False" end as is_private_e,case when t.is_private_delay=1 then "True"  else "False" end as is_private_delay_e,
        case when t.public_delay=1 then "公网" when t.public_delay=2 then "优势" when t.public_delay=3 then "无延迟" when t.public_delay=4 then "平台默认" else "无" end as public_delay,
        case when t.public_delay=1 then "Public" when t.public_delay=2 then "Advantage" when t.public_delay=3 then "Unlimited" when t.public_delay=4 then "Default" else "" end as public_delay_e,
        case when t.public_data_devel=1 then "基础" when t.public_data_devel=2 then "高级" when t.public_data_devel=3 then "不提供" when t.public_data_devel=4 then "平台默认" else "无" end as public_data_devel,
        case when t.public_data_devel=1 then "Basic" when t.public_data_devel=2 then "Advanced" when t.public_data_devel=3 then "Unsupported" when t.public_data_devel=4 then "Default" else "" end as public_data_devel_e,
        t.private_delay_seconds';
        $q = DbOnline::getDb()->createCommand(sprintf($sql, $field, "tournament", "t.id=$tid"));
        $tournamentInfo = $q->queryOne();
        return $tournamentInfo;
    }
    // 修改用户私有赛事（修改用户私有赛事详细信息）
    public static function getUserEditTournamentInfo($params)
    {
        // id
        $caInfo=ApiCompetitionAuthority::find()
            ->select(['sour_id','delay_type','data_level'])
            ->where(['id'=>$params['id']])->asArray()->one();
        if (empty($caInfo)) {
            return [];
        }
        $param['tournament_id']=$caInfo['sour_id'];
        unset($caInfo['sour_id']);
        $tournamentInfo = self::getTournamentsList($param,false);
        $currentDetail=[];
        if (!empty($tournamentInfo)) {
            $currentDetail=array_merge($tournamentInfo[0],$caInfo);
        }
        return $currentDetail;
    }
    // 新增/修改用户私有赛事（入库操作）
    public static function putUserTournamentEdit($attributes)
    {
        // 赛事id sour_id
        // 用户id user_id
        // 私有延迟 delay_type
        // 私有数据级别 data_level

        if (empty($attributes['user_id']) || empty($attributes['sour_id'])) {
            throw new RequestParamsUnvalidatedException([], "Illegal parameter");
        }
        $id = $attributes['id'] ?? null;
        // 记录log数据
        $oldInfo = [];
        $newInfo = $attributes;
        if (!$id) {
            $caInfo=ApiCompetitionAuthority::find()->where(['user_id'=>$attributes['user_id'],'sour_id'=>$attributes['sour_id']])->asArray()->one();
            if (!empty($caInfo)) {
                // 该私有赛事已经存在
                throw new BusinessException([],"Already exists！");
            }
            // add
            $ApiCompetitionAuthority=new ApiCompetitionAuthority();
        } else {
            $count=ApiCompetitionAuthority::find()->where(['user_id'=>$attributes['user_id'],'sour_id'=>$attributes['sour_id']])->andWhere(["!=","id",$id])->count();
            unset($attributes['id']);
            if ($count) {
                // 该私有赛事已经存在
                throw new BusinessException([],"Already exists！");
            }
            // update
            $ApiCompetitionAuthority=ApiCompetitionAuthority::find()->where(['id'=>$id])->one();
            $oldInfo = [
                'id' => $ApiCompetitionAuthority->id,
                'user_id' => $ApiCompetitionAuthority->user_id,
                'sour_id' => $ApiCompetitionAuthority->sour_id,
                'delay_type' => $ApiCompetitionAuthority->delay_type,
                'data_level' => $ApiCompetitionAuthority->data_level,
            ];
        }
        // 开启事物处理
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            // 变更user表变更时间
            $userModel = Admin::find()->where(['id' => $attributes['user_id']])->one();
            $userModel->setAttribute('modified_at', date('Y-m-d H:i:s', time()));
            if (!$userModel->save()) {
                throw new BusinessException($userModel->getErrors(), "Operation failed");
            }
            $ApiCompetitionAuthority->setAttributes($attributes);
            if (!$ApiCompetitionAuthority->save()) {
                throw new BusinessException($ApiCompetitionAuthority->getErrors(), "Operation failed");
            }

            // 记录log
            $diffInfo = ContrastServices::getDiffInfo($oldInfo,$newInfo);
            if ($diffInfo["changed"]) {
                OperationLogService::addLog(
                    $diffInfo['change_type'],
                    2,
                    'tournament',
                    $attributes['sour_id'],
                    $diffInfo,
                    $attributes['user_id']
                );
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            \Yii::warning($e->getMessage());
            throw $e;
        }
        // 删除Redis key
        RedisBase::keyDel([ApiConstant::PREFIX_WEB_API, 'Private-Tournament', $attributes['user_id']]);
        return true;
    }

    // 默认版本权限
    public static function getRoleList($params)
    {
        $defaultRequestRestriction = null;
        $groupId = $params['group_id'] ?? null;
        // 当前账号默认版本权限
        $userGroupInfo=ApiGroup::find()->select(['rights'])->where(['id'=>$groupId])->asArray()->one();
        $userRights= json_decode($userGroupInfo['rights'],true);
        // 所有权限数据
        $ruleArray = ApiRule::find()->select(['id', 'r_name', 'r_name_cn', 'pid', 'order'])->where(['status'=>1])->asArray()->all();
        $currentRuleArray=[];
        foreach ($ruleArray as $ruleInfo) {
            if($ruleInfo['pid']){
                $ruleInfo['is_tower'] = 2;
            }
            $currentRuleArray[$ruleInfo['id']]=$ruleInfo;
        }
        foreach ($userRights as $gId) {
            if (isset($currentRuleArray[$gId]['is_tower'])) {
                if (isset($currentRuleArray[$gId])) {
                    $currentRuleArray[$gId]['is_tower'] = 1;
                } else {
                    $currentRuleArray[$gId]['is_tower'] = 2;
                }
            }
        }
        // 结果数据
        $roleArray = self::recursiveRule($currentRuleArray,0);
        $field = array_column($roleArray, 'order');
        array_multisort($field, SORT_ASC, $roleArray);
        // 请求限制默认值
        switch ($groupId) {
            case ApiConstant::GROUP_INTERNAL_ID://内部版
                $defaultRequestRestriction = 8;
                break;
            case ApiConstant::GROUP_MAJOR_ID://专业版
                $defaultRequestRestriction = 5;
                break;
            case ApiConstant::GROUP_MAJOR_PRO_ID://专业版-加强
                $defaultRequestRestriction = 5;
                break;
            case ApiConstant::GROUP_BASIC_ID://基础版
                $defaultRequestRestriction = 4;
                break;
            case ApiConstant::GROUP_BASIC_PRO_ID://基础版-加强
                $defaultRequestRestriction = 4;
                break;
            case ApiConstant::GROUP_FREE_ID://免费版
                $defaultRequestRestriction = 2;
                break;
            case 7://未提供
                $defaultRequestRestriction = 1;
                break;
        }
        return [
            'list' => $roleArray,
            'default_restriction_id' => $defaultRequestRestriction
        ];
    }
    // 递归处理权限数据
    public static function recursiveRule($roleData, $pid)
    {
        $roleArray = [];
        foreach ($roleData as &$roleInfo) {
            if ($roleInfo['pid']==$pid) {
                $roleArray[$roleInfo['id']] = $roleInfo;
                $roleArray[$roleInfo['id']]['subset'] = self::recursiveRule($roleData, $roleInfo['id']);
            }
        }
        return $roleArray;
    }
    // 版本码表数据
    public static function getUserGroupList()
    {
        return ApiGroup::find()->select(['id','group_name','group_e_name'])->orderBy('order asc')->asArray()->all();
    }
    // 请求限制码表数据
    public static function getRequestRestrictionList()
    {
        return EnumRequestRestriction::find()->asArray()->all();
    }

    // 账号套餐对应的权限（旧版）
    public static function getUserRoleList($params)
    {
        // $params['user_id']

        // 用户所有套餐
        $setmeals=Admin::find()->alias('u')
            ->select(['s.id','s.group_id','u.group_id as u_g_id','s.opening_at','s.expiration_at','s.is_stale','s.user_rights','s.is_interface','s.request_restriction'])
            ->innerJoin("api_setmeal as s","s.user_id=u.id and (s.is_stale=1 or s.is_stale=2)")
            ->where(['u.id'=>$params['user_id']])->asArray()->all();
        // 处理后的套餐数据
        $setmealsArray=[];
        $groupId=null;
        if (!empty($setmeals)) {
            // 开启事物处理
            $transaction = \Yii::$app->db->beginTransaction();
            try {
//                $setMealsArray=[];
//                foreach ($setmeals as $setMealInfo) {
//                    $setMealsArray[$setMealInfo['is_stale']]=$setMealInfo;
//                }
//                // 此刻时间
//                $nowTime = time();
//                // 当前套餐
//                $currentSetMeal = isset($setMealsArray[1]) ? $setMealsArray[1]:[];
//                if (!empty($currentSetMeal)) {
//                    // 当前套餐开通时间
//                    $currentOpeningTime = strtotime($currentSetMeal['opening_at']);
//                    // 当前套餐过期时间
//                    $currentOpeningTime = strtotime($currentSetMeal['expiration_at']);
//                    //
//                    if ($nowTime >= $currentOpeningTime) {
//                        // 过期
//                    } elseif ($nowTime >= $currentOpeningTime && $nowTime <= $currentOpeningTime) {
//                        // 开通中
//                    }
//                }
//                // 续费套餐
//                $renewSetMeal = isset($setMealsArray[2]) ? $setMealsArray[2]:[];

                $setMealsCount=count($setmeals);
                foreach ($setmeals as $setmealInfo) {
                    // 套餐id
                    $setmealId = $setmealInfo['id'];

                    // 此刻时间
                    $nowTime = time();
                    // 套餐开通时间
                    $setMealOpeningTime = strtotime($setmealInfo['opening_at']);
                    // 套餐过期时间
                    $setMealExpirationTime = strtotime($setmealInfo['expiration_at']);
                    if ($nowTime >= $setMealExpirationTime) {
                        // 已经过期
                        $groupId = 6;// 免费版
                        // 更改套餐状态为过期状态
                        $setmealModel = ApiSetmeal::find()->where(['id' => $setmealId])->one();
                        $setmealModel->setAttribute('is_stale', 3);
                        if (!$setmealModel->save()) {
                            throw new BusinessException($setmealModel->getErrors(), "Operation failed");
                        }
                        if ($setmealInfo['is_stale'] == 1) {
                            $isNext = true;
                        }
                        if ($setmealInfo['is_stale'] == 1 && $setMealsCount==1) {
                            $setmealModel = new ApiSetmeal();
                            // 新增一条免费版的
                            $setmealModel->setAttributes([
                                'is_stale' => 1,
                                'user_id' => $params['user_id'],
                                'group_id' => $groupId,
                                'opening_at' => date("Y-m-d")." 00:00:00",
                                'expiration_at' => "2075-01-01 23:59:59",
                                'user_rights' => ApiGroup::find()->select(['rights'])->where(['id'=>$groupId])->asArray()->one()['rights'],
                                'is_interface' => 1
                            ]);
                            if (!$setmealModel->save()) {
                                throw new BusinessException($setmealModel->getErrors(), "套餐降级为免费版本降级失败！");
                            }
                            // 处理后的数据
                            $setmealsArray[$setmealModel->is_stale] = [
                                'id' => $setmealModel->id,
                                'group_id' => $setmealModel->group_id,
                                'opening_at' => $setmealModel->opening_at,
                                'expiration_at' => $setmealModel->expiration_at,
                                'is_stale' => $setmealModel->is_stale,
                                'user_rights' => $setmealModel->user_rights,
                                'is_interface' => $setmealModel->is_interface,
                                'request_restriction' => $setmealModel->request_restriction,
                            ];
                            break;
                        }

                    } elseif ($nowTime >= $setMealOpeningTime && $nowTime <= $setMealExpirationTime) {
                        // 开通中
                        if (isset($isNext) && $isNext) {
                            // 续费版套餐判断逻辑
                            if ($setmealInfo['is_stale'] == 2) {
                                // 首先把表里面当前套餐状态改为历史
                                ApiSetmeal::updateAll(['is_stale'=>3], ['and',['user_id'=>$params['user_id']],'is_stale'=>1]);
                                // 更改套餐状态为当前套餐状态
                                $setmealModel = ApiSetmeal::find()->where(['id' => $setmealId])->one();
                                $setmealModel->setAttribute('is_stale', 1);
                                if (!$setmealModel->save()) {
                                    throw new BusinessException($setmealModel->getErrors(), "Operation failed");
                                }
                                // 当前套餐状态
                                $setmealInfo['is_stale'] = 1;
                                // 切换续费版groupId
                                $groupId = $setmealModel->group_id;
                            }
                        }
                    } else {
                        // 其他意外情况
                        $groupId = 6;
                    }
                    // 处理后的数据
                    $setmealsArray[$setmealInfo['is_stale']] = $setmealInfo;
                }
                if ($groupId) {
                    // 更改用户的groupId
                    $userModel = Admin::find()->where(['id' => $params['user_id']])->one();
                    // 更改为免费版本
                    $userModel->setAttribute('group_id', $groupId);
//                    $groupName=AuthorityDetection::choiceGroupIdToName($groupId);
//                    $userModel->setAttribute('name', $groupName);
                    if (!$userModel->save()) {
                        throw new BusinessException($userModel->getErrors(), "Operation failed");
                    }
                    // 删除redis key
                    RedisBase::keyDel([ApiConstant::PREFIX_WEB_API, 'Group', $params['user_id']]);
                    RedisBase::keyDel([ApiConstant::PREFIX_WEB_API, 'SetMeal', $params['user_id']]);
                }
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                \Yii::warning($e->getMessage());
                throw $e;
            }
        }
        // 当前套餐数据
        $currentSetMealInfo = isset($setmealsArray[1]) ? $setmealsArray[1]:null;
        // 续费套餐数据
        $renewSetMealInfo = isset($setmealsArray[2]) ? $setmealsArray[2]:null;

        // 当前套餐权限数据
        $currentUserRights=json_decode($currentSetMealInfo['user_rights'],true);
        unset($currentSetMealInfo['user_rights']);
        // 续费套餐权限数据
        $renewUserRights=json_decode($renewSetMealInfo['user_rights'],true);
        unset($renewSetMealInfo['user_rights']);

        // 所有权限数据
        $ruleArray = ApiRule::find()->select(['id', 'r_name', 'r_name_cn', 'status', 'pid'])->asArray()->all();
        // 当前权限数据
        $currentRuleArray=[];
        foreach ($ruleArray as $ruleInfo) {
            if(in_array($ruleInfo['id'],$currentUserRights)){
                $ruleInfo['is_tower'] = 1;
            }else{
                $ruleInfo['is_tower'] = 2;
            }
//            if($ruleInfo['pid']){
//                if($currentSetMealInfo['u_g_id']==1){
//                    $ruleInfo['is_tower'] = 1;
//                }else{
//                    $ruleInfo['is_tower'] = 2;
//                }
//            }
            $currentRuleArray[$ruleInfo['id']]=$ruleInfo;
        }
        // 续费权限数据
        $renewRuleArray=$currentRuleArray;

        // 处理当前权限数据
//        foreach ($currentUserRights as $gId) {
//            if ($currentSetMealInfo['u_g_id']==1) {
//                break;
//            }
//            if (isset($currentRuleArray[$gId])) {
//                $currentRuleArray[$gId]['is_tower'] = 1;
//            } else {
//                $currentRuleArray[$gId]['is_tower'] = 2;
//            }
//        }
        // 当前套餐权限结果数据
        $currentRoleArray = self::recursiveRule($currentRuleArray,0);

        // 处理续费权限数据
        foreach ($ruleArray as $ruleInfo) {
            if(in_array($ruleInfo['id'],$renewUserRights)){
                $ruleInfo['is_tower'] = 1;
            }else{
                $ruleInfo['is_tower'] = 2;
            }
            $renewRuleArray[$ruleInfo['id']]=$ruleInfo;
        }
//        // 处理续费权限数据
//        foreach ($renewUserRights as $rgId) {
//
//            if ($renewRuleArray['u_g_id']==1) {
//                break;
//            }
//            if (isset($renewRuleArray[$rgId])) {
//                $renewRuleArray[$rgId]['is_tower'] = 1;
//            } else {
//                $renewRuleArray[$rgId]['is_tower'] = 2;
//            }
//        }
        // 续费套餐权限结果数据
        $renewRoleArray = self::recursiveRule($renewRuleArray,0);

        return [
            'current_user_setmeal' => $currentSetMealInfo,// 当前套餐
            'renew_user_setmeal' => $renewSetMealInfo,// 续费套餐
            'current_user_rule' => $currentRoleArray,// 当前套餐权限
            'renew_user_rule' => $renewRoleArray,// 续费套餐权限
            'interface' => self::INTERFACE_ARRAY
        ];
    }

    // 用户账号历史套餐对应的权限记录
    public static function getUserRoleHistoryList($params)
    {
        // 用户所有历史套餐权限
        $setmeals=Admin::find()->alias('u')
            ->select(['s.group_id','s.opening_at','s.expiration_at','s.is_stale','s.user_rights','s.is_interface','s.request_restriction'])
            ->leftJoin("api_setmeal as s","s.user_id=u.id and s.is_stale=3")
            ->where(['u.id'=>$params['user_id']])->asArray()->all();
        // 所有权限数据
        $ruleArray = ApiRule::find()->select(['id', 'r_name', 'r_name_cn', 'status', 'pid'])->asArray()->all();
        // 处理权限数据
        foreach ($ruleArray as $k=>$ruleInfo) {
            if($ruleInfo['pid']){
                $ruleArray[$k]['is_tower'] = 2;
            }
        }
        // 当前权限数据
        $currentRuleArray=[];
        foreach ($ruleArray as $ruleInfo) {
            $currentRuleArray[$ruleInfo['id']]=$ruleInfo;
        }
        // 结果数据
        $resultData=[];

        foreach ($setmeals as $setmealInfo) {
            // 重置赋值权限数据
            $regetRuleArray=$currentRuleArray;

            $currentRights=json_decode($setmealInfo['user_rights'],true);
            unset($setmealInfo['user_rights']);

            foreach ($currentRights as $gId) {
                if (isset($regetRuleArray[$gId])) {
                    $regetRuleArray[$gId]['is_tower'] = 1;
                } else {
                    $regetRuleArray[$gId]['is_tower'] = 2;
                }
            }
            // 套餐权限结果数据
            $renewRoleArray = self::recursiveRule($regetRuleArray,0);

            $resultData[] = [
                'user_setmeal' => $setmealInfo,
                'user_rule' => $renewRoleArray,
            ];
        }
//        echo "<pre>";
//        print_r($resultData);die;
        return $resultData;

    }
    // 用户账号套餐对应的权限记录 添加/修改/删除 分情况而定
    public static function putUserRoleEdit($params)
    {
        // id 套餐id
        // user_id 用户id
        // group_id 版本id
        // is_stale 哪种套餐（1-当前套餐 2-续费套餐）
        // opening_at 开通套餐时间
        // expiration_at 到期时间
        // user_rights 用户权限
        // request_restriction 请求限制
        // is_interface 接口（1-默认 2-自定义）

        // 套餐id
        $setMealId = $params['id']??null;
        if ($setMealId) {
            $ApiSetmealModel = ApiSetmeal::find()->where(['id' => $setMealId])->one();
        } else {
            $ApiSetmealModel=new ApiSetmeal();
        }
        // 处理时间格式
        if (preg_match('/^(\d{4})(-|\/)(\d{1,2})\2(\d{1,2})$/',$params['opening_at'])){
            $params['opening_at'] = $params['opening_at']." 00:00:00";
        }
        if (preg_match('/^(\d{4})(-|\/)(\d{1,2})\2(\d{1,2})$/',$params['expiration_at'])){
            $params['expiration_at'] = $params['expiration_at']." 23:59:59";
        }
        if ($params['group_id']==6 && $params['is_stale']==1) {
            $params['opening_at'] = date("Y-m-d")." 00:00:00";
            $params['expiration_at'] = "2075-01-01 23:59:59";
        } elseif ($params['group_id']==6 && $params['is_stale']==2) {
            throw new RequestParamsUnvalidatedException([],"免费版本不可当作续费套餐！");
        }
        // 开启事物处理
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($setMealId&&$params['group_id']==7) {
                // 修改操作
                // 修改用户表版本groupId为 未提供（下面统一操作）
                // 修改套餐表为过期状态
                $ApiSetmealModel->setAttribute('is_stale',3);
                // 修改套餐游戏为过期状态
                $SetmealGames=ApiSetmealGeme::find()->where(['user_id'=>$params['user_id'],'is_stale'=>$params['is_stale']])->all();
                if (!empty($SetmealGames)) {
                    ApiSetmealGeme::updateAll(['is_stale'=>3], ['and',['user_id'=>$params['user_id']],'is_stale'=>$params['is_stale']]);
                }
            } elseif ($setMealId&&$params['group_id']!=7) {
                // 判断当前套餐和续费套餐的时间是否冲突
                $setMeals = ApiSetmeal::find()->select(['is_stale','opening_at','expiration_at'])
                    ->where(['user_id'=>$params['user_id']])->andWhere(['in', 'is_stale', [1,2]])->asArray()
                    ->all();
                $setMealsArray=[];
                foreach ($setMeals as $setMealInfo) {
                    $setMealsArray[$setMealInfo['is_stale']]=$setMealInfo;
                }
                if ($params['is_stale']==2) {
                    // 续费套餐
                    $paramOpeningTime = strtotime($params['opening_at']);
                    $paramExpirationTime = strtotime($params['expiration_at']);
                    // 当前套餐
                    $currentSetMeal = isset($setMealsArray[1])?$setMealsArray[1]:[];
                    if (!empty($currentSetMeal)) {
                        $currentOpeningTime = strtotime($currentSetMeal['opening_at']);
                        $currentExpirationTime = strtotime($currentSetMeal['expiration_at']);
                        if ($paramOpeningTime >= $currentOpeningTime && $paramOpeningTime <= $currentOpeningTime) {
                            throw new RequestParamsUnvalidatedException([],"请修改开通套餐时间，不能和当前套餐时间冲突！");
                        }
                        if ($paramExpirationTime >= $currentExpirationTime && $paramExpirationTime <= $currentExpirationTime) {
                            throw new RequestParamsUnvalidatedException([],"请修改过期套餐时间，不能和当前套餐时间冲突！");
                        }
                    }
                } elseif ($params['is_stale']==1) {
                    // 当前套餐
                    $paramOpeningTime = strtotime($params['opening_at']);
                    $paramExpirationTime = strtotime($params['expiration_at']);
                    // 续费套餐
                    $renewSetMeal = isset($setMealsArray[2])?$setMealsArray[2]:[];
                    if (!empty($renewSetMeal)) {
                        $renewOpeningTime = strtotime($renewSetMeal['opening_at']);
                        $renewExpirationTime = strtotime($renewSetMeal['expiration_at']);
                        if ($paramOpeningTime >= $renewOpeningTime && $paramOpeningTime <= $renewExpirationTime) {
                            throw new RequestParamsUnvalidatedException([],"请修改开通套餐时间，不能和续费套餐时间冲突！");
                        }
                        if ($paramExpirationTime >= $renewOpeningTime && $paramExpirationTime <= $renewExpirationTime) {
                            throw new RequestParamsUnvalidatedException([],"请修改过期套餐时间，不能和续费套餐时间冲突！");
                        }
                    }
                } else {
                    // 报错
                    throw new RequestParamsUnvalidatedException([], "参数信息不正确！");
                }
                // 正常修改操作
                // 修改用户表版本id（下面统一操作）
                // 修改套餐表版本id
                $ApiSetmealModel->setAttributes($params);
            } else {
                $setMeals = ApiSetmeal::find()->select(['is_stale','opening_at','expiration_at'])
                    ->where(['user_id'=>$params['user_id']])->andWhere(['in', 'is_stale', [1,2]])->asArray()
                    ->all();
                $setMealsArray=[];
                foreach ($setMeals as $setMealInfo) {
                    $setMealsArray[$setMealInfo['is_stale']]=$setMealInfo;
                }
                $paramOpeningTime = strtotime($params['opening_at']);
                $paramExpirationTime = strtotime($params['expiration_at']);
                // 判断当前套餐和续费套餐的时间是否冲突
                if ($params['is_stale']==2) {
                    // 续费套餐
                    $renewSetMeal = isset($setMealsArray[2])?$setMealsArray[2]:[];
                    if (!empty($renewSetMeal)) {
                        throw new RequestParamsUnvalidatedException([],"请刷新界面在提交！");
                    }
                    // 当前套餐
                    $currentSetMeal = isset($setMealsArray[1])?$setMealsArray[1]:[];
                    if (!empty($currentSetMeal)) {
                        $currentOpeningTime = strtotime($currentSetMeal['opening_at']);
                        $currentExpirationTime = strtotime($currentSetMeal['expiration_at']);
                        if ($paramOpeningTime >= $currentOpeningTime && $paramOpeningTime <= $currentOpeningTime) {
                            throw new RequestParamsUnvalidatedException([],"请修改开通套餐时间，不能和当前套餐时间冲突！");
                        }
                        if ($paramExpirationTime >= $currentExpirationTime && $paramExpirationTime <= $currentExpirationTime) {
                            throw new RequestParamsUnvalidatedException([],"请修改过期套餐时间，不能和当前套餐时间冲突！");
                        }
                    }
                } elseif ($params['is_stale']==1) {
                    // 当前套餐
                    $currentSetMeal = isset($setMealsArray[1])?$setMealsArray[1]:[];
                    if (!empty($currentSetMeal)) {
                        throw new RequestParamsUnvalidatedException([],"请刷新界面在提交！");
                    }
                    // 续费套餐
                    $renewSetMeal = isset($setMealsArray[2])?$setMealsArray[2]:[];
                    if (!empty($renewSetMeal)) {
                        $renewOpeningTime = strtotime($renewSetMeal['opening_at']);
                        $renewExpirationTime = strtotime($renewSetMeal['expiration_at']);
                        if ($paramOpeningTime >= $renewOpeningTime && $paramOpeningTime <= $renewExpirationTime) {
                            throw new RequestParamsUnvalidatedException([],"请修改开通套餐时间，不能和续费套餐时间冲突！");
                        }
                        if ($paramExpirationTime >= $renewOpeningTime && $paramExpirationTime <= $renewExpirationTime) {
                            throw new RequestParamsUnvalidatedException([],"请修改过期套餐时间，不能和续费套餐时间冲突！");
                        }
                    }
                } else {
                    // 报错
                    throw new RequestParamsUnvalidatedException([], "参数信息不正确！");
                }
                // 新增操作
                // 修改用户表版本id（下面统一操作）
                // 新增套餐表数据
                $ApiSetmealModel->setAttributes($params);
            }
            if (!$ApiSetmealModel->save()) {
                throw new BusinessException($ApiSetmealModel->getErrors(), "Operation failed");
            }
            // 用户表
            $userModel=Admin::find()->where(['id'=>$params['user_id']])->one();
            $userModel->setAttribute('group_id',$params['group_id']);
//            $groupName=AuthorityDetection::choiceGroupIdToName($params['group_id']);
//            $userModel->setAttribute('name', $groupName);
            if ($params['is_stale'] == 2) {
                // is_renewal 是否续费
                $userModel->setAttribute('is_renewal',1);
            }
            if (!$userModel->save()) {
                throw new BusinessException($userModel->getErrors(), "Operation failed");
            }

            // 删除redis key
            RedisBase::keyDel([ApiConstant::PREFIX_WEB_API, 'Group', $params['user_id']]);
            RedisBase::keyDel([ApiConstant::PREFIX_WEB_API, 'SetMeal', $params['user_id']]);

            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            \Yii::warning($e->getMessage());
            throw $e;
        }

        return [
            'msg' => "操作成功！"
        ];
    }

    // 用户账号游戏套餐权限
    public static function getUserGameRoleList($params)
    {
        // $params['user_id']

        // 游戏表数据
        $games=ApiGame::find()->select(['id as game_id','name','e_short','image'])->asArray()->all();
        // 当前套餐游戏
        $currentGamesArray=[];
        foreach ($games as $gameInfo) {
            $gameInfo['data_level'] = 3;
            $gameInfo['opening_at'] = null;
            $gameInfo['expiration_at'] = null;
            $gameInfo['id'] = null;
            $gameInfo['is_stale'] = null;
            $gameInfo['user_id'] = $params['user_id'];
            $currentGamesArray[$gameInfo['game_id']] = $gameInfo;
        }
        // 过期游戏变更为历史状态
        $nowDate = date("Y-m-d H:i:s",time());
        $setMealExpirationIds=ApiSetmealGeme::find()->select(['id'])
            ->where(['in', 'is_stale', [1,2]])->andWhere(['user_id'=>$params['user_id']])
//            ->andWhere(['and', ['>=', "opening_at", $nowDate], ['<=', "expiration_at", $nowDate]])
            ->andWhere(['<=', "expiration_at", $nowDate])
            ->asArray()->all();
        if (!empty($setMealExpirationIds)) {
            $setMealExpirationIdArray = array_column($setMealExpirationIds,'id');
            ApiSetmealGeme::updateAll(['is_stale'=>3], ['in', 'id', $setMealExpirationIdArray]);
        }
        // 续费套餐游戏数据处理
        $renewGamesArray=$currentGamesArray;
        $setmealGames=ApiSetmealGeme::find()->alias("s")
            ->select(['s.user_id','s.game_id','s.id','s.is_stale','s.opening_at','s.expiration_at','s.data_level','g.name','g.e_short','g.image'])
            ->leftJoin("api_game as g","g.id=s.game_id")
            ->where(['in', 's.is_stale', [1,2]])
            ->andWhere(['s.user_id'=>$params['user_id']])->asArray()->all();
        foreach ($setmealGames as $info) {
            // 当前套餐游戏
            if ($info['is_stale'] == 1 && isset($currentGamesArray[$info['game_id']])) {
                $currentGamesArray[$info['game_id']]=array_merge($currentGamesArray[$info['game_id']],$info);
            }
            // 续费套餐游戏
            if ($info['is_stale'] == 2 && isset($renewGamesArray[$info['game_id']])) {
                $renewGamesArray[$info['game_id']]=array_merge($renewGamesArray[$info['game_id']],$info);
            }
        }

        return [
            'current_user_games'=>$currentGamesArray,
            'renew_user_games'=>$renewGamesArray,
        ];

    }
    // 用户账号游戏权限 (历史记录)
    public static function getUserGameRoleHistoryList($params)
    {
        // $params['user_id']
        return ApiSetmealGeme::find()->alias("s")
            ->select(['s.id','s.is_stale','s.opening_at','s.expiration_at','s.data_level','g.name','g.e_short','g.image'])
            ->leftJoin("api_game as g","g.id=s.game_id")
            ->andWhere(['s.user_id'=>$params['user_id'],'s.is_stale'=>3])
            ->asArray()->all();
    }
    // 用户账号游戏权限 添加/修改
    public static function getUserGameRoleEdit($params)
    {
        // 参数
        $attrs = $params['game_roles'];
        // id 套餐游戏id
        // user_id 用户id
        // is_stale 哪种套餐（1-当前套餐 2-续费套餐）
        // data_level 游戏数据级别（数据级别 1-基础 2-高级 3-不提供）
        // opening_at 开通游戏套餐时间
        // expiration_at 到期时间

        foreach ($attrs as $k=>$info) {
            // 处理时间格式
            if (preg_match('/^(\d{4})(-|\/)(\d{1,2})\2(\d{1,2})$/',$info['opening_at'])){
                $info['opening_at'] = $info['opening_at']." 00:00:00";
            }
            if (preg_match('/^(\d{4})(-|\/)(\d{1,2})\2(\d{1,2})$/',$info['expiration_at'])){
                $info['expiration_at'] = $info['expiration_at']." 23:59:59";
            }
            if (!empty($info['id'])) {
                $ApiSetmealGeme=ApiSetmealGeme::find()->where(['id'=>$info['id']])->one();
                if (empty($ApiSetmealGeme)) {
                    continue;
                }
                // 去除游戏权限 - 变更为历史数据
                if ($info['is_stale']==3) {
                    unset($info['data_level']);
                }
            } else {
                $ApiSetmealGeme=new ApiSetmealGeme();
            }
            $ApiSetmealGeme->setAttributes($info);
            if (!$ApiSetmealGeme->save()) {
                throw new BusinessException($ApiSetmealGeme->getErrors(), "Operation failed");
            }
        }

        return true;
    }
    // 白名单列表
    public static function getUserWhiteList($params)
    {
        // 用户表
        $userModel=Admin::find()->select(['white_list'])->where(['id'=>$params['user_id']])->one();
        $userWhiteListJson=$userModel['white_list'];
        return json_decode($userWhiteListJson,true) ?? [];
    }
    // 白名单添加/修改
    public static function getUserWhiteListEdit($params)
    {
        // 记录log数据
        $oldInfo = $newInfo = [];
        // 参数
        $whiteListJson = json_encode($params['white_list']);
        // 用户表
        $userModel=Admin::find()->where(['id'=>$params['user_id']])->one();
        // 旧数据
        if ($userModel->white_list && $userModel->white_list!='[]') {
            $oldInfo = json_decode($userModel->white_list,true);
        }
        // 新数据
        if (!empty($params['white_list'])) {
            $newInfo = $params['white_list'];
        }
        $userModel->setAttribute('white_list',$whiteListJson);
        if (!$userModel->save()) {
            throw new BusinessException($userModel->getErrors(), "Operation failed");
        }
        // 记录log
        $diffInfo = ContrastServices::getDiffInfo($oldInfo,$newInfo);
        if ($diffInfo["changed"]) {
            OperationLogService::addLog(
                $diffInfo['change_type'],
                1,
                "set_meal",
                $params['user_id'],
                $diffInfo
            );
        }
        return true;
    }

    // 游戏列表
    public static function getGamesList()
    {
        return ApiGame::find()->select(['id','name','e_name'])->asArray()->all();
    }
}