<?php

namespace app\modules\backstage;

/**
 * common module definition class
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\backstage\controllers';

    public function init()
    {
        parent::init();
    }
}
