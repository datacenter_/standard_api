<?php

namespace app\modules\api;

/**
 * v1 module definition class
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();

        $this->modules = [
            'admin' => [
                'class' => \app\modules\admin\Module::class,
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
            'common' => [
                'class' => \app\modules\common\Module::class,
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
            'backstage' => [
                'class' => \app\modules\backstage\Module::class,
                'components' => [
                    'user' => [
                        'class' => \yii\web\User::class,
                        'identityClass' => \app\modules\admin\models\Admin::class,
                    ],
                ],
            ],
        ];
    }
}
