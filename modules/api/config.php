<?php
return [
    'components' => [
        'urlManager' => [
            'rules' => [
                [
                    'class' => 'yii\web\GroupUrlRule',
                    'prefix' => 'api',
                    'rules' => [
                        // 丁可获取权限
                        'power/obtain' => 'admin/power/get-power',
                        'power/effective' => 'admin/power/power-effective',

                        'GET <module>/<controller>' => '<module>/<controller>/index',
                        '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                    ]
                ]
            ]
        ],
    ],
];