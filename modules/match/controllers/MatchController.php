<?php

namespace app\modules\match\controllers;

use app\controllers\RestController;
use app\modules\common\controllers\CommonController;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use app\modules\match\services\MatchService;
use Elasticsearch\ClientBuilder;

class MatchController extends RestController
{
    public function actionGet(){
        $params = $this->pGet();
        $info = MatchService::getMatchList($params);
        return $info;
    }

    public function actionDetail(){
        $matchId = $this->pGet('match_id');
        $info = MatchService::getMatchDetail($matchId);
        return $info;
    }
}
