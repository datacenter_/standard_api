<?php

namespace app\modules\match\controllers;

use app\controllers\RestController;
use app\modules\common\controllers\CommonController;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use app\modules\match\services\MatchService;
use Elasticsearch\ClientBuilder;

class UpcomingController extends RestController
{
    public function actionGet(){
        $params = $this->pGet();
        $status = "upcoming";
        $info = MatchService::getMatchList($params,$status);
//        $info = MatchService::getMatchUpcomingList($params);
        return $info;
    }
}
