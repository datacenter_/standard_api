<?php

namespace app\modules\match\controllers;

use app\controllers\RestController;
use app\modules\common\controllers\CommonController;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use Elasticsearch\ClientBuilder;

class LivedController extends RestController
{
    public function actionApi(){
        $match_id = 2592;//2587 2593 2592 2550
        $getUrl = '/v1/matches/detail?match_id='.$match_id;
        $res = Common::requestGetApi($getUrl);
        if ($res=='Error'){
            return false;
        }
        $match_info = json_decode($res,true)['data'];
        //初始化Es链接
        $client = EsService::initConnect();
        $index_info = 'resource_match_list';
        $indexParams = [
            'index'  => $index_info,
            'id'     => $match_id,
            'body'   => $match_info,
            'client' => [
                'timeout'         => 10,
                'connect_timeout' => 10
            ]
        ];
//        $indexResponse = $client->index($indexParams);
//        return $indexResponse;
    }

    public function actionGet(){
        $page = $this->pGet('page') ?? 1;
        $per_page = $this->pGet('per_page') ?? 20;
        $index_info = 'resource_match_list';
        $conditions = [
//            ['field' => 'description', 'type' => 'like', 'value' => '2592'],
//            ['field' => 'groupid', 'type' => 'between', 'value' => [4, 6]],
            ['field' => 'match_id', 'type' => '>=', 'value' => '2193'],
        ];
        $offset  = ($page > 0) ? ($page - 1) * $per_page : 0;
        $columns = [];//['match_id', 'description'];
        $order   = array('field' => 'match_id', 'sort' => 'desc');
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $per_page, $columns, $order);
        return $res;
    }

}
