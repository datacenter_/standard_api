<?php
/**
 *
 */

namespace app\modules\match\services;

use app\rest\exceptions\BusinessException;
use yii\db\Exception;

class Common
{
    public static function requestGetApi($getUrl)
    {
        $url = "http://59.110.6.209".$getUrl;
        $header = array(
            'Accept: application/json',
        );
        $curl = curl_init();
        //设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $url);
        //设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, 0);
        // 超时设置,以秒为单位
        curl_setopt($curl, CURLOPT_TIMEOUT, 1);

        // 超时设置，以毫秒为单位
        // curl_setopt($curl, CURLOPT_TIMEOUT_MS, 500);

        // 设置请求头
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        //执行命令
        $data = curl_exec($curl);
        // 显示错误信息
        if (curl_error($curl)) {
            //print "Error: " . curl_error($curl);
            return "Error";
        } else {
            // 打印返回的内容
            return $data;
//            curl_close($curl);
        }
    }

    public static function requestGetCross($getUrl, $header, $params)
    {
        $query = http_build_query($params);
        $query = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $query);
        $urlWithQuery = $getUrl . "?" . $query;
        $headerArray = [];
        foreach ($header as $key => $val) {
            $headerArray[] = sprintf("%s:%s", $key, $val);
        }

        $url = "http://8.210.197.37/reget.php";
        $info = [
            'url' => $urlWithQuery,
            'method' => 'get',
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($info));
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function requestGetCrossDev($getUrl, $header, $params)
    {
        $query = http_build_query($params);
        $query = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $query);
        $urlWithQuery = $getUrl . "?" . $query;
        $headerArray = [];
        foreach ($header as $key => $val) {
            $headerArray[] = sprintf("%s:%s", $key, $val);
        }

        $url = "http://123.56.255.87:8080/reget.php";
        $info = [
            'url' => $urlWithQuery,
            'method' => 'get',
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($info));
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function requestGet($url, $header, $params, $is_build = true)
    {
        if ($is_build) {
            $query = http_build_query($params);
            $query = preg_replace('/%5B[0-9]+%5D/simU', '%5B%5D', $query);
        } else {
            $query = $params;
        }
        $urlWithQuery = $url . "?" . $query;
        $headerArray = [];
        foreach ($header as $key => $val) {
            $headerArray[] = sprintf("%s:%s", $key, $val);
        }
//        print_r($urlWithQuery);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlWithQuery);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function requestPost($url, $header, $params)
    {
        $query = http_build_query($params);
        $headerArray = [];
        foreach ($header as $key => $val) {
            $headerArray[] = sprintf("%s:%s", $key, $val);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function requestPostJsonString($url, $header, $jsonStr, $method)
    {
        $headerArray = [];
        foreach ($header as $key => $val) {
            $headerArray[] = sprintf("%s:%s", $key, $val);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
        } else {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStr);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

}
