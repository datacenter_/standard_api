<?php

namespace app\modules\match\services;

use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\ApiBase;
use app\modules\common\services\RedisBase;

class MatchService
{

    public static function getMatchList($params,$status=null) {
        // 输出格式
        if(isset($params['format'])){
            ApiBase::getFormatType($params['format']);
        }
        // 根据不同套餐进行处理用户游戏套餐
        if (ApiBase::getSetMealTypeId()) {
            if (isset($params['filter']['game_id'])) {
                // 根据版本组验证套餐游戏
                $params['filter']['game_id'] = ApiBase::groupToSetUpGameId($params['filter']['game_id']);
            } else {
                // 没有筛选game_id（走默认）
                $params['filter']['game_id'] = implode(ApiBase::getAuthGameIds(),',');
            }
        }
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_matches_list';
        // 是否为所有数据列表
        if ($status){
            $conditions = [
                ['field' => 'status', 'type' => '=', 'value' => $status]
            ];
        }else{
            $conditions = [];
        }
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'match_id', 'sort' => 'desc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                $isBasic = null;
                // 根据不同套餐取延迟数据
                if (ApiBase::getSetMealTypeId()) {
                    // 验证私有赛事
                    $sportsResultInfo = ApiBase::dealMatchPrivateTournament([
                        'match_delay' => $data['delay'],
                        'tournament_id' => $data['tournament_id'],
                        'is_private' => $data['is_private'],
                        'is_private_delay' => $data['is_private_delay'],
                        'private_delay_seconds' => $data['private_delay_seconds'],
                        'public_delay' => $data['public_delay'],
                        'public_data_devel' => $data['public_data_devel'],
                    ]);
                    $isBasic = $sportsResultInfo['is_basic'];
                    // 获取延迟数
                    if (isset($sportsResultInfo['match_delay'])) {
                        $currentDelay = $sportsResultInfo['match_delay'];
                    } else {
                        $currentDelay = ApiBase::getUserDelaySecond($data['delay']);
                    }
                    $redisKey = self::getIsDelayRedisKey($data['status'],$data['deleted'],$currentDelay,$data['match_id'],$data['end_at']);
                } else {
                    $redisKey = [ApiConstant::PREFIX_API, 'Matchs', $data['match_id']];
                }
                // redis取值
                $json = RedisBase::valueGet($redisKey);
                if ($json) {
                    $matchInfo = json_decode($json, true);
                    // 是否为基础数据
                    if ($isBasic) {
                        // 显示基础结构
                        $matchInfo['is_battle_detailed'] = false;
                        $matchInfo['battles'] = [];
                        $matchInfo['pbpdata'] = [
                            'is_supported' => false,
                            'quality' => 'unsupported',
                            'opens_at' => null,
                            'endpoints' => [],
                        ];
                    }
                    $info['list'][] = $matchInfo;
                }
            }
        }
        return $info;
    }

    public static function getMatchDetail($matchId){
        if(!$matchId){
            return [];
        }
        $isBasic = null;
        // 根据不同套餐取延迟数据
        if (ApiBase::getSetMealTypeId()) {
            $EsService = EsService::initConnect();
            $matchInfo = $EsService->getFile('resource_matches_list',$matchId);
            if($matchInfo){
                // 验证游戏权限
                ApiBase::verifyTheGame($matchInfo['game_id']);
                // 验证私有赛事
                $sportsResultInfo = ApiBase::dealMatchPrivateTournament([
                    'match_delay' => $matchInfo['delay'],
                    'tournament_id' => $matchInfo['tournament_id'],
                    'is_private' => $matchInfo['is_private'],
                    'is_private_delay' => $matchInfo['is_private_delay'],
                    'private_delay_seconds' => $matchInfo['private_delay_seconds'],
                    'public_delay' => $matchInfo['public_delay'],
                    'public_data_devel' => $matchInfo['public_data_devel'],
                ]);
                $isBasic = $sportsResultInfo['is_basic'];
                // 获取延迟数
                if (isset($sportsResultInfo['match_delay'])) {
                    $currentDelay = $sportsResultInfo['match_delay'];
                } else {
                    $currentDelay = ApiBase::getUserDelaySecond($matchInfo['delay']);
                }
                $redisKey = self::getIsDelayRedisKey($matchInfo['status'],$matchInfo['deleted'],$currentDelay,$matchInfo['match_id'],$matchInfo['end_at']);
            }else{
                $redisKey = null;
            }
        } else {
            $redisKey = [ApiConstant::PREFIX_API, 'Matchs', $matchId];
        }
        // redis取值
        $jsonRes = RedisBase::valueGet($redisKey);
        $matchInfo = [];
        if($jsonRes){
            $matchInfo = json_decode($jsonRes,true);
            // 是否为基础数据
            if ($isBasic) {
                // 显示基础结构
                $matchInfo['is_battle_detailed'] = false;
                $matchInfo['battles'] = [];
                $matchInfo['pbpdata'] = [
                    'is_supported' => false,
                    'quality' => 'unsupported',
                    'opens_at' => null,
                    'endpoints' => [],
                ];
            }
        }
        return $matchInfo;
    }

    public static function getIsDelayRedisKey($match_status,$deleted,$delayTime,$match_id=null,$match_end_at=null) {
        if($delayTime != 0 && $deleted==2) {
            // 获取match状态和结束时间
            if ($match_status == 'completed') {
                $nowTime = time();
                $matchDelayEndAt = strtotime($match_end_at) + $delayTime;
                if ($nowTime >= $matchDelayEndAt) {
                    $redisKey = [ApiConstant::PREFIX_API, 'Matchs', $match_id];
                } else {
                    $redisKey = ApiBase::getDelayDate($match_id, $delayTime, 'Matchs');
                }
            } else if ($match_status == 'ongoing') {
                $redisKey = ApiBase::getDelayDate($match_id, $delayTime, 'Matchs');
            } else {
                $redisKey = [ApiConstant::PREFIX_API, 'Matchs', $match_id];
            }
        }else{
            $redisKey = [ApiConstant::PREFIX_API, 'Matchs', $match_id];
        }
        return $redisKey;
    }

}
