<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "operation_log".
 *
 * @property int $id
 * @property string|null $version_key 区分操作
 * @property int|null $log_type 1-用户列表 2-私有赛事列表
 * @property string|null $type 操作类型，添加，修改，删除
 * @property int|null $user_type 操作人类型1,管理员，2机器人
 * @property int|null $user_id 操作人id
 * @property string|null $resource_type 资源
 * @property int|null $resource_id 资源id
 * @property int|null $resource_ext_id 资源扩展id
 * @property string|null $info 操作详情
 * @property string $created_time 时间
 */
class OperationLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operation_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_type', 'user_id', 'resource_id', 'log_type', 'resource_ext_id'], 'integer'],
            [['created_time'], 'safe'],
            [['info'], 'string'],
            [['version_key', 'type', 'resource_type'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'version_key' => '区分操作',
            'log_type' => '1-用户列表 2-私有赛事列表',
            'type' => '操作类型，添加，修改，删除',
            'user_type' => '操作人类型1,管理员，2机器人',
            'user_id' => '操作人id',
            'resource_type' => '资源',
            'resource_id' => '资源id',
            'resource_ext_id' => '资源扩展id',
            'info' => '操作详情',
            'created_time' => '时间',
        ];
    }
}
