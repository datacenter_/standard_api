<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "api_enterprise_certificate".
 *
 * @property int $id
 * @property int|null $user_id 用户id
 * @property string|null $name 企业英文名字
 * @property string|null $name_cn 企业中文名字
 * @property string|null $business_address 企业地址
 * @property string|null $legal_name 法人名
 * @property string|null $surname 法人姓
 * @property int|null $country 国家
 * @property int|null $province_municipalitie 省份
 * @property int|null $city 城市
 * @property string|null $email 企业邮箱
 * @property string|null $created_at
 * @property string|null $modified_at
 * @property int|null $is_review 审核是否通过 1-通过 2-未通过 3-待审核
 */
class ApiEnterpriseCertificate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_enterprise_certificate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'is_review', 'country', 'province_municipalitie', 'city'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
            [['name', 'name_cn', 'email'], 'string', 'max' => 100],
            [['business_address'], 'string', 'max' => 150],
            [['legal_name', 'surname'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'business_address' => 'Business Address',
            'legal_name' => 'Legal Name',
            'surname' => 'Surname',
            'country' => 'Country',
            'province_municipalitie' => 'Province Municipalitie',
            'city' => 'City',
            'email' => 'Email',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'is_review' => 'Is Review',
        ];
    }

    public static function getByName($name)
    {
        return static::findOne(['name' => $name]);
    }

    public static function getByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

}
