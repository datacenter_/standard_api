<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "api_game".
 *
 * @property int $id
 * @property string|null $name 游戏名称
 * @property string|null $e_name 英文名
 * @property string|null $e_short 英文简称
 * @property string|null $logo 图标
 * @property int|null $default_game_rule 默认比赛规则
 * @property int|null $default_match_type 默认比赛类型
 * @property string|null $full_name 全称
 * @property string|null $short_name 简称
 * @property string|null $slug slug
 * @property string|null $created_at
 * @property string|null $deleted_at
 * @property string|null $modified_at
 * @property string|null $image
 * @property string|null $simple_image
 * @property string|null $item_image 道具默认图片
 * @property string|null $role_image 人物默认图片
 * @property string|null $square_image 方形向图
 * @property string|null $rectangle_image 长方形图
 * @property string|null $thumbnail 缩略图
 * @property int|null $flag 1正常，2未开放，3，删除
 */
class ApiGame extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_game';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['default_game_rule', 'default_match_type', 'flag'], 'integer'],
            [['created_at', 'deleted_at', 'modified_at'], 'safe'],
            [['name', 'e_name', 'e_short', 'full_name'], 'string', 'max' => 200],
            [['logo'], 'string', 'max' => 700],
            [['short_name', 'slug', 'image'], 'string', 'max' => 100],
            [['simple_image', 'item_image', 'role_image', 'square_image', 'rectangle_image', 'thumbnail'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'e_name' => 'E Name',
            'e_short' => 'E Short',
            'logo' => 'Logo',
            'default_game_rule' => 'Default Game Rule',
            'default_match_type' => 'Default Match Type',
            'full_name' => 'Full Name',
            'short_name' => 'Short Name',
            'slug' => 'Slug',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'modified_at' => 'Modified At',
            'image' => 'Image',
            'simple_image' => 'Simple Image',
            'item_image' => 'Item Image',
            'role_image' => 'Role Image',
            'square_image' => 'Square Image',
            'rectangle_image' => 'Rectangle Image',
            'thumbnail' => 'Thumbnail',
            'flag' => 'Flag',
        ];
    }
}
