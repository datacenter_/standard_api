<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "api_setmeal".
 *
 * @property int $id
 * @property int|null $user_id 用户id
 * @property int|null $group_id 权限组id - (只存2，3)
 * @property int|null $is_stale 过期状态 1-正常 2-过期
 * @property string|null $opening_at 开通套餐时间
 * @property string|null $expiration_at 过期时间
 * @property string|null $user_rights 用户权限
 * @property string|null $created_at
 * @property int|null $request_restriction 请求限制(对应码表)
 * @property int|null $is_interface 请求限制(对应码表)
 */
class ApiSetmeal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_setmeal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'group_id', 'is_stale', 'request_restriction', 'is_interface'], 'integer'],
            [['expiration_at', 'opening_at', 'created_at'], 'safe'],
            [['user_rights'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'group_id' => 'Group ID',
            'is_stale' => 'Is Stale',
            'opening_at' => 'Opening At',
            'expiration_at' => 'Expiration At',
            'user_rights' => 'User Rights',
            'created_at' => 'Created At',
            'request_restriction' => 'Request Restriction',
            'is_interface' => 'Is Interface',
        ];
    }
}
