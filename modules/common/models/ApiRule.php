<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "api_rule".
 *
 * @property int $id
 * @property string|null $r_name 权限英文名字
 * @property string|null $r_name_cn 权限名字
 * @property string|null $folder_name 文件名称
 * @property string|null $controller 控制器
 * @property string|null $method 方法
 * @property int|null $status 1：正常  2：禁用
 * @property int|null $order 排序
 */
class ApiRule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_rule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'pid', 'order'], 'integer'],
            [['r_name', 'r_name_cn'], 'string', 'max' => 80],
            [['folder_name', 'controller', 'method'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'r_name' => 'R Name',
            'r_name_cn' => 'R Name Cn',
            'folder_name' => 'Folder Name',
            'controller' => 'Controller',
            'method' => 'Method',
            'status' => 'Status',
            'pid' => 'pid',
            'order' => 'Order',
        ];
    }
}
