<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "api_token".
 *
 * @property int $id
 * @property string|null $token token
 * @property string|null $token_aging 令牌时效
 * @property int|null $is_revoke 是否撤销 1-正常，2-撤销
 * @property int|null $user_id 用户id
 * @property string|null $created_at 创建时间
 * @property string|null $revoke_at 撤销时间
 */
class ApiToken extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_token';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_revoke', 'user_id'], 'integer'],
            [['created_at', 'revoke_at'], 'safe'],
            [['token', 'token_aging'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'token_aging' => 'Token Aging',
            'is_revoke' => 'Is Revoke',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'revoke_at' => 'Revoke At',
        ];
    }
}
