<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "api_personal_certification".
 *
 * @property int $id
 * @property int|null $user_id 用户id
 * @property string|null $name 名
 * @property string|null $surname 姓
 * @property string|null $country 国家
 * @property string|null $province_municipalitie 省份/直辖市
 * @property string|null $city 城市
 * @property string|null $detailed_address 详细地址
 * @property string|null $created_at
 * @property string|null $modified_at
 * @property int|null $is_review 审核是否通过 1-通过 2-未通过 3-待审核
 */
class ApiPersonalCertification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_personal_certification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'is_review', 'country', 'province_municipalitie', 'city'], 'integer'],
            [['created_at', 'modified_at'], 'safe'],
            [['name', 'surname'], 'string', 'max' => 60],
            [['detailed_address'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'country' => 'Country',
            'province_municipalitie' => 'Province Municipalitie',
            'city' => 'City',
            'detailed_address' => 'Detailed Address',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'is_review' => 'Is Review',
        ];
    }
}
