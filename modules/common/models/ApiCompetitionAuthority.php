<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "api_competition_authority".
 *
 * @property int $id
 * @property int|null $user_id 用户id
 * @property int|null $sour_id 赛事id
 * @property int|null $delay_type 延迟类型（1公网 2优势 3私有 4无延迟）
 * @property int|null $data_level 数据等级（1基础 2高级）
 * @property int|null $deleted 删除状态（1-删除 2-正常）
 * @property string|null $created_at
 * @property string|null $modified_at
 * @property string|null $deleted_at
 */
class ApiCompetitionAuthority extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_competition_authority';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'sour_id', 'delay_type', 'data_level', 'deleted'], 'integer'],
            [['created_at','modified_at','deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'sour_id' => 'Sour ID',
            'delay_type' => 'Delay Type',
            'data_level' => 'Data Level',
            'deleted' => 'Deleted',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
