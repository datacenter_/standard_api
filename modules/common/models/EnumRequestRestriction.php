<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "enum_request_restriction".
 *
 * @property int $id
 * @property int|null $request_num 请求数量
 * @property string|null $hour 英文
 * @property string|null $hour_cn 中文
 */
class EnumRequestRestriction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_request_restriction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_num'], 'integer'],
            [['hour', 'hour_cn'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_num' => 'Request Num',
            'hour' => 'Hour',
            'hour_cn' => 'Hour Cn',
        ];
    }
}
