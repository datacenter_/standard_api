<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "api_country".
 *
 * @property int $id
 * @property string|null $name 中文名称
 * @property string|null $e_name 英文名称
 * @property string|null $two 2位缩写
 * @property string|null $three 3位缩写
 * @property string|null $code 国家代码
 * @property int|null $type 类别标志1国家2地区
 * @property string|null $pandascore
 * @property int|null $created_time 添加时间
 * @property string|null $image 图片
 * @property string|null $hltv
 * @property string|null $abios
 * @property string|null $languages
 */
class ApiCountry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'created_time'], 'integer'],
            [['name', 'e_name', 'three', 'hltv'], 'string', 'max' => 200],
            [['two'], 'string', 'max' => 10],
            [['code', 'pandascore', 'abios', 'languages'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'e_name' => 'E Name',
            'two' => 'Two',
            'three' => 'Three',
            'code' => 'Code',
            'type' => 'Type',
            'pandascore' => 'Pandascore',
            'created_time' => 'Created Time',
            'image' => 'Image',
            'hltv' => 'Hltv',
            'abios' => 'Abios',
            'languages' => 'Languages',
        ];
    }
}
