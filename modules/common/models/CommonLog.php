<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "common_log".
 *
 * @property int $id
 * @property string|null $type
 * @property string|null $info
 * @property string|null $source_type
 * @property int|null $source_id
 * @property string|null $created_time
 * @property string|null $modified_at
 */
class CommonLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'common_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['info'], 'string'],
            [['source_id'], 'integer'],
            [['created_time', 'modified_at'], 'safe'],
            [['type', 'source_type'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'info' => 'Info',
            'source_type' => 'Source Type',
            'source_id' => 'Source ID',
            'created_time' => 'Created Time',
            'modified_at' => 'Modified At',
        ];
    }
}
