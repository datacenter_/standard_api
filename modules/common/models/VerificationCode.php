<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "verification_code".
 *
 * @property int $id
 * @property int|null $user_id 用户id
 * @property string|null $email 邮箱
 * @property int|null $code 验证码
 * @property int|null $state 是否使用（1-已使用  2-未使用）
 * @property string $expiration_time 过期时间
 * @property string|null $created_at
 */
class VerificationCode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'verification_code';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'code', 'state'], 'integer'],
            [['email'], 'string', 'max' => 100, 'min' => 1],
            [['expiration_time'], 'required'],
            [['expiration_time', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'email' => 'Email',
            'code' => 'Code',
            'state' => 'State',
            'expiration_time' => 'Expiration Time',
            'created_at' => 'Created At',
        ];
    }
}
