<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "api_setmeal_geme".
 *
 * @property int $id
 * @property int|null $user_id 用户id
 * @property int|null $group_id 权限组id - (只存1，2，3)
 * @property int|null $game_id 游戏id
 * @property int|null $is_stale 过期状态 1-正常 2-过期
 * @property int|null $data_level 数据级别（1-基础 2-高级 3-不提供）
 * @property string|null $opening_at 开通游戏套餐时间
 * @property string|null $expiration_at 到期时间
 * @property string|null $created_at
 */
class ApiSetmealGeme extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_setmeal_geme';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'group_id', 'game_id', 'is_stale', 'data_level'], 'integer'],
            [['expiration_at', 'opening_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'group_id' => 'Group ID',
            'game_id' => 'Game ID',
            'is_stale' => 'Is Stale',
            'data_level' => 'Data Level',
            'opening_at' => 'Opening At',
            'expiration_at' => 'Expiration At',
            'created_at' => 'Created At',
        ];
    }
}
