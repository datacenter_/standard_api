<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "enum_game".
 *
 * @property int $id
 * @property string|null $name 游戏名称
 * @property string|null $e_name 英文名
 * @property string|null $e_short 英文简称
 */
class EnumMatchRule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_match_rule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'name_cn'], 'string'],
            [['group_match', 'out_match','union_match'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'e_name' => 'E Name',
            'e_short' => 'E Short',
        ];
    }
}
