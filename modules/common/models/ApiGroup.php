<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "api_group".
 *
 * @property int $id
 * @property string|null $group_name 权限组名称
 * @property string|null $rights 当前组所有权限
 * @property int|null $delay 延迟时间（秒为单位）
 * @property int|null $is_advanced (1 - 打开高级字段，0 - 关闭高级字段)
 * @property string|null $default_games 设置默认游戏项目
 * @property string|null $group_e_name 权限组名称英文
 * @property int|null $order 排序
 */
class ApiGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rights', 'default_games'], 'string'],
            [['delay', 'is_advanced', 'order'], 'integer'],
            [['group_name'], 'string', 'max' => 80],
            [['group_e_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_name' => 'Group Name',
            'rights' => 'Rights',
            'delay' => 'Delay',
            'is_advanced' => 'Is Advanced',
            'default_games' => 'Default Games',
            'group_e_name' => 'Group E Name',
            'order' => 'Order',
        ];
    }
}
