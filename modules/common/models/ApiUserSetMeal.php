<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "api_user_set_meal".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $set_meal_id
 * @property int|null $renew_set_meal_id
 * @property string|null $set_meal_game_ids
 */
class ApiUserSetMeal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'api_user_set_meal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'set_meal_id', 'renew_set_meal_id'], 'integer'],
            [['set_meal_game_ids'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'set_meal_id' => 'Set Meal ID',
            'renew_set_meal_id' => 'Renew Set Meal ID',
            'set_meal_game_ids' => 'Set Meal Game Ids',
        ];
    }
}
