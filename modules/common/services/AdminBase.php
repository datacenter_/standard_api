<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/12/04
 * Time: 22:13
 */
namespace app\modules\common\services;

use app\modules\admin\forms\EmailForm;
use app\modules\admin\models\Admin;
use app\modules\common\models\ApiToken;
use app\modules\common\models\VerificationCode;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\EntityNotExistException;
use app\rest\exceptions\RequestParamsUnvalidatedException;

class AdminBase
{
    // WEB-API
    const REVOKE_YES = 2;
    const REVOKE_NO = 1;
    const STATUS_NOT_USED = 2;
    const STATUS_USED = 1;
    /*
    * 生成（Token）访问令牌
    * @param int       $length  要生成的随机字符串长度
    * @param string    $type    随机码类型：0 - 数字+大小写字母；
    * @param string    $type              1 - 数字；
    * @param string    $type              2 - 小写字母；
    * @param string    $type              3 - 大写字母；
    * @param string    $type              4 - 特殊字符；
    * @param string    $type             -1 - 数字+大小写字母+特殊字符 (默认)
    * @return string
    */
    public static function getApiToken($uid, $type = -1, $length = 57)
    {
        $token = '';
        $arrayToken = [
            1 => "0123456789",
            2 => "abcdefghijklmnopqrstuvwxyz",
            3 => "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            4 => "-_"
        ];
        switch ($type) {
            case 0:
                array_pop($arrayToken);
                $string = implode("", $arrayToken);
                break;
            case -1:
                $string = implode("", $arrayToken);
                break;
            case 2:
            case 3:
            case 4:
                $string = $arrayToken[$type];
                break;
            default :
                return null;
                break;
        }
        $count = strlen($string) - 1;
        $rand  = rand(26,30);
        for ($i = 0; $i < $length; $i++) {
            if($i == $rand){
                $token .= $uid;
            }
            $token .= $string[rand(0, $count)];
        }
        return $token;
    }

    // 生成token
    public static function addTokenAll($userId) {
        // 默认生成三个token
        $userTokensData = [];
        for ($i = 1; $i <= 3; $i++) {
            $userTokensData[] = [
                'token' => self::getApiToken($userId),
                'is_revoke' => self::REVOKE_NO,
                'user_id' => $userId,
            ];
        }
        if (!empty($userTokensData)) {
            \Yii::$app->db->createCommand()->batchInsert(
                ApiToken::tableName(),
                ['token', 'is_revoke', 'user_id'],
                $userTokensData
            )->execute();
        }
        return;
    }

    // 生成token
    public static function addApiToken($uid) {
        $token = self::getApiToken($uid);
        if($token&&$uid){
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                // 查询当前用户的token个数
                $tokensCount = ApiToken::find()->select(['id'])->where(['user_id'=>$uid])->count();
                if ($tokensCount >= 3){
                    throw new BusinessException([],Consts::MAXIMUM_NUMBER_TOKENS_ERROR);
                }
                // 再新增一条token
                $tokenModel = new ApiToken();
                $tokenModel->setAttributes([
                    'token' => $token,
                    'is_revoke' => self::REVOKE_NO,
                    'user_id' => $uid,
                ]);
                if (!$tokenModel->save()) {
                    throw new BusinessException($tokenModel->getErrors(), Consts::FAILED_TO_GENERATE_TOKEN_ERROR);
                }
                $transaction->commit();
                // redis - 存入token的用户id
                RedisBase::valueSet([ApiConstant::PREFIX_WEB_API,'Token',$token],$uid,true);
                // redis - 存入用户延迟权限组
//                AuthorityDetection::getUserGroupDelay($uid);
            } catch (\Throwable $e) {
                $transaction->rollBack();
                \Yii::warning($e->getMessage());
                throw $e;
            }
        }
        return true;
    }
    // 忘记密码/注册 - 发送验证码
    public static function modifySendToEmail($email,$isRegister=false)
    {
        $form = new EmailForm();
        $form->setAttributes(['email'=>$email]);
        if ($form->validate()) {
            $userInfo = Admin::getByEmail($email);
            if($isRegister){
                if(!empty($userInfo)){
                    throw new BusinessException([],Consts::MAILBOX_ALREADY_EXISTS_ERROR);
                }
            }else{
                if(empty($userInfo)){
                    throw new BusinessException([],Consts::MAILBOX_DOES_NOT_EXIST_ERROR);
                }
            }
            // 验证是否已经邮箱的次数
            $nowDate = date('Y-m-d H:i:s',time());
            $codeCount = VerificationCode::find()->select(['expiration_time'])
                ->where(['user_id'=>0,'email'=>$email,'state'=>self::STATUS_NOT_USED])
                ->andWhere(['>=', "expiration_time", $nowDate])
                ->count();
            if($codeCount >= 3){
                throw new BusinessException([],Consts::VERIFICATION_CODE_REPEATEDLY_ERROR);
            }
            // 生成验证码
            $code = rand(100000, 999999);
            if($email) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {

                    // 发送邮件
                    $mail = self::sendEmail($email, $code);

                    // 验证码set入库
                    $verificationCodeModel = new VerificationCode();
                    $verificationCodeModel->setAttributes([
                        'email' => $email,
                        'code' => $code,
                        'expiration_time' => date("Y-m-d H:i:s", strtotime("+10 minute")),
                    ]);
                    if (!$verificationCodeModel->save()) {
                        throw new BusinessException($verificationCodeModel->getErrors(), Consts::CODE_GENERATION_FAILED_ERROR);
                    }

                    $transaction->commit();

                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    \Yii::warning($e->getMessage());
                    throw $e;
                }
            }else{
                throw new RequestParamsUnvalidatedException([],Consts::EMAIL_PARAMETER_ERROR);
            }
            if($mail)
                return true;
            else
                throw new BusinessException([],Consts::VERIFICATION_CODE_SENDING_ERROR);
        }else{
            throw new RequestParamsUnvalidatedException($form->getErrors());
        }
    }

    // 忘记密码/注册 - 验证验证码
    public static function modifyVerificationCode($attributes)
    {
        $code = $attributes['code'];
        $email = $attributes['email'];
        if($code && $email){
            $isTrue = false;
            $nowDate = date('Y-m-d H:i:s',time());
            $codeInfo = VerificationCode::find()->select(['code'])
                ->where(['user_id'=>0,'email'=>$email,'code'=>$code,'state'=>self::STATUS_NOT_USED])
                ->andWhere(['>=', "expiration_time", $nowDate])
                ->asArray()->one();
            if(!empty($codeInfo)) {
                $codeRes = VerificationCode::updateAll(['state'=>self::STATUS_USED],['and',['user_id'=>0],['email'=>$email],['state'=>self::STATUS_NOT_USED]]);
                if($codeRes){
                    $isTrue = true;
                }
            }else{
                // 数据不存在
                throw new EntityNotExistException([],Consts::VERIFICATION_CODE_IS_INVALID_ERROR);
            }
        }else{
            throw new RequestParamsUnvalidatedException([],Consts::VERIFICATION_CODE_PARAMETER_ERROR);
        }
        if(!$isTrue){
            throw new BusinessException([],Consts::VERIFICATION_CODE_ERROR);
        }
        return true;
    }

    // 登陆后 - 发送验证码
    public static function sendToEmail($email,$uid,$isNewMailbox=false)
    {
        if($isNewMailbox){
            $userInfo = Admin::getByEmail($email);
            if(!empty($userInfo)){
                throw new BusinessException([],Consts::MAILBOX_ALREADY_EXISTS_ERROR);
            }
        }
        // 验证是否已经邮箱的次数
        $nowDate = date('Y-m-d H:i:s',time());
        $codeCount = VerificationCode::find()->select(['expiration_time'])
            ->where(['user_id'=>$uid,'email'=>$email,'state'=>self::STATUS_NOT_USED])
            ->andWhere(['>=', "expiration_time", $nowDate])
            ->count();
        if($codeCount >= 3){
            throw new BusinessException([],Consts::VERIFICATION_CODE_REPEATEDLY_ERROR);
        }
        $code = rand(100000, 999999);
        if($email && $uid) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {

                // 发送验证码
                $mail = self::sendEmail($email, $code);

                // 验证码set入库
                $verificationCodeModel = new VerificationCode();
                $verificationCodeModel->setAttributes([
                    'user_id' => $uid,
                    'email' => $email,
                    'code' => $code,
                    'expiration_time' => date("Y-m-d H:i:s", strtotime("+10 minute")),
                ]);
                if (!$verificationCodeModel->save()) {
                    throw new BusinessException($verificationCodeModel->getErrors(), Consts::CODE_GENERATION_FAILED_ERROR);
                }

                $transaction->commit();

            } catch (\Throwable $e) {
                $transaction->rollBack();
                \Yii::warning($e->getMessage());
                // 发送验证码失败
                throw new BusinessException([], Consts::VERIFICATION_CODE_SENDING_ERROR);
            }
        }else{
            throw new RequestParamsUnvalidatedException([],Consts::EMAIL_PARAMETER_ERROR);
        }
        if ($mail) {
            return $code;
        } else {
            throw new BusinessException([], Consts::VERIFICATION_CODE_SENDING_ERROR);
        }
    }

    // 登陆后 - 验证验证码
    public static function verificationCode($code,$uid)
    {
        if($code && $uid){
            $isTrue = false;
            $nowDate = date('Y-m-d H:i:s',time());
            $codeInfo = VerificationCode::find()->select(['code'])
                ->where(['user_id'=>$uid,'code'=>$code,'state'=>self::STATUS_NOT_USED])
                ->andWhere(['>=', "expiration_time", $nowDate])
                ->asArray()->one();
            if(!empty($codeInfo)) {
                $codeRes = VerificationCode::updateAll(['state'=>self::STATUS_USED],['and',['user_id'=>$uid],['state'=>self::STATUS_NOT_USED]]);
                if($codeRes){
                    $isTrue = true;
                }
            }else{
                // 数据不存在
                throw new EntityNotExistException([],Consts::VERIFICATION_CODE_IS_INVALID_ERROR);
            }
        }else{
            throw new RequestParamsUnvalidatedException([],Consts::VERIFICATION_CODE_PARAMETER_ERROR);
        }
        if(!$isTrue){
            throw new BusinessException([],Consts::VERIFICATION_CODE_ERROR);
        }
        return true;
    }

//    // 存入cookie
//    public static function setCookies($attributes)
//    {
//        $cookies = Yii::$app->response->cookies;
//        // add a new cookie to the response to be sent
//        return $cookies->add(new \yii\web\Cookie($attributes));
//    }
//    // 取出cookie
//    public static function getCookies($field = 'user_id')
//    {
//        $cookies = Yii::$app->request->cookies;
//        // get the cookie value
//        if ($cookies->has($field)) {
//            $uid = $cookies->getValue($field);
//        }
//        return $uid ?? null;
//    }
//    // 删除cookie
//    public static function delCookies($field = 'user_id')
//    {
//        $cookies = Yii::$app->response->cookies;
//        $cookies->remove($field);
//        unset($cookies[$field]);
//    }
    /**
     * @param $email
     * @param int $code
     * @return bool
     */
    public static function sendEmail($email, int $code): bool
    {
        for ($i=0;$i<4;$i++) {
            try {
                // 发送验证码
                $mail = \Yii::$app->mailer->compose()
                    ->setFrom([env('MAILER_USERNAME') => env('MAILER_ADDRESSER')])
                    ->setTo($email)
                    ->setSubject(env('MAILER_TITLE'))
                    //发布可以带html标签的文本
                    ->setHtmlBody("Verification Code：{$code}")
                    ->send();
                return true;
            } catch (\Throwable $e) {
                continue;
            }
        }
        throw new BusinessException([],Consts::VERIFICATION_CODE_SENDING_ERROR);
    }
}
