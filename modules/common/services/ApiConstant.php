<?php

namespace app\modules\common\services;

class ApiConstant
{
    /**
     * API
     */
    const PREFIX_API = 'Api';
    /**
     * WEB-API
     */
    const PREFIX_WEB_API = 'Web-Api';
    /**
     * 用户权限组类型
     */
    const GROUP_INTERNAL_ID = 1;
    const GROUP_INTERNAL_VERSION = '内部版';
    const GROUP_INTERNAL_VERSION_DELAY = 0;

    const GROUP_MAJOR_ID = 2;
    const GROUP_MAJOR_VERSION = '专业版';
    const GROUP_MAJOR_VERSION_DELAY = 10;
    const GROUP_MAJOR_PRO_ID = 3;
    const GROUP_MAJOR_PRO_VERSION = '专业版-加强';
    const GROUP_MAJOR_PRO_VERSION_DELAY = 10;

    const GROUP_BASIC_ID = 4;
    const GROUP_BASIC_VERSION = '基础版';
    const GROUP_BASIC_VERSION_DELAY = 0;
    const GROUP_BASIC_PRO_ID = 5;
    const GROUP_BASIC_PRO_VERSION = '基础版-加强';
    const GROUP_BASIC_PRO_VERSION_DELAY = 0;

    const GROUP_FREE_ID = 6;
    const GROUP_FREE_VERSION = '免费版';
    const GROUP_FREE_VERSION_DELAY = 150;

    const NEW_USER_VERSION_ID = 7; // 新用户版本ID

    /**
     * 游戏数据级别(公用)
     */
    const GAME_DATA_LEVEL_BASIC = 1;        // 基础
    const GAME_DATA_LEVEL_ADVANCED = 2;     // 高级
    const GAME_DATA_LEVEL_UNSUPPORTED = 3;  // 不提供

    /**
     * 是否为私有赛事 / 是否设有私有延迟
     */
    const PRIVATE_TOURNAMENT_YES = 1; // 是
    const PRIVATE_TOURNAMENT_NO = 2;  // 否
    /**
     * 私有延迟类型
     */
    const PRIVATE_PUBLIC_TOURNAMENT_DELAY = 1;      // 公网
    const PRIVATE_ADVANTAGE_TOURNAMENT_DELAY = 2;   // 优势
    const PRIVATE_PRIVATE_TOURNAMENT_DELAY = 3;     // 私有
    const PRIVATE_UNLIMITED_TOURNAMENT_DELAY = 4;   // 无延迟
    /**
    /**
     * 公开延迟类型
     */
    const PU_PUBLIC_TOURNAMENT_DELAY = 1;      // 公网
    const PU_ADVANTAGE_TOURNAMENT_DELAY = 2;   // 优势
    const PU_UNLIMITED_TOURNAMENT_DELAY = 3;   // 无延迟
    const PU_DEFAULT_TOURNAMENT_DELAY = 4;     // 平台默认
    /**
     * 公开数据级别
     */
    const PU_BASIC_TOURNAMENT_DATA = 1;       // 基础
    const PU_ADVANCED_TOURNAMENT_DATA = 2;    // 高级
    const PU_UNLIMITED_TOURNAMENT_DATA = 3;   // 不提供
    const PU_DEFAULT_TOURNAMENT_DATA= 4;      // 平台默认

    /**
     * 未撤销token状态 (正常状态)
     */
    const IS_REVOKE = 1;
    /**
     * 超级token
     */
    const WEB_SOCKET_TOKEN_KEY = 'SUPER-ENCRYPTION-TOKEN-SECRET-KEY';
}