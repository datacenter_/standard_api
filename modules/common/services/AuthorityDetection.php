<?php

namespace app\modules\common\services;

use app\modules\admin\models\Admin;
use app\modules\common\models\ApiCompetitionAuthority;
use app\modules\common\models\ApiGroup;
use app\modules\common\models\ApiRule;
use app\modules\common\models\ApiSetmeal;
use app\modules\common\models\ApiSetmealGeme;
use app\modules\common\models\ApiToken;
use app\rest\exceptions\ProhibitException;
use app\rest\exceptions\UnauthorizedException;

class AuthorityDetection
{
    const EDITION = 'v1/';
    // 新版权限设置
    // 根据token验证权限
    public static function authorityProvPro($token)
    {
        // 验证token是否合法
        // 获取userId
        $userId = self::getAuthUserId($token);

        // 记录log用户信息数据
        \Yii::$app->params['user_info'] = [
            'user_id' => $userId,
            'token' => $token,
            'user_type' => null,
            'parameter' => self::parameterProcessing(), //参数
            'node' => self::nodeProcessing() //节点
        ];

        // 验证白名单
        // 验证请求限制

        // 验证套餐
        self::versionPackage($userId);
    }

    // 验证版本套餐
    public static function versionPackage($userId,$isRollBack=false)
    {
        // 检测用户套餐状态是否存在
        $userSetMealKey = RedisBase::issetKey([ApiConstant::PREFIX_WEB_API,'SetMeal',$userId]);
        if ($userSetMealKey) {
            $userSetMealJson = RedisBase::valueGet([ApiConstant::PREFIX_WEB_API,'SetMeal',$userId]);
            $userSetMeal = json_decode($userSetMealJson,true);
            $userGroupId = $userSetMeal['group_id'];
            $userRightJson = $userSetMeal['user_rights'];
            // 设置高级字段
            // 设置版本组
            // 验证权限
            // 设置游戏套餐
            // 设置私有赛事
            self::userGroupAuthority($userId,$userGroupId,$userRightJson);
        } else {
            $nowTime = time();
            $nowDate = date("Y-m-d H:i:s", $nowTime);
            // 用户套餐数据
            $userSetMealInfo = ApiSetmeal::find()->select(['group_id','user_rights'])
                ->where(['user_id' => $userId])
                ->andWhere(['<=', "opening_at", $nowDate])
                ->andWhere(['>=', "expiration_at", $nowDate])
                ->asArray()->one();
            if (!empty($userSetMealInfo)) {
                $userGroupId = $userSetMealInfo['group_id'];
                $userRightJson = $userSetMealInfo['user_rights'];
                $redisInfo = [
                    'group_id' => $userGroupId,
                    'user_rights' => $userRightJson,
                ];
                // 设置高级字段
                // 设置版本组
                // 验证权限
                // 设置游戏套餐
                // 设置私有赛事
                self::userGroupAuthority($userId,$userGroupId,$userRightJson);
                // 当前用户套餐group_id存入redis
                RedisBase::valueSet([ApiConstant::PREFIX_WEB_API, 'SetMeal', $userId], json_encode($redisInfo), false, true);
            } else {
                if ($isRollBack) {
                    // 未经授权
                    throw new UnauthorizedException();
                }
                self::versionPackage($userId, true);
            }
        }
    }

    /**
     * 用户根据套餐id识别一系列权限
     */
    public static function userGroupAuthority($userId,$userGroupId,$userRightsJson)
    {
        // 记录log
        \Yii::$app->params['user_info']['user_type'] = $userGroupId;
        // 设置私有赛事权限
        self::getUserPrivateTournament($userId);
        // 版本组数据
        $userGroupInfo = self::getAuthGroupInfo($userGroupId);
        // 版本是否开启高级字段
        $userAdvanced = intval($userGroupInfo['is_advanced']);
        // 默认套餐游戏
        $defaultGames = json_decode($userGroupInfo['default_games'],true);
        switch ($userGroupId) {
            // 内部版
            case ApiConstant::GROUP_INTERNAL_ID:
                // 设置版本组
                self::choiceRoleGroupSetting($userGroupId);
                // 不用验证权限/套餐/赛事权限
                break;
            // 专业版
            case ApiConstant::GROUP_MAJOR_ID:
            // 专业版 - 加强
            case ApiConstant::GROUP_MAJOR_PRO_ID:
                // 设置版本组
                self::choiceRoleGroupSetting($userGroupId);
                // 验证游戏套餐
                self::versionGamesPackage($userId,$userAdvanced,$defaultGames);
                // 验证权限
                self::authentication($userRightsJson);
                break;
            // 基础版
            case ApiConstant::GROUP_BASIC_ID:
            // 基础版 - 加强
            case ApiConstant::GROUP_BASIC_PRO_ID:
            // 免费版
            case ApiConstant::GROUP_FREE_ID:
                // 设置版本组
                self::choiceRoleGroupSetting($userGroupId);
                // 验证权限
                self::authentication($userRightsJson);
                // 验证游戏套餐
                self::versionGamesPackage($userId,$userAdvanced,$defaultGames);
                break;
            default:
                // 未经授权
                throw new UnauthorizedException();
        }
    }

    // 验证版本游戏套餐
    public static function versionGamesPackage($userId,$advanced,$defaultGames=[])
    {
        // 检测用户游戏套餐状态是否存在
        $userSetMealGamesKey = RedisBase::issetKey([ApiConstant::PREFIX_WEB_API,'SetMeal',$userId,'Games']);
        if ($userSetMealGamesKey) {
            $userSetMealGamesJson = RedisBase::valueGet([ApiConstant::PREFIX_WEB_API,'SetMeal',$userId,'Games']);
            $setGameInfo = json_decode($userSetMealGamesJson,true);
            $gameIds = $setGameInfo['game_ids'];
            // 所有有权限的游戏id
            $setGameIds = array_merge($defaultGames,$gameIds);
            // 所有游戏的数据等级
            $userSetMealGamesAdvanced = $setGameInfo['game_advanced'];
        } else {
            $nowTime = time();
            $nowDate = date("Y-m-d H:i:s", $nowTime);
            // 用户套餐游戏数据
            $userSetMealGames = ApiSetmealGeme::find()->select(['game_id','data_level'])
                ->where(['user_id' => $userId])
                ->andWhere(['in','data_level',[ApiConstant::GAME_DATA_LEVEL_BASIC,ApiConstant::GAME_DATA_LEVEL_ADVANCED]])
                ->andWhere(['and',['<=', "opening_at", $nowDate],['>=', "expiration_at", $nowDate]])
                ->asArray()->all();
            // 所有游戏的数据等级
            $userSetMealGamesAdvanced=[];
            // 默认游戏设置
            foreach ($defaultGames as $defaultGid) {
                $userSetMealGamesAdvanced[$defaultGid] = $advanced ? true:false;
            }
            if (!empty($userSetMealGames)) {
                $gameIds = array_column($userSetMealGames, 'game_id');
                // 所有有权限的游戏id
                $setGameIds = array_merge($defaultGames, $gameIds);
                // 购买的游戏设置
                foreach ($userSetMealGames as &$setMealGame) {
                    $userSetMealGamesAdvanced[$setMealGame['game_id']] = $setMealGame['data_level']==ApiConstant::GAME_DATA_LEVEL_ADVANCED ? true:false;
                }
                // 存redis结果数据
                $setGameRedisInfo = [
                    'game_ids' => $gameIds,
                    'game_advanced' => $userSetMealGamesAdvanced,
                ];
            } else {
                // 走默认游戏
                $setGameIds = $defaultGames;
                // 存redis结果数据
                $setGameRedisInfo = [
                    'game_ids' => [],
                    'game_advanced' => $userSetMealGamesAdvanced,
                ];
            }
            // 用户游戏套餐 存入redis
            RedisBase::valueSet([ApiConstant::PREFIX_WEB_API, 'SetMeal', $userId, 'Games'], json_encode($setGameRedisInfo), false, true);
        }
        // 设置游戏套餐-有权限的游戏id
        self::gamesIdSettings($setGameIds);
        // 设置游戏的数据等级
        self::advancedSettings($userSetMealGamesAdvanced);
    }

    // 设置advanced高级字段
    public static function gamesIdSettings(array $setGameIds=[], String $field='game_ids')
    {
        \Yii::$app->params[$field]=array_unique($setGameIds);
    }
    // 设置advanced高级字段 - 所有游戏的数据等级
    public static function advancedSettings(array $advancedArray=[], String $field='advanced')
    {
        \Yii::$app->params[$field]=$advancedArray;
    }
    // 设置是否存在私有赛事
    public static function privateTournamentSettings(bool $bool=true, array $privateTournaments=[], String $field='is_private_tournament')
    {
        \Yii::$app->params[$field]=$bool;
        if ($bool) {
            // 存在私有赛事的数据
            \Yii::$app->params['private_tournament']=$privateTournaments;
        }
    }
    // 设置group版本组
    public static function roleGroupSettings(array $base=[], String $field='group')
    {
        \Yii::$app->params[$field]=['group_id'=>$base[0],'type'=>$base[1],'delay'=>$base[2]];
    }
    // 选择性设置group版本组
    public static function choiceRoleGroupSetting($groupId=null)
    {
        switch ($groupId) {
            case ApiConstant::GROUP_INTERNAL_ID:
                $base=[ApiConstant::GROUP_INTERNAL_ID,ApiConstant::GROUP_INTERNAL_VERSION,ApiConstant::GROUP_INTERNAL_VERSION_DELAY];
                break;
            case ApiConstant::GROUP_FREE_ID:
                $base=[ApiConstant::GROUP_FREE_ID,ApiConstant::GROUP_FREE_VERSION,ApiConstant::GROUP_FREE_VERSION_DELAY];
                break;
            case ApiConstant::GROUP_BASIC_ID:
                $base=[ApiConstant::GROUP_BASIC_ID,ApiConstant::GROUP_BASIC_VERSION,ApiConstant::GROUP_BASIC_VERSION_DELAY];
                break;
            case ApiConstant::GROUP_BASIC_PRO_ID:
                $base=[ApiConstant::GROUP_BASIC_PRO_ID,ApiConstant::GROUP_BASIC_PRO_VERSION,ApiConstant::GROUP_BASIC_PRO_VERSION_DELAY];
                break;
            case ApiConstant::GROUP_MAJOR_ID:
                $base=[ApiConstant::GROUP_MAJOR_ID,ApiConstant::GROUP_MAJOR_VERSION,ApiConstant::GROUP_MAJOR_VERSION_DELAY];
                break;
            case ApiConstant::GROUP_MAJOR_PRO_ID:
                $base=[ApiConstant::GROUP_MAJOR_PRO_ID,ApiConstant::GROUP_MAJOR_PRO_VERSION,ApiConstant::GROUP_MAJOR_PRO_VERSION_DELAY];
                break;
            default:
                // 未经授权
                throw new UnauthorizedException();
        }
        self::roleGroupSettings($base);
    }

    // 根据用户Id获取当前用户的私有赛事
    public static function getUserPrivateTournament($userId)
    {
        // 从redis获取token的用户id
        $userPrivateTournamentKey = RedisBase::issetKey([ApiConstant::PREFIX_WEB_API,'Private-Tournament',$userId]);
        if ($userPrivateTournamentKey) {
            $userPrivateTournamentJson = RedisBase::valueGet([ApiConstant::PREFIX_WEB_API,'Private-Tournament',$userId]);
            $privateTournamentsArray = json_decode($userPrivateTournamentJson,true);
        } else {
            $privateTournamentsArray = [];
            $privateTournaments = ApiCompetitionAuthority::find()->select(['sour_id', 'delay_type', 'data_level'])
                ->where(['user_id' => $userId, 'deleted' => 2])->asArray()->all();
            foreach ($privateTournaments as $privateTournamentInfo) {
                $currentSourId = $privateTournamentInfo['sour_id'];
                unset($privateTournamentInfo['sour_id']);
                $privateTournamentsArray[$currentSourId] = $privateTournamentInfo;
            }
            // 存入redis
            RedisBase::valueSet([ApiConstant::PREFIX_WEB_API, 'Private-Tournament', $userId], json_encode($privateTournamentsArray), false, true);
        }
        empty($privateTournamentsArray) ? self::privateTournamentSettings(false):self::privateTournamentSettings(true,$privateTournamentsArray);
    }

    // 根据token获取用户Id
    public static function getAuthUserId($token)
    {
        // 从redis获取token的用户id
        $userTokenKey = RedisBase::issetKey([ApiConstant::PREFIX_WEB_API,'Token',$token]);
        if ($userTokenKey) {
            $userId = RedisBase::valueGet([ApiConstant::PREFIX_WEB_API,'Token',$token]);
        } else {
            $tokenUser = ApiToken::find()->select(['user_id'])
                ->where(['token' => $token, 'is_revoke' => ApiConstant::IS_REVOKE])
                ->asArray()->one();
            if (!empty($tokenUser)) {
                $userId = $tokenUser['user_id'];
                // token存入redis
                RedisBase::valueSet([ApiConstant::PREFIX_WEB_API,'Token', $token],$userId,true);
            } else {
                // 未经授权
                throw new UnauthorizedException();
            }
        }

        return $userId;
    }
    // 根据userId获取groupId
    public static function getAuthGroupId($userId)
    {
        // 检测User-Group-Id redis是否存在
        $userGroupIdKey = RedisBase::issetKey([ApiConstant::PREFIX_WEB_API,'Group',$userId]);
        if ($userGroupIdKey) {
            $userGroupId = RedisBase::valueGet([ApiConstant::PREFIX_WEB_API,'Group',$userId]);
        } else {
            $nowTime = time();
            $nowDate = date("Y-m-d H:i:s", $nowTime);
            // 用户套餐数据
            $userGroupIdInfo = ApiSetmeal::find()->select(['group_id'])
                ->where(['user_id' => $userId])
                ->andWhere(['<=', "opening_at", $nowDate])
                ->andWhere(['>=', "expiration_at", $nowDate])
                ->asArray()->one();
            if (!empty($userGroupIdInfo)) {
                // 用户 - Group_id
                $userGroupId = $userGroupIdInfo['group_id'];
                // 存入 - Group_id - redis
                RedisBase::valueSet([ApiConstant::PREFIX_WEB_API,'Group', $userId],$userGroupId,false,true);
            } else {
                // 未经授权
                throw new UnauthorizedException();
            }
        }

        return $userGroupId;
    }
    // 根据groupId获取权限组数据
    public static function getAuthGroupInfo($userGroupId)
    {
        // 检测权限组数据key是否存在
        $groupInfoKey = RedisBase::issetKey([ApiConstant::PREFIX_WEB_API,'Group','Info',$userGroupId]);
        if ($groupInfoKey) {
            // 获取当前用户权限组数据
            $groupInfoJson = RedisBase::valueGet([ApiConstant::PREFIX_WEB_API,'Group','Info',$userGroupId]);
            $userGroupInfo = json_decode($groupInfoJson,true);
        } else {
            $userGroupInfo = ApiGroup::find()->select(['id','group_name','group_e_name','is_advanced','default_games'])
                ->where(['id'=>$userGroupId])->asArray()->one();
            if (!empty($userGroupInfo)) {
                RedisBase::valueSet([ApiConstant::PREFIX_WEB_API,'Group','Info',$userGroupId],json_encode($userGroupInfo),false,true);
            } else {
                // 未经授权
                throw new UnauthorizedException();
            }
        }

        return $userGroupInfo;
    }
    // 权限验证
    public static function authentication($ruleIdsJson)
    {
        // 获取真实路由
        $hereRoute    = \Yii::$app->controller->route;
        // 所有权限Id
        $ruleIdsArray = json_decode($ruleIdsJson,true);
        // 检测路由id-Key是否存在
        $currentRoute = str_replace('/','-',$hereRoute);
        $currentRouteKey = RedisBase::issetKey([ApiConstant::PREFIX_WEB_API,$currentRoute]);
        if($currentRouteKey){
            // 获取路由Id
            $routeId = RedisBase::valueGet([ApiConstant::PREFIX_WEB_API,$currentRoute]);
        }else{
            // 重新获取路由Id
            $routeArray = explode('/',$hereRoute);
            $routeInfo  = ApiRule::find()->select(['id','folder_name','controller','method'])
                ->where(['folder_name'=>$routeArray[1],'controller'=>$routeArray[2],'method'=>$routeArray[3]])
                ->asArray()->one();
            if(!empty($routeInfo)){
                $routeId = $routeInfo['id'];
                unset($routeInfo['id']);
                // 重新set-redis
                $currentRoute = str_replace('/','-',self::EDITION.implode('/', $routeInfo));
                RedisBase::valueSet([ApiConstant::PREFIX_WEB_API,$currentRoute],$routeId,false,true);
            }else{
                // 该用户没有权限，禁止操作
                throw new ProhibitException();
            }
        }
        if(!in_array($routeId,$ruleIdsArray)){
            // 该用户没有权限，禁止操作
            throw new ProhibitException();
        }
    }

    // socket 根据userid获取版本组和延迟
    public static function getUserGroupDelay($userId)
    {
        $currentUserDelayKey = RedisBase::issetKey([ApiConstant::PREFIX_WEB_API, 'User', $userId]);
        if ($currentUserDelayKey) {
            // 从redis获取用户延迟权限组
            $userDelayJson = RedisBase::valueGet([ApiConstant::PREFIX_WEB_API, 'User', $userId]);
            $userGroupInfo = json_decode($userDelayJson,true);
        } else {
            // 获取权限组
            $userGroupInfo = Admin::find()->alias('admin')
                ->select(['api_group.group_name', 'api_group.delay'])
                ->leftJoin('api_group', 'admin.group_id = api_group.id')
                ->where(['admin.id' => $userId])
                ->asArray()->one();
            // 用户权限组存入redis
            if (!empty($userGroupInfo)) {
                RedisBase::valueSet([ApiConstant::PREFIX_WEB_API, 'User', $userId],json_encode($userGroupInfo),true);
            }
        }

        return $userGroupInfo;
    }

    // 参数
    public static function parameterProcessing()
    {
        $getParameters = \Yii::$app->request->get();
        unset($getParameters['token']);
        if(!empty($getParameters)){
            $parameterStr = "";
            foreach ($getParameters as $key => $fields) {
                if (strstr($key,'/v1/')) {
                    continue;
                }
                if (is_array($fields)) {
                    $fieldStr = "";
                    foreach ($fields as $k => $field) {
                        $fieldStr .= "{$k}={$field},";
                    }
                    $fieldStr = rtrim($fieldStr, ',');
                    $parameterStr .= "{$key}[{$fieldStr}],";
                } else {
                    $parameterStr .= "{$key}={$fields},";
                }
            }
            $parameterString = rtrim($parameterStr,',');
        }else{
            $parameterString = "";
        }
        return $parameterString;
    }
    // 节点
    public static function nodeProcessing()
    {
        $pathInfo = \Yii::$app->request->getPathInfo();
        $nodesInfo = explode('/',$pathInfo);
        return $nodesInfo[0]."/".$nodesInfo[1];
    }



    // groupId识别name（没用）
    public static function choiceGroupIdToName($groupId=null)
    {
        switch ($groupId) {
            case ApiConstant::GROUP_INTERNAL_ID:
                $groupName=ApiConstant::GROUP_INTERNAL_VERSION;
                break;
            case ApiConstant::GROUP_FREE_ID:
                $groupName=ApiConstant::GROUP_FREE_VERSION;
                break;
            case ApiConstant::GROUP_BASIC_ID:
                $groupName=ApiConstant::GROUP_BASIC_VERSION;
                break;
            case ApiConstant::GROUP_BASIC_PRO_ID:
                $groupName=ApiConstant::GROUP_BASIC_PRO_VERSION;
                break;
            case ApiConstant::GROUP_MAJOR_ID:
                $groupName=ApiConstant::GROUP_MAJOR_VERSION;
                break;
            case ApiConstant::GROUP_MAJOR_PRO_ID:
                $groupName=ApiConstant::GROUP_MAJOR_PRO_VERSION;
                break;
            default:
                // 未经授权
                throw new UnauthorizedException();
        }
        return $groupName;
    }
}
