<?php
/**
 *
 */

namespace app\modules\common\services;

use Elasticsearch\ClientBuilder;

class EsService
{
    private $client;
    private static $_instance;
    private function __construct()
    {
        $this->client = ClientBuilder::create()->setHosts([
            [
                'host'   => env('ES_HOST'),
                'port'   => env('ES_PORT'),
                'scheme' => env('ES_SCHEME'),
                'user'   => env('ES_USER'),
                'pass'   => env('ES_PASS')
            ]
        ])->setConnectionPool('\Elasticsearch\ConnectionPool\SimpleConnectionPool')
            ->setRetries(10)->build();
    }

    // 私有属性的克隆方法 防止被克隆
    public function __clone(){
        trigger_error('Clone is not allowed !');
    }

    public static function initConnect()
    {
        if(! (self::$_instance instanceof self) )
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function init()
    {
        return self::initConnect()->getClent();
    }

    public function getClent()
    {
        return $this->client;
    }

    // 删除索引
    public function deleteIndex($indexName)
    {
        $indexParam = [
            'index' => $indexName
        ];
        $index = $this->init()->indices()->exists($indexParam);
        if($index){
            $this->init()->indices()->delete($indexParam);
        }
        return true;
    }

    // 查看索引
    public function searchIndex($indexName)
    {
        $indexParam = [
            'index' => $indexName
        ];
        return $this->init()->indices()->exists($indexParam);
    }

    // 获取某条数据
    public function getFile($indexName,$apiId)
    {
        try {
            $params = [
                'index' => $indexName,
                'id' => (int)$apiId
            ];
            $info = $this->init()->get($params);
            return $info['_source'];
        } catch (\Exception $e) {
            return null;
        }
    }

    // 获取文档id数据
    public function getStrIdFile($indexName,$apiId)
    {
        try {
            $params = [
                'index' => $indexName,
                'type' => "_doc",
                'id' => $apiId
            ];
            return $this->init()->getSource($params);
        } catch (\Exception $e) {
            return null;
        }
    }

    // 搜索条件的所有数据
    public function searchAll($indexName,$field,$apiId,$columns=[])
    {
        $params['index'] = $indexName;
        $params['body']['query']['match'][$field] = $apiId;
        $params['body']['_source'] = $columns;
        $infos = $this->init()->search($params);
        $listInfos = [];
        foreach ($infos['hits']['hits'] as $k=>$dataInfo) {
            $listInfos[] = $dataInfo['_source'];
        }
        return $listInfos;
    }

    //es搜索
    public function search($index_info,$conditions = array(), $offset = 0, $limit = 20, $columns = array(), $order=[])
    {
        //match 模糊匹配多个
        //term是代表完全匹配  比如数字，日期，布尔值或 not_analyzed 的字符串
        //bool联合查询: must,should,must_not
        $params = [
            "track_total_hits" => true, // 让10000条以后可以显示
            '_source' => $columns,
            'from'    => $offset,
            'size'    => $limit,
        ];
        // 排序
        if(isset($order['field'])&&isset($order['sort'])){
            $params['sort'] = [
                $order['field'] => [
                    'order' => $order['sort'],
                ],
            ];
        }else{
            if(!empty($order)){
                $params['sort'] = $order;
            }
        }
        // 筛选
        if (!empty($conditions)) {
            foreach ($conditions as $v) {
                switch ($v['type']) {
                    case 'between':
                        $params['query']['bool']['must'][]['range'][$v['field']]['gte'] = $v['value'][0];
                        $params['query']['bool']['must'][]['range'][$v['field']]['lte'] = $v['value'][1];
                        break;
                    case '>':
                        $params['query']['bool']['must'][]['range'][$v['field']]['gt'] = $v['value'];
                        break;
                    case '>=':
                        $params['query']['bool']['must'][]['range'][$v['field']]['gte'] = $v['value'];
                        break;
                    case '<':
                        $params['query']['bool']['must'][]['range'][$v['field']]['lt'] = $v['value'];
                        break;
                    case '<=':
                        $params['query']['bool']['must'][]['range'][$v['field']]['lte'] = $v['value'];
                        break;
                    case '=':
                        $params['query']['bool']['must'][]['match'][$v['field']] = $v['value'];
                        break;
                    case 'filter':
                        $params['query']['bool']['filter'][]['term'][$v['field']] = $v['value'];
                        break;
                    case '==':
                        $params['query']['bool']['must'][]['terms'][$v['field']] = $v['value'];
                        break;
                    case '===':
                        // 查询子数组的值
                        $params['query']['bool']['must'][]['term'][$v['field']]['value'] = $v['value'];
                        break;
                    case '!=':
                        $params['query']['bool']['must_not'][]['term'][$v['field']] = $v['value'];
                        break;
                    case 'in':
                        if (!empty($v['value']) && is_array($v['value'])) {
                            foreach ($v['value'] as $m => $n) {
                                $params['query']['bool']['must'][] = array(
                                    'term' => array(
                                        $v['field'] => $n,
                                    ),
                                );
                            }
                        }
                        break;
                    case 'not in':
                        if (!empty($v['value']) && is_array($v['value'])) {
                            foreach ($v['value'] as $m => $n) {
                                $params['query']['bool']['must_not'][] = array(
                                    'term' => array(
                                        $v['field'] => $n,
                                    ),
                                );
                            }
                        }
                        break;
                    case 'like':
                        $params['query']['bool']['must'][]['wildcard'][$v['field']] = "*".$v['value']."*";
                        break;
                    default:
                        return false;
                }
            }
        }
//        print_r($params);die;
        $searchParams = [
            'index'  => $index_info,
            'body' => $params,
            'client' => [
                'timeout'         => 10,
                'connect_timeout' => 10
            ]
        ];
        $searchResponse = $this->init()->search($searchParams);
        //$res            = json_decode($searchResponse, true);
        $reData['list'] = [];
        if (!empty($searchResponse['hits']['hits'])) {
            foreach ($searchResponse['hits']['hits'] as $k => $v) {
                $reData['list'][$k] = $v['_source'];
            }
        }
        $reData['count'] = !empty($searchResponse['hits']['total']['value']) ? $searchResponse['hits']['total']['value'] : 0;
        return $reData;
    }
}
