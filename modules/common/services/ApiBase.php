<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/9/9
 * Time: 22:13
 */
namespace app\modules\common\services;

use app\rest\exceptions\ProhibitException;
use app\rest\exceptions\UnauthorizedException;

class ApiBase
{
    // API
    const PAGE = 1;
    const PAGE_MAX = 10000;
    const PER_PAGE = 50;
    const PER_MAX_PAGE = 200;
    // 搜索组
    const PARAMETER_FILTER = 'filter';
    const PARAMETER_RANGE = 'range';
    const PARAMETER_SEARCH = 'search';
    const PARAMETER_SORT = 'sort';
    // 游戏
    const GAME_TYPE_CSGO = 1;
    const GAME_TYPE_LOL  = 2;
    const GAME_TYPE_DOTA = 3;

    // 参数过滤
    public static function issetParameterFilter($params)
    {
        if(isset($params['page']) && $params['page']){
            $page = $params['page'];
            if(preg_match("/^\d*$/",$page)){
                if($page<self::PAGE){
                    $params['page'] = self::PAGE;
                } elseif ($page>self::PAGE_MAX){
                    $params['page'] = self::PAGE_MAX;
                }else{
                    $params['page'] = $page;
                }
            }else{
                $params['page'] = self::PAGE;
            }
        }else{
            $params['page'] = self::PAGE;
        }
        if(isset($params['per_page']) && $params['per_page']){
            $perPage = $params['per_page'];
            if(preg_match("/^\d*$/",$perPage)){
                if($perPage<1){
                    $params['per_page'] = self::PER_PAGE;
                } elseif ($perPage>self::PER_MAX_PAGE){
                    $params['per_page'] = self::PER_MAX_PAGE;
                }else{
                    $params['per_page'] = $perPage;
                }
            }else{
                $params['per_page'] = self::PER_PAGE;
            }
        }else{
            $params['per_page'] = self::PER_PAGE;
        }
        $searchInfo = [];
        $sortInfo = [];
        foreach ($params as $key=>$value){
            switch ($key){
                case self::PARAMETER_FILTER:
                    $searchInfo[$key] = self::getFieldName($value,'==');
                    break;
                case self::PARAMETER_RANGE:
                    foreach ($value as $k=>$val){
                        if(strstr($val,',')) {
                            $dateTimeInfo = explode(",", $val);
                            $dateTimes = array_filter($dateTimeInfo);
                            $dateTimesCount = count($dateTimes);
                            if($dateTimesCount) {
                                if ($dateTimesCount == 1) {
                                    if (strtotime($dateTimes[0])) {
                                        $dates = [$dateTimes[0], $dateTimes[0]];
                                        $searchInfo[$key][] = ['field' => $k, 'type' => 'between', 'value' => $dates];
                                    }
                                }
                                if ($dateTimesCount >= 2){
                                    if (strtotime($dateTimes[0]) && strtotime($dateTimes[1])) {
                                        $startDateTime = date('Y-m-d H:i:s',strtotime($dateTimes[0]));
                                        $endDateTime = date('Y-m-d H:i:s',strtotime($dateTimes[1]));
                                        $dates = [$startDateTime,$endDateTime];
                                        $searchInfo[$key][] = ['field' => $k, 'type' => 'between', 'value' => $dates];
                                    }
                                }
                            }
                        }else{
                            if(strtotime($val)){
                                $dateTime = date('Y-m-d H:i:s',strtotime($val));
                                $dates = [$dateTime,$dateTime];
                                $searchInfo[$key][] = ['field' => $k, 'type' => 'between', 'value' => $dates];
                            }
                        }
                    }
                    break;
                case self::PARAMETER_SEARCH:
                    $searchInfo[$key] = self::getFieldName($value,'like');
                    break;
                case self::PARAMETER_SORT:
                    foreach ($value as $k=>$val){
                        $sort = strtolower($val);
                        if(in_array($sort,['asc','desc'])) {
                            $sortInfo[$key][] = [
                                $k => [
                                    'order' => $sort,
                                ],
                            ];
                        }
                    }
                    break;
                default :
                    break;
            }
        }
        $conditions = [];
        if(isset($searchInfo[self::PARAMETER_FILTER])){
            $conditions = array_merge($conditions,$searchInfo[self::PARAMETER_FILTER]);
        }else{
            // 默认未删除数据
            $conditions[] = ['field' => 'deleted', 'type' => '=', 'value' => 2];
        }
        if(isset($searchInfo[self::PARAMETER_RANGE])){
            $conditions = array_merge($conditions,$searchInfo[self::PARAMETER_RANGE]);
        }
        if(isset($searchInfo[self::PARAMETER_SEARCH])){
            $conditions = array_merge($conditions,$searchInfo[self::PARAMETER_SEARCH]);
        }
        if(isset($sortInfo[self::PARAMETER_SORT])){
            $sorts = $sortInfo[self::PARAMETER_SORT];
        }
        return [
            'page' => $params['page'],
            'per_page' => $params['per_page'],
            'conditions' => $conditions,
            'sort' => isset($sorts) ? $sorts:null,
        ];
    }

    // 返回 [] 里面内容
    public static function getFieldName($fieldArray,$type='')
    {
        // 默认deleted
        if($type=='==') {
            if(isset($fieldArray['deleted'])){
                if($fieldArray['deleted'] === 'false'){
                    $fieldArray['deleted'] = 2;
                }elseif ($fieldArray['deleted'] === 'true'){
                    $fieldArray['deleted'] = 1;
                }else{
                    // 暂不处理
                }
            }else{
                $fieldArray['deleted'] = 2;
            }
        }
        $info = [];
        foreach ($fieldArray as $k=>$val){
            if($k&&$val){
                if($type=='=='){
                    if($k!='external_id' && $k!='steam_id' && (strstr($k,'_id') || $k=='deleted')){
                        $searchs = explode(",", $val);
                        $ids = array_filter($searchs);
                        $searchIdsCre = [];
                        foreach ($ids as $id){
                            if(is_numeric($id)) {
                                if (preg_match("/^\d*$/", $id)) {
                                    if(strlen($id) >= 9){
                                        $id = 0;
                                    }
                                    array_push($searchIdsCre, $id);
                                }else{
                                    $id_float = floatval(sprintf("%.2f",$id));
                                    if(strlen($id_float) >= 14){
                                        $id = 0;
                                    }
                                    array_push($searchIdsCre, $id);
                                }
                            }
                        }
                        if(!empty($searchIdsCre)){
                            $info[] = ['field' => $k, 'type' => $type, 'value' => $searchIdsCre];
                        }
                    }elseif ($k=='team_snapshot_name'){
                        $searchs = explode(",", $val);
                        $chooses = array_filter($searchs);
                        $searchStrsCre = [];
                        foreach ($chooses as $str){
                            array_push($searchStrsCre,(String)$str);
                        }
                        if(!empty($searchStrsCre)){
                            $info[] = ['field' => $k, 'type' => $type, 'value' => $searchStrsCre];
                        }
                    } else {
                        $searchs = explode(",", $val);
                        $searchsCre = array_filter($searchs);
                        $info[] = ['field' => $k, 'type' => $type, 'value' => $searchsCre];
                    }
                }else{
                    $info[] = ['field' => $k, 'type' => $type, 'value' => $val];
                }
            }
        }
        return $info;
    }

    // 数组按照字段排序
    public static function sortArrayByField($data, $field, $order = SORT_ASC)
    {
        $keys = array_keys($data);
        $array_column = array_column($data, $field);
        array_multisort(
            $array_column, $order, SORT_NUMERIC, $data, $keys
        );
        $data = array_combine($keys, $data);
        $currentData = array_merge($data);
        return $currentData;
    }

    // 时间格式转换
    public static function conversionTimestamp($date)
    {
        $date = str_replace('_',':',str_replace('~',' ',$date));
        return strtotime($date);
    }
    // 按照时间排序 取出最近时间的redisKey
    public static function getDateRedisKey($redisKeyArray=[], $s, $e)
    {
        $redisKeys = $datetime = [];
        foreach ($redisKeyArray as &$val){
            $info = explode(":",$val);
            $redisKeys[] = $info;
            $datetime[]  = $info[3];
        }
        array_multisort($datetime,SORT_DESC,$redisKeys);
        $redisKey = null;
        if(!empty($redisKeys)){
            $finalRedisKeys = array_filter($redisKeys, function($v) use ($s, $e) {
                $currentTime = self::conversionTimestamp($v[3]);
                $sTime = self::conversionTimestamp($s);
                $eTime = self::conversionTimestamp($e);
                return $currentTime>=$sTime && $currentTime<=$eTime;
            });
            if(!empty($finalRedisKeys)){
                $finalRedisKeys = array_merge($finalRedisKeys);
                $redisKey = $finalRedisKeys[0];
            }
        }
        return $redisKey;
    }
    // 得到延迟后的时间
    public static function getDelayDate($matchId,$second=0,$rey='Match')
    {
        // 2020-12-02 19:12:30 - 20
        // 模拟
//        $time = strtotime('2021-01-08 16:30:30');
        $time = time();
        $nowTimeStamp   = $time - $second;
        $againTimeStamp = $time - ($second+60);
        // 返回减完秒后的分钟 和 前一分钟的数据
        $nowDate   = date('Y-m-d~H_i_s',$nowTimeStamp);
        $againDate = date('Y-m-d~H_i_s',$againTimeStamp);
        $searchRedisKeyDate = [
            0 => date('Y-m-d~H_i',$nowTimeStamp),
            1 => date('Y-m-d~H_i',$againTimeStamp),
        ];
        $currentRedisKeyArray = [];
        foreach ($searchRedisKeyDate as $date){
            $currentRedisKeys = RedisBase::searchKey([ApiConstant::PREFIX_API,$rey,$matchId,$date]);
            foreach ($currentRedisKeys as $key){
                $currentRedisKeyArray[] = $key;
            }
        }
        $redisKey = null;
        if(!empty($currentRedisKeyArray)) {
            $redisKey = self::getDateRedisKey($currentRedisKeyArray,$againDate,$nowDate);
        }
        return $redisKey;
    }

    // 获取当前用户的延迟秒数
    public static function getUserDelaySecond($currentMatchDelay,$currentDelay=0)
    {
        $userGroupInfo = \Yii::$app->params['group'] ?? [];
        $userGroupId = $userGroupInfo['group_id'] ?? null;
        switch ($userGroupId) {
            case ApiConstant::GROUP_INTERNAL_ID: //内部版
                break;
            case ApiConstant::GROUP_BASIC_ID: //基础版
                // 公网速度
                $currentDelay = $currentMatchDelay;
                break;
            case ApiConstant::GROUP_BASIC_PRO_ID: //基础版-加强
                // 公网速度
                $currentDelay = $currentMatchDelay;
                break;
            case ApiConstant::GROUP_MAJOR_ID: //专业版
                if($currentMatchDelay>10){
                    $currentDelay = 10;
                }else{
                    $currentDelay = $currentMatchDelay;
                }
                break;
            case ApiConstant::GROUP_MAJOR_PRO_ID: //专业版-加强
                if($currentMatchDelay>10){
                    $currentDelay = 10;
                }else{
                    $currentDelay = $currentMatchDelay;
                }
                break;
            case ApiConstant::GROUP_FREE_ID: //免费版
                // 公网速度
                $currentDelay = $currentMatchDelay;
                break;
            default:
                // 该用户没有权限
                throw new ProhibitException();
        }
        return $currentDelay;
    }

    /**
     * 私有赛事权限问题  （match层）
     * match_delay            -   当前比赛延迟时间
     * tournament_id          -   当前比赛-赛事id
     * is_private             -   是否私有(1-是 2-否)
     * is_private_delay       -   是否设有私有延迟 (1-是 2-否）
     * private_delay_seconds  -   私有延迟秒数
     * public_delay           -   公开延迟（1-公网 2-优势 3-无延迟 4-平台默认）
     * public_data_devel      -   公开数据级别（1-基础 2-高级 3-不提供 4-平台默认）
     */
    public static function dealMatchPrivateTournament(array $tournamentInfo = [],$currentDelay=null,$isBasic=false)
    {
        // 当前比赛延迟时间
        $currentMatchDelay = $tournamentInfo['match_delay'];
        // 赛事id
        $currentMatchTournamentId = $tournamentInfo['tournament_id'];
        // 是否私有
        $is_private = $tournamentInfo['is_private'] ?? null;
        // 是否设有私有延迟
        $is_private_delay = $tournamentInfo['is_private_delay'];
        // 设置的私有延迟数
        $private_delay_seconds = $tournamentInfo['private_delay_seconds'];
        // 公开延迟
        $public_delay = $tournamentInfo['public_delay'];
        // 公开数据级别
        $public_data_devel = $tournamentInfo['public_data_devel'];

        if ($is_private==ApiConstant::PRIVATE_TOURNAMENT_YES) {
            // 当前比赛是私有赛事

            // 判断当前用户有没有此私有赛事
            $userPrivateTournaments = self::getPrivateTournamentList();
            if (isset($userPrivateTournaments[$currentMatchTournamentId])) {

                // 私有延迟类型
                $privateDelayType = $userPrivateTournaments[$currentMatchTournamentId]['delay_type'];
                // 验证私有延迟
                switch ($privateDelayType) {
                    case ApiConstant::PRIVATE_PUBLIC_TOURNAMENT_DELAY: // 公网
                        $currentDelay = $currentMatchDelay;
                        break;
                    case ApiConstant::PRIVATE_ADVANTAGE_TOURNAMENT_DELAY: // 优势
                        if($currentMatchDelay>10){
                            $currentDelay = 10;
                        }else{
                            $currentDelay = $currentMatchDelay;
                        }
                        break;
                    case ApiConstant::PRIVATE_PRIVATE_TOURNAMENT_DELAY: // 私有
                        // 私有延迟
                        if ($is_private_delay==ApiConstant::PRIVATE_TOURNAMENT_YES) {
                            // 设置了私有延迟
                            if($currentMatchDelay>$private_delay_seconds){
                                $currentDelay = $private_delay_seconds;
                            }else{
                                if($currentMatchDelay>10){
                                    $currentDelay = 10;
                                }else{
                                    $currentDelay = $currentMatchDelay;
                                }
                            }
                        } else {
                            // 未设置私有延迟
                            if($currentMatchDelay>10){
                                $currentDelay = 10;
                            }else{
                                $currentDelay = $currentMatchDelay;
                            }
                        }
                        break;
                    case ApiConstant::PRIVATE_UNLIMITED_TOURNAMENT_DELAY: // 无延迟
                        $currentDelay = 0;
                        break;
                }

                return [
                    'is_basic' => $isBasic,
                    'match_delay' => $currentDelay,
                ];

            } else {
                // 当前用户没有此私有赛事 -- 验证公开版赛事权限  -- 暂不处理
            }

        } else {
            // 当前比赛不是私有赛事  -- 验证公开版赛事权限  -- 暂不处理
        }

        // 其他情况都 -- 验证公开版赛事权限

        // 验证公开数据级别
        switch ($public_data_devel) {
            case ApiConstant::PU_BASIC_TOURNAMENT_DATA: // 基础
            case ApiConstant::PU_ADVANCED_TOURNAMENT_DATA: // 高级
            case ApiConstant::PU_DEFAULT_TOURNAMENT_DATA: // 平台默认
                break;
            case ApiConstant::PU_UNLIMITED_TOURNAMENT_DATA: // 不提供
                // 限制match字段显示
                $isBasic = true;
                break;
            default :
                // 其他情况也走平台默认
                break;
        }

        // 验证公开延迟
        switch ($public_delay) {
            case ApiConstant::PU_PUBLIC_TOURNAMENT_DELAY: // 公网
                $currentDelay = $currentMatchDelay;
                break;
            case ApiConstant::PU_ADVANTAGE_TOURNAMENT_DELAY: // 优势
                if($currentMatchDelay>10){
                    $currentDelay = 10;
                }else{
                    $currentDelay = $currentMatchDelay;
                }
                break;
            case ApiConstant::PU_UNLIMITED_TOURNAMENT_DELAY: // 无延迟
                $currentDelay = 0;
                break;
            case ApiConstant::PU_DEFAULT_TOURNAMENT_DELAY: // 平台默认
                $currentDelay = self::getUserDelaySecond($currentMatchDelay);
                break;
        }

        return [
            'is_basic' => $isBasic,
            'match_delay' => $currentDelay,
        ];
    }

    /**
     * 私有赛事权限问题  （battle层）
     * match_delay            -   当前比赛快的时间
     * tournament_id          -   当前比赛-赛事id
     * is_private             -   是否私有(1-是 2-否)
     * is_private_delay       -   是否设有私有延迟 (1-是 2-否）
     * private_delay_seconds  -   私有延迟秒数
     * public_delay           -   公开延迟（1-公网 2-优势 3-无延迟 4-平台默认）
     * public_data_devel      -   公开数据级别（1-基础 2-高级 3-不提供 4-平台默认）
     */
    public static function dealPrivateTournament(array $tournamentInfo = [],$currentDelay=null,$isBasic=null)
    {
        // 赛事id
        $currentMatchTournamentId = $tournamentInfo['tournament_id'];
        // 是否私有
        $is_private = $tournamentInfo['is_private'] ?? null;
        // 是否设有私有延迟
        $is_private_delay = $tournamentInfo['is_private_delay'];
        // 设置的私有延迟数
        $private_delay_seconds = $tournamentInfo['private_delay_seconds'];
        // 公开延迟
        $public_delay = $tournamentInfo['public_delay'];
        // 公开数据级别
        $public_data_devel = $tournamentInfo['public_data_devel'];
        // 当前比赛延迟时间
        $currentMatchDelay = $tournamentInfo['match_delay'];

        if ($is_private==ApiConstant::PRIVATE_TOURNAMENT_YES) {
            // 当前比赛是私有赛事

            // 判断当前用户有没有此私有赛事
            $userPrivateTournaments = self::getPrivateTournamentList();
            if (isset($userPrivateTournaments[$currentMatchTournamentId])) {
                // 当前用户有此私有赛事   --  验证私有版赛事权限

                // 私有延迟类型
                $privateDelayType = $userPrivateTournaments[$currentMatchTournamentId]['delay_type'];
                // 验证私有延迟
                switch ($privateDelayType) {
                    case ApiConstant::PRIVATE_PUBLIC_TOURNAMENT_DELAY: // 公网
                        $currentDelay = $currentMatchDelay;
                        break;
                    case ApiConstant::PRIVATE_ADVANTAGE_TOURNAMENT_DELAY: // 优势
                        if($currentMatchDelay>10){
                            $currentDelay = 10;
                        }else{
                            $currentDelay = $currentMatchDelay;
                        }
                        break;
                    case ApiConstant::PRIVATE_PRIVATE_TOURNAMENT_DELAY: // 私有
                        // 私有延迟
                        if ($is_private_delay==ApiConstant::PRIVATE_TOURNAMENT_YES) {
                            // 设置了私有延迟
                            if($currentMatchDelay>$private_delay_seconds){
                                $currentDelay = $private_delay_seconds;
                            }else{
                                if($currentMatchDelay>10){
                                    $currentDelay = 10;
                                }else{
                                    $currentDelay = $currentMatchDelay;
                                }
                            }
                        } else {
                            // 未设置私有延迟
                            if($currentMatchDelay>10){
                                $currentDelay = 10;
                            }else{
                                $currentDelay = $currentMatchDelay;
                            }
                        }
                        break;
                    case ApiConstant::PRIVATE_UNLIMITED_TOURNAMENT_DELAY: // 无延迟
                        $currentDelay = 0;
                        break;
                }

                // 私有数据级别
                $privateDataLevelType = $userPrivateTournaments[$currentMatchTournamentId]['data_level'];
                // 验证私有数据级别
                $isBasic = $privateDataLevelType == ApiConstant::GAME_DATA_LEVEL_BASIC ? true:false;
                return [
                    'is_basic' => $isBasic,
                    'match_delay' => $currentDelay,
                ];
            } else {
                // 当前用户没有此私有赛事 -- 验证公开版赛事权限  -- 暂不处理
            }
        } else {
            // 当前比赛不是私有赛事  -- 验证公开版赛事权限  -- 暂不处理
        }

        // 其他情况都 -- 验证公开版赛事权限

        // 验证公开数据级别
        switch ($public_data_devel) {
            case ApiConstant::PU_BASIC_TOURNAMENT_DATA: // 基础
                $isBasic = true;
                break;
            case ApiConstant::PU_ADVANCED_TOURNAMENT_DATA: // 高级
                $isBasic = false;
                break;
            case ApiConstant::PU_UNLIMITED_TOURNAMENT_DATA: // 不提供
                // 暂时先报错没有权限 - 后面需要修改在进行修改
                // 该用户没有权限
                throw new ProhibitException();
                break;
            case ApiConstant::PU_DEFAULT_TOURNAMENT_DATA: // 平台默认
                break;
            default :
                // 其他情况也走平台默认
                break;
        }

        // 验证公开延迟
        switch ($public_delay) {
            case ApiConstant::PU_PUBLIC_TOURNAMENT_DELAY: // 公网
                $currentDelay = $currentMatchDelay;
                break;
            case ApiConstant::PU_ADVANTAGE_TOURNAMENT_DELAY: // 优势
                if($currentMatchDelay>10){
                    $currentDelay = 10;
                }else{
                    $currentDelay = $currentMatchDelay;
                }
                break;
            case ApiConstant::PU_UNLIMITED_TOURNAMENT_DELAY: // 无延迟
                $currentDelay = 0;
                break;
            case ApiConstant::PU_DEFAULT_TOURNAMENT_DELAY: // 平台默认
                $currentDelay = self::getUserDelaySecond($currentMatchDelay);
                break;
        }

        return [
            'is_basic' => $isBasic,
            'match_delay' => $currentDelay,
        ];

    }
    // 获取用户是否有私有赛事
    public static function getPrivateTournamentBool($field = 'is_private_tournament')
    {
        $isPrivateTournament = \Yii::$app->params[$field] ?? null;
        $bool = false;
        if ($isPrivateTournament) {
            $bool = true;
        }
        return $bool;
    }
    // 获取当前用户的私有赛事数据
    public static function getPrivateTournamentList($field = 'private_tournament')
    {
        $privateTournaments = [];
        if (isset(\Yii::$app->params[$field])) {
            $privateTournaments = \Yii::$app->params[$field];
        }
        return $privateTournaments;
    }

    // 是否是套餐 并且 是不是内部版
    public static function getSetMealTypeId($field = 'group')
    {
        $userGroupId = \Yii::$app->params[$field]['group_id'] ?? null;
        if ($userGroupId) {
            $isBool = true;
            if ($userGroupId == ApiConstant::GROUP_INTERNAL_ID) {
                $isBool = false;
            }
            return $isBool;
        }
        // 该用户没有权限
        throw new ProhibitException();
    }

    // 是否有游戏权限 并且 判断是否为基础数据级别
    public static function isBasicsResult($gameId=null,$afferentIsBasic=null,$field = 'advanced')
    {
        if (! self::getSetMealTypeId()) {
            return false;
        }
        if ($afferentIsBasic===true || $afferentIsBasic===false) {
            return $afferentIsBasic;
        }
        if (isset(\Yii::$app->params[$field][$gameId])) {
            if (\Yii::$app->params[$field][$gameId]) {
                $isBasic = false;
            } else {
                $isBasic = true;
            }
            return $isBasic;
        }
        // 该用户没有权限
        throw new ProhibitException();
    }


    /**
     * 验证是否有游戏权限
     */
    public static function verifyTheGame($gameId) {
        // game_id 根据用户游戏套餐进行处理
        if(!in_array($gameId,self::getAuthGameIds())){
            // 该用户没有权限，禁止操作
            throw new ProhibitException();
        }
    }
    /**
     * 根据版本组设置套餐游戏
     */
    public static function groupToSetUpGameId($filterGameIdsStr)
    {
        $filterGameIds = array_unique(explode(',', $filterGameIdsStr));
        $filterGameIdArray = array_filter($filterGameIds, function($val) {
            if (preg_match("/^\d*$/", $val)) {
                return $val;
            }
        });
        $userGameIdArray = self::getAuthGameIds();
        $currentGameIds = array_intersect($userGameIdArray,$filterGameIdArray);
        return !$currentGameIds ? implode($userGameIdArray,','):implode($currentGameIds,',');
    }
    // 游戏权限ids
    public static function getAuthGameIds($field = 'game_ids')
    {
        return \Yii::$app->params[$field];
    }
    /**
     *
     * xml/json 默认json
     * format [ xml/json ]
     *
     */
    public static function getFormatType($formatType)
    {
        switch ($formatType){
            case 'xml':
                \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
                break;
            case 'json':
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                break;
            case 'html':
                \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
                break;
            default :
                break;
        }
    }
}