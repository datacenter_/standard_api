<?php
/**
 *
 */

namespace app\modules\common\services;


class Consts
{
    // token数量最多3个，可以重置token！
    const MAXIMUM_NUMBER_TOKENS_ERROR = 'The maximum number of tokens is 3. You can reset the token!';
    // 生成token令牌失败！
    const FAILED_TO_GENERATE_TOKEN_ERROR = 'Failed to generate token!';
    // 该邮箱已存在，请更换邮箱！
    const MAILBOX_ALREADY_EXISTS_ERROR = 'Email is invalid or already taken!';
    // 该账号邮箱不存在，请更换邮箱！
    const MAILBOX_DOES_NOT_EXIST_ERROR = 'Email is invalid or already taken!';
    // 请勿重复发送验证码，十分钟后再发送！
    const VERIFICATION_CODE_REPEATEDLY_ERROR = 'Please do not send the verification code repeatedly, and send it after 10 minutes!';
    // 生成code失败！
    const CODE_GENERATION_FAILED_ERROR = 'Code generation failed!';
    // email参数错误!
    const EMAIL_PARAMETER_ERROR = 'Email parameter error!';
    // 验证码发送失败！
    const VERIFICATION_CODE_SENDING_ERROR = 'Verification code sending failed!';
    // 验证码已失效或已使用，请重新获取验证码！
    const VERIFICATION_CODE_IS_INVALID_ERROR = 'Verification code is invalid!';
    // 验证码参数错误！
    const VERIFICATION_CODE_PARAMETER_ERROR = 'Verification code parameter error!';
    // 验证码错误！
    const VERIFICATION_CODE_ERROR = 'Verification code is invalid!';

    // 您输入密码错误!
    const YOUR_PASSWORD_IS_WRONG_ERROR = 'Incorrect Password!';
    // 重置邮箱失败！
    const RESET_MAILBOX_FAILED_ERROR = 'Reset mailbox failed!';
    // 原始密码错误！
    const ORIGINAL_PASSWORD_ERROR = 'Incorrect Old Password!';
    // 完善信息失败！
    const FAILED_TO_IMPROVE_INFORMATION_ERROR = 'Failed to improve information!';

    // 企业认证失败！
    const ENTERPRISE_CERTIFICATION_FAILED_ERROR = 'Enterprise certification failed!';
    // 个人认证失败！
    const PERSONAL_AUTHENTICATION_FAILED_ERROR = 'Personal authentication failed!';

    // 重置令牌失败！
    const RESET_TOKEN_FAILED_ERROR = 'Reset token failed!';
    // 操作token令牌失败！
    const FAILED_TO_OPERATE_TOKEN_ERROR = 'Failed to operate token!';
    // 验证码验证失败！
    const VERIFICATION_CODE_VERIFICATION_FAILED_ERROR = 'Verification code verification failed!';
    // 修改密码失败！
    const PASSWORD_MODIFICATION_FAILED_ERROR = 'Password modification failed!';
}