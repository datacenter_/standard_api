<?php
/**
 *
 */

namespace app\modules\common\services;


use app\modules\common\models\HelperImageToAli;
use app\modules\file\services\UploadService;
use app\rest\exceptions\BusinessException;
use Swlib\Util\Helper;

class ImageConversionHelper
{
    const RESIZE_TYPE_EQUIVALENCY = 'equivalency';
    const RESIZE_TYPE_FIXED = 'fixed';

    // 阿里的图片可以自定义大小，这个用来加resize后缀
    public static function showSizeConversion($url, $resizeConfig, $type = "ali")
    {
        $url=self::purifyImage($url);
        // svg 格式不能转
        $urlDataInfo = (pathinfo($url));
        if(!isset($urlDataInfo['extension'])) return;
        if(!isset($resizeConfig['width'])){
            return $url.'?x-oss-process=image/resize,h_'.$resizeConfig['height'];
        }
        $ext = $urlDataInfo['extension'];
        if ($ext == 'svg') {
            return $url;
        }
        if(!strpos($url,'aliyuncs')){
            return $url;
        }
        $extStr = '';
        $resizeType = $resizeConfig['resize_type'];
        switch ($resizeType) {
            case self::RESIZE_TYPE_EQUIVALENCY:
                $maxLength = $resizeConfig['max'];
                $extStr = sprintf('resize,l_%s', $maxLength);
                break;
            case self::RESIZE_TYPE_FIXED:
                $width = $resizeConfig['width'];
                $height = $resizeConfig['height'];
                $extStr = sprintf('resize,m_fixed,h_%s,w_%s', $height, $width);
                break;
            default:
                break;
        }
        return $url . '?x-oss-process=image/' . $extStr;
    }

    public static function showFixedSizeConversion($url, $height, $width, $type = "ali")
    {
        if(isset($url)){
            $resizeConfig = [
                'resize_type' => self::RESIZE_TYPE_FIXED,
                'width' => $width,
                'height' => $height,
            ];
            return self::showSizeConversion($url, $resizeConfig, $type);
        }
    }
    public static function showHeightConversion($url, $height, $type = "ali")
    {
        if(isset($url)){
            $resizeConfig = [
                'resize_type' => self::RESIZE_TYPE_FIXED,
                'height' => $height,
            ];
            return self::showSizeConversion($url, $resizeConfig, $type);
        }
    }

    public static function purifyImage($url, $type='ali')
    {
        $urlArr=explode('?',$url);
        return $urlArr[0];
    }



    public static function mvImageToAli($url)
    {
        // 检查是否已经存过，如果没有，则上传到阿里，如果存在，就调用已经存在的url
        $info = HelperImageToAli::find()->where(['origin_url' => $url])->one();
        if ($info) {

        } else {
            // 下载url
//            $localInfo = self::getImage($url);
//            $localPath=$localInfo['save_path'];
            $localPath=self::downfile($url);
            $fileName = basename($localPath);
            $ext = substr($fileName, strrpos($fileName, '.') + 1); // 上传文件后缀
            $dst = 'transfer_station/' . md5(time()) . '/'  . md5($fileName).'.'.$ext;
//            $dst = 'transfer_station/' . md5(time()) . '/' . $fileName;
            $aliUrl = UploadService::upload($dst, $localPath);
            // 上传到ali,
            $info = new HelperImageToAli();
            $info->setAttributes([
                'origin_url' => $url,
                'ali_url' => $aliUrl,
                'identity_key' => md5($url)
            ]);
            if (!$info->save()) {
                throw new BusinessException($info->getErrors(), '转存失败');
            }
            // 存数据库
        }
        return $info['ali_url'];
    }

    //把图片下载到本地的目录，返回本地目录
    public static function imgDownLocal($img_url)
    {
        $path = '';             //默认路径
        $cacheDir = \Yii::$app->basePath . '/downloadTmp';      //图片本地目录地址
        if (!is_dir($cacheDir)) {                         //如果目录不存在
            mkdir($cacheDir, 0777, true);                 //创建目录
        }

        $cmd = "wget --no-check-certificate -P {$cacheDir} {$img_url}";
        exec($cmd, $output, $returnValue);
        print_r($output);
        if ($returnValue == 0) {
            $path = $cacheDir . '/' . basename($img_url);
        }
        return $path;
    }

    public static function getImage($url, $save_dir = '', $filename = '', $type = 1)
    {
//        if(strpos($url,'pandascore.co')){
//            $url='http://47.75.63.34/reget.php?url='.$url;
//        }
        $save_dir=\Yii::$app->basePath . '/downloadTmp/'.md5(time());
        $filename=basename($url);
        if (trim($url) == '') {
            return array('file_name' => '', 'save_path' => '', 'error' => 1);
        }
        if (trim($save_dir) == '') {
            $save_dir = './';
        }
        if (trim($filename) == '') {//保存文件名
            $ext = strrchr($url, '.');
            if ($ext != '.gif' && $ext != '.jpg') {
                return array('file_name' => '', 'save_path' => '', 'error' => 3);
            }
            $filename = time() . $ext;
        }
        if (0 !== strrpos($save_dir, '/')) {
            $save_dir .= '/';
        }
//创建保存目录
        if (!file_exists($save_dir) && !mkdir($save_dir, 0777, true)) {
            return array('file_name' => '', 'save_path' => '', 'error' => 5);
        }
//获取远程文件所采用的方法
        if ($type) {
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $img = curl_exec($ch);
            curl_close($ch);
        } else {
            ob_start();
            readfile($url);
            $img = ob_get_contents();
            ob_end_clean();
        }
//$size=strlen($img);
//文件大小
        print_r($save_dir . $filename);
        $fp2 = @fopen($save_dir . $filename, 'a');
        fwrite($fp2, $img);
        fclose($fp2);
        unset($img, $url);
        return array('file_name' => $filename, 'save_path' => $save_dir . $filename, 'error' => 0);
    }

    public static function downfile($httpUrl)
    {
        $url=$httpUrl;
//        $url=implode("/",$urlEncodeArray);
        $file = file_get_contents($url);
//        $fileArr=explode('.',$httpUrl);
//        $fileExt=end($fileArr);
        $filename=basename($url);
        $pic_local_path = \Yii::$app->getBasePath() . '/public/assets/cache/'.md5(time());
        $pic_local = $pic_local_path . '/' . $filename;

        if (!file_exists($pic_local_path)) {
//            print_r($pic_local_path);
//            exit;
            $info=mkdir($pic_local_path, 0777,true);
            @chmod($pic_local_path, 0777);
        }

        file_put_contents($pic_local, $file);
        return $pic_local;
    }
}