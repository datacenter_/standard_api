<?php

namespace app\modules\common\services;

use Yii;

class RedisBase
{
    // 有效期（秒为单位）
    const EXPIRES_AT = 8000;
    /**
     * Redis
     */
    // key-value 取值
    public static function valueGet($key)
    {
        $keyName = self::handleKey($key);
        return Yii::$app->redis->get($keyName);
    }
    // key-value 存值
    public static function valueSet($key,$value,$isExpire=false,$expires_at=false)
    {
        $keyName = self::handleKey($key);
        Yii::$app->redis->set($keyName,$value);
        if($isExpire){
            Yii::$app->redis->expire($keyName, self::EXPIRES_AT);
        }
        if($expires_at){
            $expireTime = mktime(23, 59, 59, date("m"), date("d"), date("Y"));
            $currentExpireTime = $expireTime - time();
            Yii::$app->redis->expire($keyName, $currentExpireTime);
        }
        return;
    }
    // hash 获取表中指定字段的值。若表不存在则返回false。
    public static function hashGet($key,$field)
    {
        $keyName = self::handleKey($key);
        return Yii::$app->redis->hget($keyName,$field);
    }
    // hash 获取某个表中所有的字段和值。
    public static function hashGetAll($key)
    {
        $keyName = self::handleKey($key);
        return Yii::$app->redis->hgetall($keyName);
    }
    // redis-key 模糊匹配(battle/match)
    public static function searchKey($key){
        $keyName = self::handleKey($key);
        return Yii::$app->redis->keys("{$keyName}*");
    }
    // redis -删除- key
    public static function keyDel($key){
        $keyName = self::handleKey($key);
        return Yii::$app->redis->del($keyName);
    }
    // redis-key 是否存在
    public static function issetKey($key){
        $keyName = self::handleKey($key);
        return Yii::$app->redis->exists($keyName);
    }
    // redis - Key-Name
    public static function handleKey($keyArray)
    {
        Yii::$app->redis->select(env('ES_REDIS_DATABASE'));
        $keyString = implode(":", $keyArray);
        return sprintf("%s", $keyString);
    }

}
