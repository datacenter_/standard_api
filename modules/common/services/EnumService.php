<?php
/**
 *
 */

namespace app\modules\common\services;


use app\modules\common\models\EnumCountry;
use app\modules\common\models\EnumGame;
use app\modules\common\models\EnumGameMatchType;
use app\modules\common\models\EnumGameRules;
use app\modules\common\models\EnumMatchRule;
use app\modules\common\models\EnumMatchStageType;
use app\modules\common\models\EnumMatchState;
use app\modules\common\models\EnumTournamentState;
use yii\db\ActiveRecord;
use yii\db\Exception;

class EnumService
{
    const ENUM_PER = "gcs:enum:";
    const ENUM_MATCH_CONFIG = [
        'games' => EnumGame::class,
        'country' => EnumCountry::class,
        'country_key_pandascore' => EnumCountry::class,
        'country_key_two' => EnumCountry::class,
        'game_rules' =>EnumGameRules::class,
        'game_match_type'=>EnumGameMatchType::class,
        'match_state' =>EnumMatchState::class,
    ];

    public static function getGameLogo()
    {
        return EnumGame::find()->select('logo,id')->asArray()->all();
    }

    public static function getTournamentState()
    {
        return EnumTournamentState::find()->all();
    }

    public static function getStageType()
    {
        return EnumMatchStageType::find()->all();
    }

    public static function getMatchRule($type)
    {
        if ($type == 1) {
            return EnumMatchRule::find()->select('id,name,name_cn')->where(['group_match' => 1])->all();
        }
        if ($type == 2) {
            return EnumMatchRule::find()->select('id,name,name_cn')->where(['out_match' => 1])->all();
        }
        if ($type == 3) {
            return EnumMatchRule::find()->select('id,name,name_cn')->where(['union_match' => 1])->all();
        }
    }

    public static function getEnum($key)
    {
        $val = self::cacheGet($key);
        if ($val && 0) {
            return $val;
        } else {
            if (isset(self::ENUM_MATCH_CONFIG[$key])) {
                $class = self::ENUM_MATCH_CONFIG[$key];
                if (new $class instanceof ActiveRecord) {
                    //
                    $keyName = 'id';
                    $keyInfo = explode("_key_", $key);
                    if (count($keyInfo) > 1) {
                        $keyName = $keyInfo[1];
                    }
                    if ($key == 'country_key_two') {
                        $where = ['type' => 0];
                    }
                    if ($key == 'origin') {
                        $where = ['flag' => 1];
                    }
                    if (isset($where) && $where) {
                        $list = $class::find()->where($where)->orderBy('id asc')->asArray()->all();
                    } else {
                        $list = $class::find()->orderBy('id asc')->asArray()->all();
                    }

                    $list = array_column($list, null, $keyName);
                    self::cacheSet($key, $list);
                    return $list;
                } elseif (is_callable($class)) {
                    $list = $class();
                    self::cacheSet($key, $list);
                    return $list;
                }
            }
        }
    }

//    public static function

    public static function getGameInfoByEShort($shortName)
    {
        return EnumGame::find()->where(['e_short' => $shortName])->asArray()->one();
    }
    public static function getCountryInfoByEName($EName)
    {
        return EnumCountry::find()->where(['e_name' => $EName])->asArray()->one();
    }

    public static function getCountryInfoByHltvEName($EName)
    {
        return EnumCountry::find()->where(['hltv' => $EName])->asArray()->one();
    }

    private static function cacheGet($key)
    {
        $key = self::ENUM_PER . $key;
        $cache = \Yii::$app->cache;
        return $cache->get($key);
    }

    private static function cacheSet($key, $val, $life = 86400)
    {
        $key = self::ENUM_PER . $key;
        $cache = \Yii::$app->cache;
        return $cache->set($key, $val, $life);
    }

    public static function getCountryByTwo($twoKey)
    {
        if($twoKey){
            $list = self::getEnum('country_key_two');
            if (isset($list[$twoKey])) {
                return $list[$twoKey];
            }
        }
    }

    public static function getCountryByPandascore($key)
    {
        if($key){
            $list = self::getEnum('country_key_pandascore');
            if (isset($list[$key])) {
                return $list[$key];
            }
        }
    }
}