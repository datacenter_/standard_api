<?php
/**
 *
 */

namespace app\modules\common\services;


class DbHelper
{
    public static function getWhere($config,$params)
    {
        $where=[];
        if(!$params ||count($params)==0){
            return $where;
        }
        foreach($config as $key=>$val){
            //
            // 如果值是字符串
            if((!is_callable($val) && !is_array($val)) && isset($params[$val]) && $params[$val]){
                $where[]=['=',$val,$params[$val]];
            }elseif(is_array($val) && isset($params[$key]) && $params[$key]){
                $where[]=[$val["type"],$val["key"],$params[$key]];
            }elseif(is_callable($val)){
                $tmpWhere=$val($params);
                if($tmpWhere){
                    $where[]=$tmpWhere;
                }
            }
        }
        return $where;
    }
}