<?php
namespace app\modules\common\controllers;
use app\controllers\GuestController;
use app\modules\common\models\EnumGame;
use app\modules\common\models\EnumPosition;
use app\modules\common\models\EnumState;
use app\modules\common\models\EnumTournamentState;
use app\modules\common\services\EnumService;

/**
 * Class TaskController
 * @package app\modules\common\controllers
 */
class EnumController extends GuestController
{
    /**
     * 推送排期
     */
    public function actionGetGames()
    {
        return EnumGame::find()->all();
    }

    public function actionGetPosition()
    {
        $position = EnumPosition::find()->all();

        $info = [];
        foreach ($position as $key => $value) {
            $info[$value['game_id']][] = $value;
        }
        return $info;
    }

    public function actionGetState()
    {
        $state = EnumState::find()->all();

        return $state;
    }

    public function actionGetTournamentState()
    {
        return EnumService::getTournamentState();
    }

    public function actionGetMatchStageType()
    {
        return EnumService::getStageType();
    }

    public function actionGetMatchRule()
    {
        $type = \Yii::$app->getRequest()->get('stage_type');
        return EnumService::getMatchRule($type);
    }

    public function actionOrigin()
    {
        return array_values(EnumService::getEnum('origin'));
    }

    public function actionEnums()
    {
        $type=$this->pGet('type');
        return EnumService::getEnum($type);
    }
}