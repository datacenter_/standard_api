<?php
namespace app\modules\common\controllers;

use app\controllers\GuestController;
use app\modules\common\services\TaskService;

/**
 * Class TaskController
 * @package app\modules\common\controllers
 */
class TaskController extends GuestController
{
    /**
     * 推送排期
     */
    public function actionSendMessage()
    {
        return TaskService::handleSchedule();
    }
}