<?php
namespace app\modules\common\controllers;

use app\controllers\GuestController;
use app\modules\common\models\CommonOperation;
use app\modules\common\Module;
use app\constants\Platform;
use app\modules\common\services\EsService;
use Elasticsearch\ClientBuilder;

/**
 * Class LoginController
 * @package app\modules\admin\controllers
 * @property \app\modules\admin\Module $module
 */
class CommonController
{
    public static function initEs(){
        $client = EsService::initEs();
        return $client;
    }
}