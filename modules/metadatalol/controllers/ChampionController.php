<?php

namespace app\modules\metadatalol\controllers;

use app\controllers\RestController;
use app\modules\metadatalol\services\MetadataLolService;
use app\modules\common\controllers\CommonController;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use Elasticsearch\ClientBuilder;

class ChampionController extends RestController
{

    public function actionGet(){
        $params = $this->pGet();
        $info = MetadataLolService::getChampionList($params);
        return $info;
    }

    public function actionDetail(){
        $champion_id = $this->pGet('champion_id');
        $info = MetadataLolService::getChampionDetail($champion_id);
        return $info;
    }
}
