<?php

namespace app\modules\metadatalol\controllers;

use app\controllers\RestController;
use app\modules\common\controllers\CommonController;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use app\modules\metadatalol\services\MetadataLolService;
use Elasticsearch\ClientBuilder;

class SummonerspellController extends RestController
{
    public function actionGet(){
        $params = $this->pGet();
        $info = MetadataLolService::getSummonerspellList($params);
        return $info;
    }

    public function actionDetail(){
        $summoner_spell_id = $this->pGet('summoner_spell_id');
        $info = MetadataLolService::getSummonerspellDetail($summoner_spell_id);
        return $info;
    }
}
