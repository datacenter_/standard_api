<?php

namespace app\modules\metadatalol\controllers;

use app\controllers\RestController;
use app\modules\common\controllers\CommonController;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use app\modules\metadatalol\services\MetadataLolService;
use Elasticsearch\ClientBuilder;

class RuneController extends RestController
{
    public function actionGet(){
        $params = $this->pGet();
        $info = MetadataLolService::getRuneList($params);
        return $info;
    }

    public function actionDetail(){
        $rune_id = $this->pGet('rune_id');
        $info = MetadataLolService::getRuneDetail($rune_id);
        return $info;
    }
}
