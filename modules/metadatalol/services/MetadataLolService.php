<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/31
 * Time: 22:00
 */
namespace app\modules\metadatalol\services;

use app\modules\common\services\ApiBase;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\RedisBase;

class MetadataLolService
{
    public static function getChampionList($params)
    {
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_champion_list';
        $conditions = [];
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'champion_id', 'sort' => 'asc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Lol','champion',$data['champion_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getChampionDetail($champion_id)
    {
        if(!$champion_id){
            return [];
        }
        // redis取值
        $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Lol','champion',$champion_id]);
        $info = [];
        if($json){
            $info = json_decode($json,true);
        }
        return $info;
    }

    public static function getItemList($params)
    {
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_item_list';
        $conditions = [];
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'item_id', 'sort' => 'asc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Lol','item',$data['item_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getItemDetail($item_id)
    {
        if(!$item_id){
            return [];
        }
        // redis取值
        $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Lol','item',$item_id]);
        $info = [];
        if($json){
            $info = json_decode($json,true);
        }
        return $info;
    }

    public static function getRuneList($params)
    {
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_rune_list';
        $conditions = [];
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'rune_id', 'sort' => 'asc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Lol','rune',$data['rune_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getRuneDetail($rune_id)
    {
        if(!$rune_id){
            return [];
        }
        // redis取值
        $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Lol','rune',$rune_id]);
        $info = [];
        if($json){
            $info = json_decode($json,true);
        }
        return $info;
    }

    public static function getSummonerspellList($params)
    {
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_summonerspell_list';
        $conditions = [];
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'summoner_spell_id', 'sort' => 'asc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Lol','summonerSpell',$data['summoner_spell_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getSummonerspellDetail($summoner_spell_id)
    {
        if(!$summoner_spell_id){
            return [];
        }
        // redis取值
        $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Lol','summonerSpell',$summoner_spell_id]);
        $info = [];
        if($json){
            $info = json_decode($json,true);
        }
        return $info;
    }

}