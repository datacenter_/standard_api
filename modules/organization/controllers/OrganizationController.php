<?php

namespace app\modules\organization\controllers;

use app\controllers\RestController;
use app\modules\common\controllers\CommonController;
use app\modules\organization\services\OrganizationService;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use Elasticsearch\ClientBuilder;

class OrganizationController extends RestController
{
    public function actionGet(){
        $params = $this->pGet();
        $info = OrganizationService::getOrganizationList($params);
        return $info;
    }

    public function actionDetail(){
        $organizationId = $this->pGet('organization_id');
        $info = OrganizationService::getOrganizationDetail($organizationId);
        return $info;
    }
}
