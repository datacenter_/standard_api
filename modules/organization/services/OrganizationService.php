<?php

namespace app\modules\organization\services;

use app\modules\common\services\ApiBase;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\RedisBase;

class OrganizationService
{

    public static function getOrganizationList($params) {
        $index_info = 'resource_organization_list';
        $params = ApiBase::issetParameterFilter($params);
        $conditions = [];
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'organization_id', 'sort' => 'asc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Organizations',$data['organization_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getOrganizationDetail($organizationId){
        if(!$organizationId){
            return [];
        }
        // redis取值
        $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Organizations',$organizationId]);
        $info = [];
        if($json){
            $info = json_decode($json,true);
        }
        return $info;
    }
}