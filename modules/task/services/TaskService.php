<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/31
 * Time: 22:00
 */

namespace app\modules\task\services;


use app\modules\common\services\EsService;

class TaskService
{
    // 删除所有索引
    public static function indexDeleteAll($indexName)
    {
        if($indexName){
            $indexArray = [$indexName];
        }else {
            $indexArray = [
                // lol
                'resource_champion_list',
                'resource_item_list',
                'resource_rune_list',
                'resource_summonerspell_list',
                // dota
                'resource_hero_list',
                'resource_dotaitem_list',
                // csgo
                'resource_csgomap_list',
                'resource_weapon_list',
                // game
                'resource_game_list',
                // tournament
                'resource_tournament_list',
                // organization
                'resource_organization_list',
                // team
                'resource_team_list',
                // player
                'resource_player_list',
                // match
                'resource_matches_list',
                // battle
                'resource_battle_list',
                // csgo_battle
//                'resource_battle_5e_list',
            ];
        }
        $client = EsService::initConnect();
        foreach ($indexArray as $key=>$index){
            $client->deleteIndex($index);
        }
        return true;
    }

}