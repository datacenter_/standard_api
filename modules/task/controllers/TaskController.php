<?php

namespace app\modules\task\controllers;

//use app\controllers\RestController;
use app\modules\task\services\TaskService;
use yii\rest\Controller;

class TaskController extends Controller
{
    // 删除索引
    public function actionIndexDel(){
        $index_name = \Yii::$app->request->get('index_name');
        $info = TaskService::indexDeleteAll($index_name);
        return $info;
    }
}
