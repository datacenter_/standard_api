<?php

namespace app\modules\statslol\controllers;

use app\controllers\RestController;
use app\modules\statslol\services\StatslolService;

class StatslolController extends RestController
{

    //Get stats for team（Lol战队统计数据详情）
    public function actionTeamLolDetail(){
        $params = $this->pGet();
        $info = StatslolService::getTeamLolDetail($params);
        return $info;
    }

    public function actionTeamMostBannedChampions(){
        $params = $this->pGet();
        $info = StatslolService::getTeamMostBannedChampions($params);
        return $info;
    }

    public function actionTeamMostPickedChampions(){
        $params = $this->pGet();
        $info = StatslolService::getTeamMostPickedChampions($params);
        return $info;
    }

    public function actionPlayerDetail(){
        //20091
        $params = $this->pGet();
        $info = StatslolService::getPlayerDetail($params);
        return $info;
    }

    public function actionMostPickedChampions(){
        $params = $this->pGet();
        $info = StatslolService::getPlayerMostPickedChampions($params);
        return $info;
    }
}
