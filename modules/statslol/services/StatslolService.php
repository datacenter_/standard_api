<?php

namespace app\modules\statslol\services;
use app\modules\common\services\EsService;
class StatslolService
{

    public static function getTeamLolDetail($params)
    {
        if(!isset($params['number_of_matches']) && !$params['number_of_matches']){
            return [];
        }
        $page = empty($params['page']) ? 1 : $params['page'];
        $per_page = empty($params['per_page']) ? 50 : $params['per_page'];
        $index_info = "stats-v2-lol-team-{$params['number_of_matches']}";
        $index = self::issetIndexKey($index_info);
        if(!$index){
            return [
                'list' => [],
                'count' => 0
            ];
        }
        $conditions = [];
        $offset  = ($page > 0) ? ($page - 1) * $per_page : 0;
        $columns = [];//['match_id', 'description'];
        // 筛选
        if (isset($params['team_id']) && $params['team_id']){
            $conditions[] = ['field' => 'content.team.team_id', 'type' => '===', 'value' => $params['team_id']];
        }
        if (isset($params['faction']) && $params['faction']){
            $conditions[] = ['field' => 'content.faction', 'type' => '===', 'value' => $params['faction']];
        }
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $per_page, $columns);
        $resData = [
            'list' => [],
            'count' => $res['count']
        ];
        if(!empty($res['list'])){
            foreach ($res['list'] as $key=>$team){
                $team = $team['content'];
                $info = Structure::getStatsTeamInfo($team);
                $resData['list'][] = $info;
            }
        }
        return $resData;
    }

    public static function getTeamMostBannedChampions($params)
    {
        if(!isset($params['number_of_battles']) && !$params['number_of_battles']){
            return [];
        }
        $page = empty($params['page']) ? 1 : $params['page'];
        $per_page = empty($params['per_page']) ? 50 : $params['per_page'];
        $index_info = "stats-v2-lol-team-ban-{$params['number_of_battles']}";
        $index = self::issetIndexKey($index_info);
        if(!$index){
            return [
                'list' => [],
                'count' => 0
            ];
        }
        $conditions = [];
        $offset  = ($page > 0) ? ($page - 1) * $per_page : 0;
        $columns = [];//['match_id', 'description'];
        // 筛选
        if (isset($params['team_id']) && $params['team_id']){
            $conditions[] = ['field' => 'content.team.team_id', 'type' => '=', 'value' => $params['team_id']];
        }
        if (isset($params['faction']) && $params['faction']){
            $conditions[] = ['field' => 'content.faction', 'type' => '===', 'value' => $params['faction']];
        }
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $per_page, $columns);
        $resData = [
            'list' => [],
            'count' => $res['count']
        ];
        if(!empty($res['list'])){
            foreach ($res['list'] as $key=>$team){
                $team = $team['content'];
                $info = Structure::getTeamBannedChampionInfo($team);
                $resData['list'][] = $info;
            }
        }
        return $resData;
    }

    public static function getTeamMostPickedChampions($params)
    {
        if(!isset($params['number_of_battles']) && !$params['number_of_battles']){
            return [];
        }
        $page = empty($params['page']) ? 1 : $params['page'];
        $per_page = empty($params['per_page']) ? 50 : $params['per_page'];
        $index_info = "stats-v2-lol-team-pick-{$params['number_of_battles']}";
        $index = self::issetIndexKey($index_info);
        if(!$index){
            return [
                'list' => [],
                'count' => 0
            ];
        }
        $conditions = [];
        $offset  = ($page > 0) ? ($page - 1) * $per_page : 0;
        $columns = [];//['match_id', 'description'];
        // 筛选
        if (isset($params['team_id']) && $params['team_id']){
            $conditions[] = ['field' => 'content.team.team_id', 'type' => '=', 'value' => $params['team_id']];
        }
        if (isset($params['faction']) && $params['faction']){
            $conditions[] = ['field' => 'content.faction', 'type' => '===', 'value' => $params['faction']];
        }
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $per_page, $columns);
        $resData = [
            'list' => [],
            'count' => $res['count']
        ];
        if(!empty($res['list'])){
            foreach ($res['list'] as $key=>$team){
                $team = $team['content'];
                $info = Structure::getTeamPickedChampionInfo($team);
                $resData['list'][] = $info;
            }
        }
        return $resData;
    }

    public static function getPlayerMostPickedChampions($params)
    {
        if(!isset($params['number_of_battles']) && !$params['number_of_battles']){
            return [];
        }
        $page = empty($params['page']) ? 1 : $params['page'];
        $per_page = empty($params['per_page']) ? 50 : $params['per_page'];
        $index_info = "stats-v2-lol-player-pick-{$params['number_of_battles']}";
        $index = self::issetIndexKey($index_info);
        if(!$index){
            return [
                'list' => [],
                'count' => 0
            ];
        }
        $conditions = [];
        $offset  = ($page > 0) ? ($page - 1) * $per_page : 0;
        $columns = [];//['match_id', 'description'];
        // 筛选
        if (isset($params['player_id']) && $params['player_id']){
            $conditions[] = ['field' => 'content.player.player_id', 'type' => '=', 'value' => $params['player_id']];
        }
        if (isset($params['faction']) && $params['faction']){
            $conditions[] = ['field' => 'content.faction', 'type' => '===', 'value' => $params['faction']];
        }
//        print_r($conditions);die;
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $per_page, $columns);
//        $res     = $EsService->searchAll($index_info,$conditions, $columns);
//        $res     = $EsService->esGet($index_info);
        $resData = [
            'list' => [],
            'count' => $res['count']
        ];
        if(!empty($res['list'])){
            foreach ($res['list'] as $key=>$player){
                $player = $player['content'];
                $info = Structure::getPlayerMostPickedInfo($player);
                $resData['list'][] = $info;
            }
        }
        return $resData;
    }

    public static function getPlayerDetail($params)
    {
        if(!isset($params['number_of_battles']) && !$params['number_of_battles']){
            return [];
        }
        $page = empty($params['page']) ? 1 : $params['page'];
        $per_page = empty($params['per_page']) ? 50 : $params['per_page'];
        $index_info = "stats-v2-lol-player-{$params['number_of_battles']}";
        $index = self::issetIndexKey($index_info);
        if(!$index){
            return [
                'list' => [],
                'count' => 0
            ];
        }
        $conditions = [];
        $offset  = ($page > 0) ? ($page - 1) * $per_page : 0;
        $columns = [];//['match_id', 'description'];
        // 筛选
        if (isset($params['player_id']) && $params['player_id']){
            $conditions[] = ['field' => 'content.player.player_id', 'type' => '=', 'value' => $params['player_id']];
        }
        if (isset($params['faction']) && $params['faction']){
            $conditions[] = ['field' => 'content.faction', 'type' => '===', 'value' => $params['faction']];
        }
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $per_page, $columns);
        $resData = [
            'list' => [],
            'count' => $res['count']
        ];
        if(!empty($res['list'])){
            foreach ($res['list'] as $key=>$player){
                $player = $player['content'];
                $info = Structure::getPlayerDetailInfo($player);
                $resData['list'][] = $info;
            }
        }
        return $resData;
    }

    public static function issetIndexKey($index_info)
    {
        $client = EsService::initConnect();
        return $client->searchIndex($index_info);
    }
}
