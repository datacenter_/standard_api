<?php
namespace app\modules\statscsgo\services;

use app\modules\common\services\EsService;

class StatsCsgoService
{
    public static function getPlayerDetail($params)
    {
        if(!isset($params['number_of_battles']) && !$params['number_of_battles']){
            return [];
        }
        $page = empty($params['page']) ? 1 : $params['page'];
        $per_page = empty($params['per_page']) ? 50 : $params['per_page'];
        $index_info = "stats-v2-csgo-player-{$params['number_of_battles']}";
        $index = self::issetIndexKey($index_info);
        if(!$index){
            return [
                'list' => [],
                'count' => 0
            ];
        }
        $conditions = [];
        $offset  = ($page > 0) ? ($page - 1) * $per_page : 0;
        $columns = [];//['match_id', 'description'];
        // 筛选
        if (isset($params['player_id']) && $params['player_id']){
            $conditions[] = ['field' => 'content.player.player_id', 'type' => '=', 'value' => $params['player_id']];
        }
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $per_page, $columns);
        $resData = [
            'list' => [],
            'count' => $res['count']
        ];

        if(!empty($res['list'])){
            foreach ($res['list'] as $key=>$player){
                $player = $player['content'];
                $info = Structure::getPlayerDetailInfo($player);
                $resData['list'][] = $info;
            }
        }
        return $resData;



    }

    public static function issetIndexKey($index_info)
    {
        $client = EsService::initConnect();
        return $client->searchIndex($index_info);
    }

}