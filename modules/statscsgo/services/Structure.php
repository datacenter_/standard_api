<?php


namespace app\modules\statscsgo\services;


class Structure
{
    public static function getPlayerDetailInfo($player){
        $info['player']['player_id'] = $player['player']['player_id'];
        $info['player']['nick_name'] = $player['player']['nick_name'];
        $info['player']['game']['game_id'] = $player['player']['game']['game_id'];
        $info['player']['game']['name'] = $player['player']['game']['name'];
        $info['player']['game']['name_cn'] = $player['player']['game']['name_cn'];
        $info['player']['game']['default_game_rules'] = $player['player']['game']['default_game_rules'];
        $info['player']['game']['default_match_type'] = $player['player']['game']['default_match_type'];
        $info['player']['game']['full_name'] = $player['player']['game']['full_name'];
        $info['player']['game']['short_name'] = $player['player']['game']['short_name'];
        $info['player']['game']['slug'] = $player['player']['game']['slug'];
        $info['player']['game']['image'] = $player['player']['game']['image'];
        $info['player']['game']['modified_at'] = $player['player']['game']['modified_at'];
        $info['player']['game']['created_at'] = $player['player']['game']['created_at'];
        $info['player']['game']['deleted_at'] = $player['player']['game']['deleted_at'];
        $info['player']['nationality']['id'] = $player['player']['nationality']['id'];
        $info['player']['nationality']['name'] = $player['player']['nationality']['name'];
        $info['player']['nationality']['name_cn'] = $player['player']['nationality']['name_cn'];
        $info['player']['nationality']['short_name'] = $player['player']['nationality']['short_name'];
        $info['player']['nationality']['image'] = $player['player']['nationality']['image'];
        $info['player']['name'] =  $player['player']['name'];
        $info['player']['name_cn'] = $player['player']['name_cn'];
        $info['player']['role'] = $player['player']['role'];
        $info['player']['slug'] = $player['player']['slug'];
        $info['player']['image'] = $player['player']['image'];
        $info['player']['teams']['team_id'] =  $player['player']['teams']['team_id'];
        $info['player']['teams']['name'] = $player['player']['teams']['name'];
        $info['player']['teams']['game']['game_id'] = $player['player']['game']['game_id'];
        $info['player']['teams']['game']['name'] = $player['player']['game']['name'];
        $info['player']['teams']['game']['name_cn'] = $player['player']['game']['name_cn'];
        $info['player']['teams']['game']['default_game_rules'] = $player['player']['game']['default_game_rules'];
        $info['player']['teams']['game']['default_match_type'] = $player['player']['game']['default_match_type'];
        $info['player']['teams']['game']['full_name'] = $player['player']['game']['full_name'];
        $info['player']['teams']['game']['short_name'] = $player['player']['game']['short_name'];
        $info['player']['teams']['game']['slug'] = $player['player']['game']['slug'];
        $info['player']['teams']['game']['image'] = $player['player']['game']['image'];
        $info['player']['teams']['game']['modified_at'] = $player['player']['game']['modified_at'];
        $info['player']['teams']['game']['created_at'] = $player['player']['game']['created_at'];
        $info['player']['teams']['game']['deleted_at'] = $player['player']['game']['deleted_at'];
        $info['player']['teams']['country']['id'] = $player['player']['teams']['country']['id'];
        $info['player']['teams']['country']['name'] = $player['player']['teams']['country']['name'];
        $info['player']['teams']['country']['name_cn'] = $player['player']['teams']['country']['name_cn'];
        $info['player']['teams']['country']['short_name'] = $player['player']['teams']['country']['short_name'];
        $info['player']['teams']['country']['image'] = $player['player']['teams']['country']['image'];
        $info['player']['teams']['full_name'] = $player['player']['teams']['full_name'];
        $info['player']['teams']['short_name'] = $player['player']['teams']['short_name'];
        $info['player']['teams']['alias'] = $player['player']['teams']['alias'];
        $info['player']['teams']['slug'] = $player['player']['teams']['slug'];
        $info['player']['teams']['image'] = $player['player']['teams']['image'];
        $info['player']['teams']['status'] = $player['player']['teams']['status'];
        $info['player']['teams']['modified_at'] = $player['player']['teams']['modified_at'];
        $info['player']['teams']['created_at'] = $player['player']['teams']['created_at'];
        $info['player']['modified_at'] = $player['player']['modified_at'];
        $info['player']['created_at'] = $player['player']['modified_at'];
        $info['player']['deleted_at'] = $player['player']['modified_at'];
        $info['total_number_of_battles'] = $player['total_number_of_battles'];
        $info['total_number_of_rounds'] = $player['total_number_of_rounds'];
        $info['number_of_battles'] = $player['number_of_battles'];
        $info['battles_count'] = $player['battles_count'];
        $info['rounds_count'] = $player['rounds_count'];
        $info['kill'] = $player['kill'];
        $info['headshot_kills'] =  $player['headshot_kills'];
        $info['kills_per_round'] = $player['kills_per_round'];
        $info['headshot_rate'] = $player['headshot_rate'];
        $info['deaths'] = $player['deaths'];
        $info['death_per_round'] = $player['death_per_round'];
        $info['k_d_ratio'] = $player['k_d_ratio'];
        $info['assists'] = $player['assists'];
        $info['assists_per_round'] = $player['assists_per_round'];
        $info['flash_assists'] = $player['flash_assists'];
        $info['flash_assists_per_round'] = $player['flash_assists_per_round'];
        $info['average_damage_per_round'] = $player['average_damage_per_round'];
        $info['first_kills'] = $player['first_kills'];
        $info['first_kills_rate'] = $player['first_kills_rate'];
        $info['first_deaths'] = $player['first_deaths'];
        $info['first_deaths_rate'] = $player['first_deaths_rate'];
        $info['multi_kills'] = $player['multi_kills'];
        $info['multi_kills_rate'] = $player['multi_kills_rate'];
        $info['kast'] = $player['kast'];
        $info['rating'] = $player['rating'];

        return $info;

    }

}