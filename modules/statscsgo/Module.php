<?php

namespace app\modules\statscsgo;

/**
 * admin module definition class
 * @property \yii\web\user user
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\statscsgo\controllers';
}
