<?php
namespace app\modules\statscsgo\controllers;

use app\controllers\RestController;
use app\modules\statscsgo\services\StatsCsgoService;

class StatsCsgoController extends RestController
{
    public function actionPlayerDetail(){
        $params = $this->pGet();
        $info = StatsCsgoService::getPlayerDetail($params);
        return $info;
    }

}
