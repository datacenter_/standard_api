<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/31
 * Time: 22:00
 */
namespace app\modules\metadatacsgo\services;

use app\modules\common\services\ApiBase;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\RedisBase;

class MetadatacsgoService
{

    public static function getMapList($params)
    {
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_csgomap_list';
        $conditions = [];
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'map_id', 'sort' => 'asc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Csgo','map',$data['map_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getMapDetail($map_id)
    {
        if(!$map_id){
            return [];
        }
        // redis取值
        $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Csgo','map',$map_id]);
        $info = [];
        if($json){
            $info = json_decode($json,true);
        }
        return $info;
    }

    public static function getWeaponList($params)
    {
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_weapon_list';
        $conditions = [];
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'weapon_id', 'sort' => 'asc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Csgo','weapon',$data['weapon_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getWeaponDetail($weapon_id)
    {
        if(!$weapon_id){
            return [];
        }
        // redis取值
        $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Csgo','weapon',$weapon_id]);
        $info = [];
        if($json){
            $info = json_decode($json,true);
        }
        return $info;
    }
}