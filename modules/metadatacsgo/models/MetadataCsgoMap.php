<?php

namespace app\modules\metadatacsgo\models;

use Yii;

/**
 * This is the model class for table "metadata_csgo_map".
 *
 * @property int $id
 * @property string|null $name 名称
 * @property string|null $name_cn 名称（中文）
 * @property int|null $state 状态
 * @property string|null $external_name 官方名称
 * @property string|null $short_name 简称
 * @property string|null $map_type 地图类型
 * @property string|null $map_type_cn 地图类型（中文）
 * @property int|null $is_default 是否为默认地图
 * @property string|null $slug Slug
 * @property string|null $square_image 方形图标
 * @property string|null $rectangle_image 长方形图标
 * @property string|null $thumbnail 缩略图
 * @property string|null $modified_at 变更日期
 * @property string|null $created_at 创建日期
 * @property string|null $deleted_at 删除日期
 * @property int|null $deleted 1已删除，2正常
 * @property int|null $flag
 * @property string|null $external_id
 */
class MetadataCsgoMap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'metadata_csgo_map';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state', 'is_default', 'deleted', 'flag'], 'integer'],
            [['modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['name', 'name_cn', 'external_name', 'short_name', 'map_type', 'map_type_cn', 'slug', 'square_image', 'rectangle_image', 'thumbnail', 'external_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'state' => 'State',
            'external_name' => 'External Name',
            'short_name' => 'Short Name',
            'map_type' => 'Map Type',
            'map_type_cn' => 'Map Type Cn',
            'is_default' => 'Is Default',
            'slug' => 'Slug',
            'square_image' => 'Square Image',
            'rectangle_image' => 'Rectangle Image',
            'thumbnail' => 'Thumbnail',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'deleted' => 'Deleted',
            'flag' => 'Flag',
            'external_id' => 'External ID',
        ];
    }
}
