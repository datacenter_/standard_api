<?php

namespace app\modules\metadatacsgo;

/**
 * admin module definition class
 * @property \yii\web\user user
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\metadatacsgo\controllers';
}
