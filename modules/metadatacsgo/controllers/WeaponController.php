<?php

namespace app\modules\metadatacsgo\controllers;

use app\controllers\RestController;
use app\modules\common\controllers\CommonController;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use app\modules\metadatacsgo\services\MetadatacsgoService;
use Elasticsearch\ClientBuilder;

class WeaponController extends RestController
{
    public function actionGet(){
        $params = $this->pGet();
        $info = MetadatacsgoService::getWeaponList($params);
        return $info;
    }

    public function actionDetail(){
        $weapon_id = $this->pGet('weapon_id');
        $info = MetadatacsgoService::getWeaponDetail($weapon_id);
        return $info;
    }
}
