<?php

namespace app\modules\metadatacsgo\controllers;

use app\controllers\RestController;
use app\modules\common\controllers\CommonController;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use app\modules\metadatacsgo\services\MetadatacsgoService;
use Elasticsearch\ClientBuilder;

class MapController extends RestController
{
    public function actionGet(){
        $params = $this->pGet();
        $info = MetadatacsgoService::getMapList($params);
        return $info;
    }

    public function actionDetail(){
        $map_id = $this->pGet('map_id');
        $info = MetadatacsgoService::getMapDetail($map_id);
        return $info;
    }
}
