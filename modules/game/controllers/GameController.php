<?php

namespace app\modules\game\controllers;

use app\controllers\RestController;
use app\modules\game\services\GameService;
use app\modules\common\services\EsService;
use app\modules\common\controllers\CommonController;
use app\modules\match\services\Common;
use Elasticsearch\ClientBuilder;

class GameController extends RestController
{
    public function actionGet(){
        $params = $this->pGet();
        $info = GameService::getGameList($params);
        return $info;
    }

    public function actionDetail(){
        $game_id = $this->pGet('game_id');
        $info = GameService::getGameDetail($game_id);
        return $info;
    }
}
