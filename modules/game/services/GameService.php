<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/31
 * Time: 22:00
 */
namespace app\modules\game\services;

use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\ApiBase;
use app\modules\common\services\RedisBase;

class GameService
{

    public static function getGameList($params)
    {
        // 过滤检索条件
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_game_list';
        // 筛选
        if (isset($params['conditions'])){
            $conditions = $params['conditions'];
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'game_id', 'sort' => 'asc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $client = EsService::initConnect();
        $res     = $client->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Games',$data['game_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getGameDetail($gameId)
    {
        if(!$gameId){
            return [];
        }
        // redis取值
        $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Games',$gameId]);
        $info = [];
        if($json){
            $info = json_decode($json,true);
        }
        return $info;
    }
}