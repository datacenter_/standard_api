<?php
namespace app\modules\admin\controllers;

use app\modules\admin\services\PowerService;

use yii\rest\Controller;

/**
 * Class PowerController
 * @package app\modules\admin\controllers
 */
class PowerController extends Controller
{
    // 丁可专用 - 获取权限
    public function actionGetPower()
    {
        $token = \Yii::$app->getRequest()->get('token',null);
        $matchId = \Yii::$app->getRequest()->get('match_id',null);
        return PowerService::getPower($token,$matchId);
    }
    // 丁可专用 - token是否有效
    public function actionPowerEffective()
    {
        $token = \Yii::$app->getRequest()->get('token',null);
        return PowerService::getPowerEffective($token);
    }
}