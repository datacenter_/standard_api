<?php
namespace app\modules\admin\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\admin\services\AuthService;
use app\modules\admin\services\User\UserService;

/**
 * Class AdminManagerController
 * @package app\modules\admin\controllers
 */
class UserController extends WithTokenAuthController
{
    /**
     * 根据token获取用户信息
     */
    public function actionTokenToUser()
    {
        $userInfo = $this->getIdentity();
        $tokenUserInfo = [
            'id' => $userInfo->id,
            'name' => $userInfo->name,
            'username' => $userInfo->username,
            'email' => $userInfo->email,
        ];
        return $tokenUserInfo;
//        return UserService::getUser($uid);
    }

    /**
     * 用户数据权限
     * 管理用户的套餐
     */
    public function actionDataPower()
    {
        $uid = $this->getIdentity()->getId();
        $userGroupId = $this->getIdentity()->getGroupId();
        return UserService::getUserDataPower($uid,$userGroupId);
    }

    /**
     * 用户访问数据统计日志记录
     */
    public function actionDataStatisticsRecord()
    {
        $params = \Yii::$app->getRequest()->get();
        $uid = $this->getIdentity()->getId();
        return UserService::getDataStatisticsRecord($uid,$params);
    }
    /**
     * 用户访问数据日志记录
     */
    public function actionDataRecord()
    {
        $params = \Yii::$app->getRequest()->get();
        $uid = $this->getIdentity()->getId();
        return UserService::getDataRecord($uid,$params);
    }

    /**
     * 获取详情
     */
    public function actionGetAuthInfo()
    {
        return $this->getIdentity();
    }

    /**
     * 退出登陆
     */
    public function actionLogout()
    {
        $token = $this->getIentityToken();

        return AuthService::logout($token);
    }
}
