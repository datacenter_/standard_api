<?php
namespace app\modules\admin\controllers;

use app\controllers\RestController;
use app\controllers\WithTokenAuthController;
use app\modules\admin\services\PersonalCenter\PersonalCenterService;

/**
 * Class PersonalCenterController
 * @package app\modules\admin\controllers
 */
class PersonalCenterController extends WithTokenAuthController
{
    // 重置邮箱 - 验证密码 - 成功后-发送验证码
    public function actionResetSendEmail()
    {
        $uid = $this->getIdentity()->getId();
        $attributes = \Yii::$app->getRequest()->post();
        return PersonalCenterService::ResetSendEmail($attributes,$uid);
    }
    // 重置邮箱 - 验证当前邮箱验证码 - 是否通过
    public function actionResetEmailCode()
    {
        $code = \Yii::$app->getRequest()->post('code');
        $uid = $this->getIdentity()->getId();
        return PersonalCenterService::ResetEmailCode($code,$uid);
    }
    // 重置邮箱 - 验证新邮箱 - 发送验证码
    public function actionResetEmailSend()
    {
        $email = \Yii::$app->getRequest()->post('email');
        $uid = $this->getIdentity()->getId();
        return PersonalCenterService::NewMailboxSendCode($email,$uid);
    }
    // 重置邮箱 - 验证新邮箱验证码是否通过 - 最后修改邮箱
    public function actionNewEmailCodeSave()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $uid = $this->getIdentity()->getId();
        return PersonalCenterService::NewEmailCode($attributes,$uid);
    }

    // 个人用户账户安全
    public function actionImproveInformation()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $uid = $this->getIdentity()->getId();
        return PersonalCenterService::ImproveInformation($attributes,$uid);
    }

    // 个人用户账号信息
    public function actionAccountInformation()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $uid = $this->getIdentity()->getId();
        return PersonalCenterService::accountInformation($attributes,$uid);
    }
}