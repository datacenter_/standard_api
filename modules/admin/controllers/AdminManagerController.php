<?php
namespace app\modules\admin\controllers;

use app\controllers\WithTokenAuthController;
use app\modules\admin\services\AuthService;

/**
 * Class AdminManagerController
 * @package app\modules\admin\controllers
 */
class AdminManagerController extends WithTokenAuthController
{
    /**
     * 新增
     * @return mixed
     * @throws \app\rest\exceptions\RequestParamsUnvalidatedException
     */
    public function actionAdd()
    {
        $attributes = \Yii::$app->getRequest()->post();

        return AuthService::register($attributes);
    }

    /**
     * 修改
     * @return mixed
     * @throws \app\rest\exceptions\RequestParamsUnvalidatedException
     */
    public function actionUpdate()
    {
        $attributes = \Yii::$app->getRequest()->post();

        return AuthService::register($attributes);
    }

    /**
     * 更改密码
     */
    public function actionRePassword()
    {
        $attributes = \Yii::$app->getRequest()->post();

        return AuthService::rePassword($attributes);
    }

    /**
     * 管理员列表
     * @return array
     */
    public function actionList()
    {
        $params = \Yii::$app->getRequest()->get();

        return AuthService::adminList($params);
    }

    /**
     * 获取权限
     * @return AuthService[]
     */
    public function actionGetPermission()
    {
        return AuthService::getPermission();
    }


    /**
     * 删除管理员
     * @return array
     * @throws \app\rest\exceptions\BusinessException
     */
    public function actionDelete()
    {
        $post = \Yii::$app->getRequest()->post();

        return AuthService::delete($post);
    }
}