<?php
namespace app\modules\admin\controllers;

//use app\controllers\RestController;
use app\controllers\WithTokenAuthController;
use app\modules\admin\services\Attestation\AttestationService;

/**
 * Class AttestationController
 * @package app\modules\admin\controllers
 */
class AttestationController extends WithTokenAuthController
{
    // 认证信息
    public function actionDetail()
    {
        $uid = $this->getIdentity()->getId();
        return AttestationService::detailAttestation($uid);
    }
    // 企业认证
    public function actionEnterprise()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $uid = $this->getIdentity()->getId();
        return AttestationService::setEnterprise($attributes,$uid);
    }
    // 个人认证
    public function actionPersonal()
    {
        $attributes = \Yii::$app->getRequest()->post();
        $uid = $this->getIdentity()->getId();
        return AttestationService::setPersonal($attributes,$uid);
    }
    // 地区数据
    public function actionRegions()
    {
        return AttestationService::getRegions();
    }
}