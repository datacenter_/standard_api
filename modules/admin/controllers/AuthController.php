<?php

namespace app\modules\admin\controllers;

use app\controllers\GuestController;
use app\modules\admin\services\AuthService;
use app\modules\common\services\ImageConversionHelper;
use app\modules\common\services\AdminBase;

/**
 * Class AuthController
 * @package app\modules\admin\controllers
 */
class AuthController extends GuestController
{
    /**
     * 登录
     * @return array
     * @throws \app\rest\exceptions\AuthenticationFailedException
     */
    public function actionLogin()
    {
        $attributes = \Yii::$app->getRequest()->post();
        return AuthService::login($attributes);
    }
    /**
     * 商务后台登录
     * @return array
     * @throws \app\rest\exceptions\AuthenticationFailedException
     */
    public function actionBusinessLogin()
    {
        $attributes = \Yii::$app->getRequest()->post();
        return AuthService::BusinessLogin($attributes);
    }
    /**
     * 注册
     * @return mixed
     * @throws \app\rest\exceptions\RequestParamsUnvalidatedException
     */
    public function actionRegistrator()
    {
        $attributes = \Yii::$app->getRequest()->post();
        return AuthService::registrator($attributes);
    }
    /**
     * 忘记密码-发送邮件
     * @return mixed
     * @throws \app\rest\exceptions\RequestParamsUnvalidatedException
     */
    public function actionSendEmail()
    {
        $email = \Yii::$app->getRequest()->post('email');
        return AdminBase::modifySendToEmail($email);
    }
    /**
     * 注册-发送邮件
     * @return mixed
     * @throws \app\rest\exceptions\RequestParamsUnvalidatedException
     */
    public function actionRegisterSendEmail()
    {
        $email = \Yii::$app->getRequest()->post('email');
        return AdminBase::modifySendToEmail($email,true);
    }
    /**
     * 忘记密码-验证
     * @return mixed
     * @throws \app\rest\exceptions\RequestParamsUnvalidatedException
     */
    public function actionModifyPassword()
    {
        $attributes = \Yii::$app->getRequest()->post();
        return AuthService::setModifyPassword($attributes);
    }

    public function actionDown()
    {
        $url=$this->pGet('url');
        $info=ImageConversionHelper::downfile($url);
        return $info;
    }
}