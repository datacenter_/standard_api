<?php
namespace app\modules\admin\controllers;

//use app\controllers\RestController;
use app\controllers\WithTokenAuthController;
use app\modules\admin\services\Secret\SecretService;

/**
 * Class SecretController
 * @package app\modules\admin\controllers
 */
class SecretController extends WithTokenAuthController
{
    // 新增token
    public function actionAdd()
    {
        $uid = $this->getIdentity()->getId();
        return SecretService::setSecret($uid);
    }
    // 获取当前用户所有token
    public function actionGet()
    {
        $uid = $this->getIdentity()->getId();
        return SecretService::getSecret($uid);
    }
    // 重置token
    public function actionRevoke()
    {
        $uid = $this->getIdentity()->getId();
        $tokenId = \Yii::$app->getRequest()->get('token_id',null);
        return SecretService::revokeSecret($tokenId,$uid);
    }
    // 生效/撤销 token
    public function actionTokenEdit()
    {
        $uid = $this->getIdentity()->getId();
        $tokenId = \Yii::$app->getRequest()->get('token_id',null);
        $isRevoke = \Yii::$app->getRequest()->get('is_revoke',null);
        return SecretService::editSecret($tokenId,$isRevoke,$uid);
    }
}