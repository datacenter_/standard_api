<?php
return [
    'components' => [
        'urlManager' => [
            'rules' => [
                [
                    'class' => 'yii\web\GroupUrlRule',
                    'prefix' => 'v1/admin',
                    'rules' => [
                        'POST login' => 'auth/login',
                        'POST <action>' => 'admin-manager/<action>',
                        'GET,POST users/<action>' => 'user-manager/<action>',
                        'POST users/behaviors/<id:\d+>' => 'user-manager/behaviors',
                        'POST schedules/behaviors/<id:\d+>' => 'schedule-manager/behaviors',
                        'POST kols/behaviors/<id:\d+>' => 'kol-manager/behaviors',
                    ]
                ]
            ]
        ],
    ],
];
