<?php
namespace app\modules\admin\forms;

use yii\base\Model;

/**
 * Class LoginForm
 * @package app\modules\admin\forms
 */
class BusinessLoginForm extends Model
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $password;

    public function rules()
    {
        return [
            [['name', 'password'], 'trim'],
            [['name', 'password'], 'required'],
            [['name', 'password'], 'string', 'max' => 100, 'min' => 1],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => '用户名',
            'password' => 'Incorrect Password', // 密码
        ];
    }
}