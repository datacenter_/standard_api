<?php
namespace app\modules\admin\forms;

use app\modules\common\models\ApiPersonalCertification;
use yii\base\Model;

/**
 * Class EnterpriseForm
 * @package app\modules\admin\forms
 */
class PersonalForm extends Model
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $surname;

    /**
     * @var integer
     */
    public $country;

    /**
     * @var integer
     */
    public $province_municipalitie;

    /**
     * @var integer
     */
    public $city;

    /**
     * @var string
     */
    public $detailed_address;

    public function rules()
    {
        return [
            [['name', 'surname', 'detailed_address','country','province_municipalitie','city'], 'trim'],
            [['name', 'surname', 'detailed_address'], 'required'],
            [['name', 'surname', 'detailed_address'], 'string', 'max' => 100, 'min' => 1],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'surname' => 'Surname',
            'business_address' => 'Business address',
        ];
    }
}