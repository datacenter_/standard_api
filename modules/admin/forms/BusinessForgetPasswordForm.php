<?php
namespace app\modules\admin\forms;

use yii\base\Model;

/**
 * Class LoginForm
 * @package app\modules\admin\forms
 */
class BusinessForgetPasswordForm extends Model
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $old_password;

    /**
     * @var string
     */
    public $new_password;

    public function rules()
    {
        return [
            [['name', 'old_password', 'new_password'], 'trim'],
            [['name', 'old_password', 'new_password'], 'required'],
            [['name'], 'string', 'max' => 20, 'min' => 4],
            [['old_password', 'new_password'], 'string', 'max' => 18, 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => '用户名',
            'old_password' => 'Old Password', // 密码
            'new_password' => 'New Password', // 密码
        ];
    }
}