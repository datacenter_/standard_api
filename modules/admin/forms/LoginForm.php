<?php
namespace app\modules\admin\forms;

use yii\base\Model;

/**
 * Class LoginForm
 * @package app\modules\admin\forms
 */
class LoginForm extends Model
{
    /**
     * @var string
     */
//    public $username;
    public $email;

    /**
     * @var string
     */
    public $password;

    public function rules()
    {
        return [
//            [['username', 'password'], 'trim'],
//            [['username', 'password'], 'required'],
//            [['username', 'password'], 'string', 'max' => 100, 'min' => 1],
            [['email', 'password'], 'trim'],
            [['email', 'password'], 'required'],
            [['email', 'password'], 'string', 'max' => 100, 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
//            'username' => '用户名',
            'email' => 'Email address', // 用户名
            'password' => 'Incorrect Password', // 密码
        ];
    }
}