<?php
namespace app\modules\admin\forms;

use app\modules\admin\models\Admin;
use yii\base\Model;

/**
 * Class RegistratorForm
 * @package app\modules\admin\forms
 */
class RegistratorForm extends Model
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var integer
     */
    public $code;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $confirm_password;

    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => 'Email is invalid or already taken!'],
            ['email', 'email', 'message' => 'Email is invalid or already taken!'],
            ['email', 'unique', 'targetClass' => Admin::class, 'message' => 'Email is invalid or already taken!'],
            ['code', 'required', 'message' => 'Verification code is invalid!'],
            ['code', 'integer', 'message' => 'Verification code is invalid!'],
            [['password', 'confirm_password'], 'required'],
            [['password', 'confirm_password'], 'string', 'min' => 6, 'max' => 16, 'message' => "{attribute}It's 6-16 digits or letters"],
            ['confirm_password', 'compare', 'compareAttribute' => 'password', 'message' => 'The two passwords are inconsistent'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email address',
            'code' => 'Verification code is invalid',
            'password' => 'Incorrect Password',
            'confirm_password' => 'Confirm password',
        ];
    }
}