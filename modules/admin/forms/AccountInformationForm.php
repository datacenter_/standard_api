<?php
namespace app\modules\admin\forms;

use yii\base\Model;

/**
 * Class ImproveInformationForm
 * @package app\modules\admin\forms
 */
class AccountInformationForm extends Model
{
    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $cell_phone;

    /**
     * @var string
     */
    public $tele_phone;

    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $time_zone;

    public function rules()
    {
        return [
            [['username', 'country', 'time_zone', 'tele_phone', 'cell_phone'], 'trim'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'User name',
            'country' => 'Country',
            'time_zone' => 'Time zone',
            'tele_phone' => 'Tele phone',
            'cell_phone' => 'Cell phone',
        ];
    }
}