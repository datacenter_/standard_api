<?php
namespace app\modules\admin\forms;

use yii\base\Model;

/**
 * Class ForgetPasswordForm
 * @package app\modules\admin\forms
 */
class ForgetPasswordForm extends Model
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var integer
     */
    public $code;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $confirm_password;

    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['code', 'required'],
            ['code', 'integer'],

            [['password', 'confirm_password'], 'required'],
            [['password', 'confirm_password'], 'string', 'min' => 6, 'max' => 100, 'message' => "{attribute}It's 6-16 digits or letters"],
            ['confirm_password', 'compare', 'compareAttribute' => 'password', 'message' => 'The two passwords are inconsistent'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email address',
            'code' => 'Verification code is invalid',
        ];
    }
}