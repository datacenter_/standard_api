<?php
namespace app\modules\admin\forms;

use yii\base\Model;

/**
 * Class EnterpriseForm
 * @package app\modules\admin\forms
 */
class EnterpriseForm extends Model
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $business_address;

    /**
     * @var integer
     */
    public $country;

    /**
     * @var integer
     */
    public $province_municipalitie;

    /**
     * @var integer
     */
    public $city;

    /**
     * @var string
     */
    public $legal_name;

    /**
     * @var string
     */
    public $surname;

    public function rules()
    {
        return [
            [['name', 'business_address', 'legal_name', 'surname','country','province_municipalitie','city'], 'trim'],
            [['name', 'business_address', 'legal_name', 'surname'], 'required'],
            [['name', 'business_address', 'legal_name', 'surname'], 'string', 'max' => 100, 'min' => 1],
//            ['name', 'unique', 'targetClass' => ApiEnterpriseCertificate::class, 'message' => '公司名称已存在!'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
//            ['email', 'unique', 'targetClass' => ApiEnterpriseCertificate::class, 'message' => '邮箱名已存在!'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Enterprise mailbox',
            'name' => 'Business name',
            'business_address' => 'Company address',
            'legal_name' => 'Corporate name',
            'surname' => 'Surname of legal person',
        ];
    }
}