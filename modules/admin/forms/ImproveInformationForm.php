<?php
namespace app\modules\admin\forms;

use app\modules\admin\models\Admin;
use yii\base\Model;

/**
 * Class ImproveInformationForm
 * @package app\modules\admin\forms
 */
class ImproveInformationForm extends Model
{
    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $original_password;

    /**
     * @var string
     */
    public $confirm_password;

    public function rules()
    {
        return [
//            [['username'], 'string'],
            ['username', 'string', 'min' => 1, 'max' => 20, 'message' => 'User name 1-20 digits or letters'],
//            [['password', 'confirm_password'], 'required'],
            [['password', 'confirm_password', 'original_password'], 'string', 'min' => 6, 'max' => 16, 'message' => "{attribute}It's 6-16 digits or letters"],
            ['confirm_password', 'compare', 'compareAttribute' => 'password', 'message' => 'The two passwords are inconsistent'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'User name',
            'original_password' => 'Incorrect Old Password',
            'password' => 'New password',
            'confirm_password' => 'Confirm new password',
        ];
    }
}