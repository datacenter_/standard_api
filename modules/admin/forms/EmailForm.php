<?php
namespace app\modules\admin\forms;

use yii\base\Model;

/**
 * Class EmailForm
 * @package app\modules\admin\forms
 */
class EmailForm extends Model
{
    /**
     * @var string
     */
    public $email;

    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email address',
        ];
    }
}