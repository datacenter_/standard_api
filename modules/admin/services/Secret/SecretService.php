<?php
namespace app\modules\admin\services\Secret;

use app\modules\common\models\ApiToken;
use app\modules\common\services\AdminBase;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\Consts;
use app\modules\common\services\RedisBase;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RequestParamsUnvalidatedException;

/**
 * Class SecretService
 * @package app\modules\admin\services\Secret
 */
class SecretService
{
    // 生成token
    public static function setSecret($uid)
    {
        if($uid) {
            // 生成token
            return AdminBase::addApiToken($uid);
        }
        throw new BusinessException([],Consts::FAILED_TO_GENERATE_TOKEN_ERROR);
    }
    // 重置 - token
    public static function revokeSecret($tokenId,$uid)
    {
        $tokenInfo = ApiToken::find()->where(['id'=>$tokenId])->one();
        if(!empty($tokenInfo)) {
            $token = AdminBase::getApiToken($uid);
            // redis - 删除当前token
            RedisBase::keyDel([ApiConstant::PREFIX_WEB_API,'Token', $tokenInfo->token]);
            // 修改token
            $tokenInfo->setAttributes([
                'token' => $token,
            ]);
            if (!$tokenInfo->save()) {
                throw new BusinessException($tokenInfo->getErrors(), Consts::RESET_TOKEN_FAILED_ERROR);
            }
        }
        return true;
    }
    // 生效/撤销 token
    public static function editSecret($tokenId,$isRevoke,$uid)
    {
        // $isRevoke 1-正常，2-撤销
        if($tokenId && in_array($isRevoke,[1,2])){
            $revokeCode = $isRevoke==1?2:1;
            $tokenInfo = ApiToken::find()->where(['id'=>$tokenId])->one();
            if(!empty($tokenInfo)) {
                // 撤销转生效
                if($revokeCode==1){
                    // redis - 存入token的用户id
                    RedisBase::valueSet([ApiConstant::PREFIX_WEB_API,'Token',$tokenInfo->token],$uid,true);
                }else{
                // 生效转撤销
                    // redis - 删除撤销的token
                    RedisBase::keyDel([ApiConstant::PREFIX_WEB_API,'Token', $tokenInfo->token]);
                }
                $tokenInfo->setAttributes([
                    'is_revoke' => $revokeCode,
                ]);
                if (!$tokenInfo->save()) {
                    throw new BusinessException($tokenInfo->getErrors(), Consts::FAILED_TO_OPERATE_TOKEN_ERROR);
                }
            }
        }else{
            // 参数不合法
            throw new RequestParamsUnvalidatedException();
        }
        return true;
    }
    // 获取token
    public static function getSecret($uid)
    {
        return ApiToken::find()
            ->select(['id','token','is_revoke','created_at','revoke_at'])
            ->where(['user_id'=>$uid])
            ->asArray()
            ->all();
    }
}