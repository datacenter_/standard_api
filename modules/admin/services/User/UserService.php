<?php

namespace app\modules\admin\services\User;

use app\modules\admin\models\Admin;
use app\modules\common\models\ApiGame;
use app\modules\common\models\ApiSetmeal;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;

class UserService
{
    // token获取用户详情
    public static function getUser($uid)
    {
        $userInfo = Admin::find()
            ->where(['id'=>$uid])
            ->asArray()
            ->one();
        unset($userInfo['password']);
        return $userInfo;
    }

    const PERMANENT_DATE = '2075-01-01 00:00:00';
    const UNEXPIRED_STATUS = 1;
    // 用户数据权限
    public static function getUserDataPower($userId,$userGroupId)
    {
        // 所有游戏数据
        $q = ApiGame::find()->alias('g');
        // 该用户有的游戏权限
        switch ($userGroupId){
            case ApiConstant::GROUP_INTERNAL_ID: // 内部版
                $games = $q->select('id,name,e_name,e_short,logo')->asArray()->all();
                array_walk($games, function (&$value) {
                    $value = array_merge(
                        $value,
                        ['is_stale' => self::UNEXPIRED_STATUS, 'expiration_at' => self::PERMANENT_DATE]
                    );
                } );
                // 套餐类型
                $setMeal = [
                    'group_name' => 'Internal version',
                    'group_name_cn' => ApiConstant::GROUP_INTERNAL_VERSION,
                    'maturities' => self::PERMANENT_DATE, // 永久
                ];
                break;
            case ApiConstant::GROUP_PROFESSIONAL_ID: // 专业版
                // 套餐游戏
                $games = self::getPackageGame($userId,$q);
                // 套餐类型
                $setmeal = ApiSetmeal::find()->select('expiration_at')->where(['user_id'=>$userId])->one();
                // 套餐类型
                $setMeal = [
                    'group_name' => 'Professional version',
                    'group_name_cn' => ApiConstant::GROUP_PROFESSIONAL_EDITION,
                    'maturities' => $setmeal->expiration_at,
                ];
                break;
            case ApiConstant::GROUP_REGULAR_ID: // 普通版
                // 套餐游戏
                $games = self::getPackageGame($userId,$q);
                // 套餐类型
                $setmeal = ApiSetmeal::find()->select('expiration_at')->where(['user_id'=>$userId])->one();
                // 套餐类型
                $setMeal = [
                    'group_name' => 'Normal version',
                    'group_name_cn' => ApiConstant::GROUP_REGULAR_EDITION,
                    'maturities' => $setmeal->expiration_at,
                ];
                break;
            case ApiConstant::GROUP_FREE_ID: // 免费版
                // 套餐游戏
                $games = self::getPackageGame($userId,$q);
                // 套餐类型
                $setMeal = [
                    'group_name' => 'Free version',
                    'group_name_cn' => ApiConstant::GROUP_FREE_VERSION,
                    'maturities' => self::PERMANENT_DATE, // 永久
                ];
                break;
            case ApiConstant::NEW_USER_VERSION_ID: // 新用户版
                // 套餐游戏
                $games = self::getPackageGame($userId,$q,true);
                $setMeal = [];
                break;
            default:
                return [];
        }
        return [
            'games'    => $games,
            'set_meal' => $setMeal,
        ];
    }
    // 套餐游戏
    public static function getPackageGame($userId,$q,$newUser=false)
    {
        // 套餐游戏
        $games = $q->select('g.id,g.name,g.e_name,g.e_short,g.logo,sg.is_stale,sg.expiration_at')
            ->leftJoin('api_setmeal_geme as sg', "sg.game_id = g.id and sg.user_id = {$userId}")
            ->orderBy('g.id asc')
            ->asArray()->all();
        if($newUser){
            return $games;
        }
        // 免费3个游戏
        for ($i=0; $i<3; $i++) {
            $games[$i]['is_stale'] = self::UNEXPIRED_STATUS;
            $games[$i]['expiration_at'] = self::PERMANENT_DATE;
        }
        return $games;
    }


    // 用户访问数据统计记录
    public static function getDataStatisticsRecord_news($userId,$params)
    {

    }

    // 用户访问数据统计记录
    public static function getDataStatisticsRecord($userId,$params)
    {
        $params['per_page'] = !empty($params['per_page']) ? $params['per_page']:15;
        $conditions = [
            ['field' => 'user_id', 'type' => '=', 'value' => $userId]
        ];
        $columns = ['path','p90','p99'];
        if(isset($params['duration_type'])){
            $duration_type = $params['duration_type'];
            if(in_array($duration_type,['1h','1d','3d'])){
                $columns = array_merge($columns,["count_{$duration_type}","sum_res_{$duration_type}"]);
            }else{
                return [
                    'total' => [],
                    'list' => [],
                    'count' => 0,
                ];
            }
        }else{
            return [
                'total' => [],
                'list' => [],
                'count' => 0,
            ];
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $EsService = EsService::initConnect();
        $res = $EsService->search('guoguo',$conditions,$offset,$params['per_page'],$columns);
        $totalRequests = 0; // 请求 总数
        $totalResponseByte = 0; // 响应字节 总数
        $totalP90 = 0; // P90 延迟 总数
        $totalP99 = 0; // P99 延迟 总数
        if(!empty($res['list'])) {
            $countData = $EsService->searchAll('guoguo', 'user_id', $userId, $columns);
            foreach ($countData as $k => $info) {
                $totalRequests += $info["count_{$duration_type}"];
                $totalResponseByte += $info["sum_res_{$duration_type}"];
                $totalP90 += $info['p90'];
                $totalP99 += $info['p99'];
            }
        }
        $res['total'] = [
            'total_requests' => $totalRequests,
            'total_requests_byte' => $totalResponseByte,
            'total_p90' => $totalP90,
            'total_p99' => $totalP99,
        ];
        return $res;
    }
    // 用户访问数据日志记录
    public static function getDataRecord($userId,$params)
    {
        $params['per_page'] = !empty($params['per_page']) ? $params['per_page']:15;
        $conditions = [
            ['field' => 'user_id', 'type' => '=', 'value' => $userId]
        ];
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $EsService = EsService::initConnect();
        return $EsService->search('guoshuai',$conditions,$offset,$params['per_page'],['path','node','parameter','run_time','date']);
    }
}
