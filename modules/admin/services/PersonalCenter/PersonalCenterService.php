<?php
namespace app\modules\admin\services\PersonalCenter;

use app\constants\Contract;
use app\modules\admin\forms\AccountInformationForm;
use app\modules\admin\forms\EmailForm;
use app\modules\admin\forms\ImproveInformationForm;
use app\modules\common\services\AdminBase;
use app\modules\common\services\Consts;
use app\rest\exceptions\AuthenticationFailedException;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RequestParamsUnvalidatedException;
use app\modules\admin\models\Admin;

/**
 * Class PersonalCenterService
 * @package app\modules\admin\services\PersonalCenter
 */
class PersonalCenterService
{
    // 重置邮箱 - 验证密码 - 成功后发送验证码
    public static function ResetSendEmail($attributes,$uid)
    {
        $identity = Admin::getByUid($uid);
        // 验证密码
        if (!empty($identity) && $identity->is_enable == Contract::COMMON_DB_YES
            && \Yii::$app->getSecurity()->validatePassword($attributes['password'], $identity->password)) {
            unset($identity['password']);
            // 发送验证码
            return AdminBase::sendToEmail($identity->email,$uid);
        }else{
            throw new AuthenticationFailedException([],Consts::YOUR_PASSWORD_IS_WRONG_ERROR);
        }
    }

    // 重置邮箱 - 验证当前邮箱验证码是否通过
    public static function ResetEmailCode($code,$uid)
    {
        // 验证code
        return AdminBase::verificationCode($code,$uid);
    }

    // 重置邮箱 - 新邮箱发送验证码
    public static function NewMailboxSendCode($email,$uid)
    {
        $form = new EmailForm();
        $form->setAttributes(['email'=>$email]);
        if ($form->validate()) {
            // 发送验证码
            return AdminBase::sendToEmail($email,$uid,true);
        }else{
            throw new RequestParamsUnvalidatedException([],'Email is invalid or already taken');
        }
    }
    // 重置邮箱 - 验证新邮箱验证码是否通过 - 最后修改邮箱
    public static function NewEmailCode($attributes,$uid)
    {
        $form = new EmailForm();
        $form->setAttributes(['email'=>$attributes['email']]);
        if ($form->validate()) {
            // 验证验证码
            $codeRes = AdminBase::verificationCode($attributes['code'],$uid);
            if($codeRes){
                $userInfo = Admin::getByUid($uid);
                $userInfo->email = $form->email;
                if (!$userInfo->save()) {
                    throw new BusinessException([],Consts::RESET_MAILBOX_FAILED_ERROR);
                }
            }
        }else{
            throw new RequestParamsUnvalidatedException($form->getErrors());
        }
        return true;
    }

    // 个人用户账号信息
    public static function accountInformation($attributes,$uid)
    {
        $form = new AccountInformationForm();
        $form->setAttributes($attributes);
        if ($form->validate()) {
            $identity = Admin::getByUid($uid);
            $identity->username = $form->username;
            $identity->cell_phone = $form->cell_phone;
            $identity->tele_phone = $form->tele_phone;
            $identity->country = $form->country;
            $identity->time_zone = $form->time_zone;
        }else{
            throw new RequestParamsUnvalidatedException($form->getErrors());
        }
        if (!$identity->save()) {
            throw new BusinessException([],Consts::FAILED_TO_IMPROVE_INFORMATION_ERROR);
        }
        return true;
    }

    // 个人用户账户安全
    public static function ImproveInformation($attributes,$uid)
    {
        $form = new ImproveInformationForm();
        $form->setAttributes($attributes);
        if ($form->validate()) {
            $identity = Admin::getByUid($uid);
            if($form->username && $form->password && $form->confirm_password && $form->original_password){
                // 验证密码
                if (!empty($identity) && $identity->is_enable == Contract::COMMON_DB_YES
                    && \Yii::$app->getSecurity()->validatePassword($attributes['original_password'], $identity->password)) {
                    unset($identity['password']);
                    // 修改信息
                    $identity->username = $form->username;
                    $identity->password = \Yii::$app->getSecurity()->generatePasswordHash($form->password);
                }else{
                    throw new AuthenticationFailedException([],Consts::ORIGINAL_PASSWORD_ERROR);
                }
            }else {
                if($form->username){
                    // 修改信息
                    $identity->username = $form->username;
                }
                if ($form->password && $form->confirm_password && $form->original_password) {
                    // 验证密码
                    if (!empty($identity) && $identity->is_enable == Contract::COMMON_DB_YES
                        && \Yii::$app->getSecurity()->validatePassword($attributes['original_password'], $identity->password)) {
                        unset($identity['password']);
                        // 修改信息
                        $identity->password = \Yii::$app->getSecurity()->generatePasswordHash($form->password);
                    }else{
                        throw new AuthenticationFailedException([],Consts::ORIGINAL_PASSWORD_ERROR);
                    }
                }
            }
        }else{
            throw new RequestParamsUnvalidatedException($form->getErrors());
        }
        if (!$identity->save()) {
            throw new BusinessException([],Consts::FAILED_TO_IMPROVE_INFORMATION_ERROR);
        }
        return true;
    }
}