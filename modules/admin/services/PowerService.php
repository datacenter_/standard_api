<?php

namespace app\modules\admin\services;

use app\modules\common\services\ApiBase;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\AuthorityDetection;
use app\modules\common\services\EsService;
use app\rest\exceptions\EntityNotExistException;
use app\rest\exceptions\NoOperationPermissionException;
use app\rest\exceptions\RequestParamsUnvalidatedException;
use app\rest\exceptions\UnauthorizedException;

class PowerService
{
    // 获取权限
    public static function getPower($token,$matchId)
    {
        // 检测token
        if(!$token){
            // token未授权
            throw new UnauthorizedException();
        }
        // match_id 必须参数
        if($matchId){
            $EsService = EsService::initConnect();
            $matchInfo = $EsService->getFile('resource_match_list',$matchId);
            if ($matchInfo) {
                // 可变的延迟时间
                if (isset($matchInfo['delay'])) {
                    $matchDelaySecond = $matchInfo['delay'];
                }else{
                    $matchDelaySecond = 0;
                }
                // 游戏id
                $gameId = $matchInfo['game_id'];
            } else {
                // 没有该比赛数据
                throw new EntityNotExistException();
            }
        }else{
            // matchId 错误
            throw new RequestParamsUnvalidatedException([],"请传入match_id参数！");
        }
        if($matchId && $token=='Nmi-sBmEGMmPcJfKt_aGNWEXtP2KcwEFM1XHDTCn00T-0NpKPH1') {
//            $matchDelay = ApiGroup::find()->select(['group_name','delay'])->asArray()->all();
            // 权限组数组 - 暂时定死
            $userGroupDelay = [
                0 => [
                    'group_name' => ApiConstant::GROUP_INTERNAL_VERSION,
                    'delay' => ApiConstant::GROUP_INTERNAL_VERSION_DELAY,
                ],
                1 => [
                    'group_name' => ApiConstant::GROUP_PROFESSIONAL_EDITION,
                    'delay' => ApiConstant::GROUP_PROFESSIONAL_EDITION_DELAY,
                ],
                2 => [
                    'group_name' => ApiConstant::GROUP_REGULAR_EDITION,
                    'delay' => ApiConstant::GROUP_REGULAR_EDITION_DELAY,
                ],
                3 => [
                    'group_name' => ApiConstant::GROUP_FREE_VERSION,
                    'delay' => ApiConstant::GROUP_FREE_VERSION_DELAY,
                ],
            ];
            $userGroupDelayArray = [];
            foreach ($userGroupDelay as $key=>&$groupInfo){
                $userGroupDelayArray[] = self::getGroupInfo($groupInfo,$matchDelaySecond);
            }
            return $userGroupDelayArray;
        }else{
            // 用户id
            $userId = AuthorityDetection::getAuthUserId($token);
            $userGroupId = AuthorityDetection::getAuthGroupId($userId);
            // 内部版不用验证套餐
            if ($userGroupId != 1) {
                // 验证套餐
                AuthorityDetection::versionPackage($userId);
                if (isset(\Yii::$app->params['group'])) {
                    // 免费版 没有操作权限
                    throw new NoOperationPermissionException();
                } else {
                    // 验证游戏套餐
                    AuthorityDetection::versionGamesPackage($userId);
                    ApiBase::verifyTheGame($gameId);
                }
            }
            // 从redis获取用户延迟权限组
            $userGroupInfo = AuthorityDetection::getUserGroupDelay($userId);
            if (!empty($userGroupInfo)) {
                return self::getGroupInfo($userGroupInfo,$matchDelaySecond,true);
            }
        }
        // 没有操作权限
        throw new NoOperationPermissionException();
    }

    // 处理版本组 计算延迟时间
    public static function getGroupInfo($groupInfo,$matchDelaySecond,$power=false)
    {
        $currentGroupInfo = $groupInfo;
        switch ($groupInfo['group_name']){
            case ApiConstant::GROUP_PROFESSIONAL_EDITION: //专业版
                if($matchDelaySecond < $groupInfo['delay']){
                    $currentGroupInfo['delay'] = $matchDelaySecond;
                }
                break;
            case ApiConstant::GROUP_REGULAR_EDITION: //普通版
                if($matchDelaySecond>10){
                    $currentGroupInfo['delay'] = $matchDelaySecond-10;
                }else{
                    $currentGroupInfo['delay'] = $matchDelaySecond;
                }
                break;
            case ApiConstant::GROUP_FREE_VERSION: //免费版
                if($power){
                    // 免费版 没有操作权限
                    throw new NoOperationPermissionException();
                }
                $currentGroupInfo['delay'] = $matchDelaySecond;
                break;
            default:
                break;
        }
        return $currentGroupInfo;
    }


    // 超哥对接丁可 api权限接口
    public static function getPowerEffective($token)
    {
        // 检测token
        if(!$token){
            // token未授权
            throw new UnauthorizedException();
        }
        // 用户id
        $userId = AuthorityDetection::getAuthUserId($token);
        // 用户版本组id
        $userGroupId = AuthorityDetection::getAuthGroupId($userId);
        // 只能是内部或者专业版 可以访问
        if (empty($userGroupId) || !in_array($userGroupId,[ApiConstant::GROUP_INTERNAL_ID,ApiConstant::GROUP_PROFESSIONAL_ID])) {
            // 未经授权
            throw new UnauthorizedException();
        }

        return true;
    }
}
