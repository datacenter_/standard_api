<?php
namespace app\modules\admin\services\Attestation;

use app\modules\common\models\ApiCountry;
use app\modules\common\models\ApiEnterpriseCertificate;
use app\modules\common\models\ApiPersonalCertification;
use app\modules\admin\forms\EnterpriseForm;
use app\modules\admin\forms\PersonalForm;
use app\modules\common\services\Consts;
use app\rest\exceptions\BusinessException;
use app\rest\exceptions\RequestParamsUnvalidatedException;

/**
 * Class AttestationService
 * @package app\modules\admin\services\Attestation
 */
class AttestationService
{
    // 认证信息
    public static function detailAttestation($uid)
    {
        $where = ['user_id' => $uid];
        // 查看认证信息
        $EnterpriseInfo = ApiEnterpriseCertificate::find()->where($where)->asArray()->one();
        if(!empty($EnterpriseInfo)){
            // 企业
            $EnterpriseInfo['type'] = 'enterprise';
        }else{
            $EnterpriseInfo = ApiPersonalCertification::find()->where($where)->asArray()->one();
            if(!empty($EnterpriseInfo)){
                // 个人
                $EnterpriseInfo['type'] = 'personal';
            }
        }
        return $EnterpriseInfo;
    }
    // 企业认证
    public static function setEnterprise(array $attributes = [],$uid)
    {
        $form = new EnterpriseForm();
        $form->setAttributes($attributes);
        if ($form->validate()) {
            $enterprise = ApiEnterpriseCertificate::find()->where(['user_id' => $uid])->one();
            if(empty($enterprise))
            {
                $enterprise = ApiEnterpriseCertificate::instance(true);
            }
            $enterprise->user_id = $uid;
            $enterprise->name = $form->name;
            $enterprise->business_address = $form->business_address;
            $enterprise->country = $form->country;
            $enterprise->province_municipalitie = $form->province_municipalitie;
            $enterprise->city = $form->city;
            $enterprise->legal_name = $form->legal_name;
            $enterprise->surname = $form->surname;
            $enterprise->email = $form->email;
            if (!$enterprise->save()) {
                throw new BusinessException([], Consts::ENTERPRISE_CERTIFICATION_FAILED_ERROR);
            }
            return true;
        } else {
            throw new RequestParamsUnvalidatedException($form->getErrors());
        }
    }
    // 个人认证
    public static function setPersonal(array $attributes = [],$uid)
    {
        $form = new PersonalForm();
        $form->setAttributes($attributes);
        if ($form->validate()) {
            $personal = ApiPersonalCertification::find()->where(['user_id' => $uid])->one();
            if(empty($enterprise))
            {
                $personal = ApiPersonalCertification::instance(true);
            }
            $personal->user_id = $uid;
            $personal->name = $form->name;
            $personal->country = $form->country;
            $personal->surname = $form->surname;
            $personal->province_municipalitie = $form->province_municipalitie;
            $personal->city = $form->city;
            $personal->detailed_address = $form->detailed_address;
            if(!$personal->save()){
                throw new BusinessException([],Consts::PERSONAL_AUTHENTICATION_FAILED_ERROR);
            }
            return true;
        } else {
            throw new RequestParamsUnvalidatedException($form->getErrors());
        }
    }
    // 地区数据
    public static function getRegions()
    {
//        $json_string = file_get_contents(\Yii::$app->basePath.'/data/country.json');
//        return json_decode($json_string,true);
        $q = ApiCountry::find()->select('id,name,e_name,two,image');
        $zhCountry = $q->orderBy("CONVERT( name USING gbk ) COLLATE gbk_chinese_ci ASC")->asArray()->all();
        $enCountry = $q->orderBy('e_name asc')->asArray()->all();
        return [
            'zh' => $zhCountry,
            'en' => $enCountry
        ];
    }
}