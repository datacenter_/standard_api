<?php
namespace app\modules\analytics\services;

use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\RedisBase;

class MatchesService
{
    // 转小写
    public static function string_tolower($number_of_matches){
        return strtolower($number_of_matches);
    }
    public static function getTeamStatsDetail($params)
    {
        if(!isset($params['match_id']) && !$params['match_id']){
            return [];
        }
        if(!isset($params['number_of_matches']) || !$params['number_of_matches']){
            $params['number_of_matches'] = 10;
        }
        // 最后返回数据
        $resData = [
            'list' => [],
            'count' => 0
        ];
        $number_of_matches = self::string_tolower($params['number_of_matches']);
        $index_info = "stats-v2-common-team-{$number_of_matches}";
        // 检测索引
        $index = self::issetIndexKey($index_info);
        if(!$index){
            return $resData;
        }
        // es连接
        $EsService = EsService::initConnect();
        // 查看有没有这场比赛
        $matchInfo = $EsService->getFile('resource_matches_list',$params['match_id']);
        // matchid是否存在
        if ($matchInfo) {
            // 战队id数据
            $teamIdArray = $matchInfo['team_id'];
            // 比赛状态
            $matchStatus = $matchInfo['status'];
            // 比赛计划开始时间
            $matchScheduledBeginTime = strtotime($matchInfo['scheduled_begin_at']);
            $matchScheduledBeginAt   = date('Y-m-d\TH:i:s+08:00', $matchScheduledBeginTime);
        } else {
            return $resData;
        }

        if (in_array($matchStatus,['ongoing','upcoming'])) {
            // 未来数据

            foreach ($teamIdArray as $key=>$teamId) {
                // 初始化
                $info = [];
                $info['team'] = null;
                $info['total_number_of_matches'] = 0;
                $info['total_number_of_battles'] = 0;
                $info['number_of_matches'] = 0;
                $info['match_stats']['matches_count'] = 0;
                $info['match_stats']['matches_win_count'] = 0;
                $info['match_stats']['matches_win_rate'] = "0.00%";
                $info['battle_stats']['battles_count'] = 0;
                $info['battle_stats']['battles_win_count'] = 0;
                $info['battle_stats']['battles_win_rate'] = "0.00%";
                if ($teamId != -1) {
                    $teamJsonRes = RedisBase::valueGet([ApiConstant::PREFIX_API, 'Teams', $teamId]);
                    if ($teamJsonRes) {
                        $teamInfo = json_decode($teamJsonRes, true);
                        // unset不需要的字段
                        unset($teamInfo['region']);
                        unset($teamInfo['world_ranking']);
                        unset($teamInfo['total_earnings']);
                        unset($teamInfo['average_player_age']);
                        unset($teamInfo['history_players']);
                        unset($teamInfo['introduction']);
                        unset($teamInfo['introduction_cn']);
                        unset($teamInfo['players']);
                        $info['team'] = $teamInfo;
                    }
                }
                // 条件
                $conditions = [];
                // 筛选
                $dateTime = date('Y-m-d\TH:i:s+08:00', time());
                $conditions[] = ['field' => 'content.begin_time', 'type' => '<', 'value' => $dateTime];
                $conditions[] = ['field' => 'content.team.team_id', 'type' => 'filter', 'value' => $teamId];
                $order = ['field'=>'content.begin_time','sort'=>'desc'];
                $currentData = $EsService->search($index_info,$conditions,0,1,[],$order);
                if (!empty($currentData['list'])) {
                    $teamStatsInfo = $currentData['list'][0]['content'];
                    $info['total_number_of_matches'] = $teamStatsInfo['total_number_of_matches'];
                    $info['total_number_of_battles'] = $teamStatsInfo['total_number_of_battles'];
                    $info['number_of_matches'] = $teamStatsInfo['number_of_matches'];
                    $info['match_stats']['matches_count'] = $teamStatsInfo['match_stats']['matches_count'];
                    $info['match_stats']['matches_win_count'] = $teamStatsInfo['match_stats']['matches_win_count'];
                    $info['match_stats']['matches_win_rate'] = $teamStatsInfo['match_stats']['matches_win_rate'];
                    $info['battle_stats']['battles_count'] = $teamStatsInfo['battle_stats']['battles_count'];
                    $info['battle_stats']['battles_win_count'] = $teamStatsInfo['battle_stats']['battles_win_count'];
                    $info['battle_stats']['battles_win_rate'] = $teamStatsInfo['battle_stats']['battles_win_rate'];
                }
                $resData['list'][] = $info;
            }

            $resData['count'] = count($resData['list']);

        } else {
            // 筛选
            $conditions[] = ['field' => 'content.match_id', 'type' => '=', 'value' => $params['match_id']];
            $res = $EsService->search($index_info,$conditions);
            $teamsMatchData = $res['list'];
            if (!empty($teamsMatchData)) {

                // 绝对历史数据
                foreach ($teamsMatchData as $key => &$team) {
                    // 待处理的数据
                    $teamStatsInfo = $team['content'];
                    // 初始化
                    $info = [];
                    $info['team'] = null;
                    $info['total_number_of_matches'] = 0;
                    $info['total_number_of_battles'] = 0;
                    $info['number_of_matches'] = 0;
                    $info['match_stats']['matches_count'] = 0;
                    $info['match_stats']['matches_win_count'] = 0;
                    $info['match_stats']['matches_win_rate'] = "0.00%";
                    $info['battle_stats']['battles_count'] = 0;
                    $info['battle_stats']['battles_win_count'] = 0;
                    $info['battle_stats']['battles_win_rate'] = "0.00%";
                    // redis取出team结构
                    if ($teamStatsInfo['team']['team_id']) {
                        $teamJsonRes = RedisBase::valueGet([ApiConstant::PREFIX_API, 'Teams', $teamStatsInfo['team']['team_id']]);
                        if ($teamJsonRes) {
                            $teamInfo = json_decode($teamJsonRes, true);
                            // unset不需要的字段
                            unset($teamInfo['region']);
                            unset($teamInfo['world_ranking']);
                            unset($teamInfo['total_earnings']);
                            unset($teamInfo['average_player_age']);
                            unset($teamInfo['history_players']);
                            unset($teamInfo['introduction']);
                            unset($teamInfo['introduction_cn']);
                            unset($teamInfo['players']);
                            $info['team'] = $teamInfo;
                        }
                    }
                    // 查询结果数据
                    // 条件
                    $conditions = [];
                    // 筛选
                    $conditions[] = ['field' => 'content.begin_time', 'type' => '<', 'value' => $teamStatsInfo['begin_time']];
                    $conditions[] = ['field' => 'content.team.team_id', 'type' => 'filter', 'value' => $teamStatsInfo['team']['team_id']];
                    $order = ['field'=>'content.begin_time','sort'=>'desc'];
                    $currentData = $EsService->search($index_info,$conditions,0,1,[],$order);
                    if (!empty($currentData['list'])) {
                        // 结果数据
                        $currentTeamStatsInfo = $currentData['list'][0]['content'];
                        $info['total_number_of_matches'] = $currentTeamStatsInfo['total_number_of_matches'];
                        $info['total_number_of_battles'] = $currentTeamStatsInfo['total_number_of_battles'];
                        $info['number_of_matches'] = $currentTeamStatsInfo['number_of_matches'];
                        $info['match_stats']['matches_count'] = $currentTeamStatsInfo['match_stats']['matches_count'];
                        $info['match_stats']['matches_win_count'] = $currentTeamStatsInfo['match_stats']['matches_win_count'];
                        $info['match_stats']['matches_win_rate'] = $currentTeamStatsInfo['match_stats']['matches_win_rate'];
                        $info['battle_stats']['battles_count'] = $currentTeamStatsInfo['battle_stats']['battles_count'];
                        $info['battle_stats']['battles_win_count'] = $currentTeamStatsInfo['battle_stats']['battles_win_count'];
                        $info['battle_stats']['battles_win_rate'] = $currentTeamStatsInfo['battle_stats']['battles_win_rate'];
                    }
                    $resData['list'][] = $info;
                }

                $resData['count'] = count($resData['list']);

            } else {
                // es库没有这场统计
                foreach ($teamIdArray as $k=>$teamId) {
                    // 初始化
                    $info = [];
                    $info['team'] = null;
                    $info['total_number_of_matches'] = 0;
                    $info['total_number_of_battles'] = 0;
                    $info['number_of_matches'] = 0;
                    $info['match_stats']['matches_count'] = 0;
                    $info['match_stats']['matches_win_count'] = 0;
                    $info['match_stats']['matches_win_rate'] = "0.00%";
                    $info['battle_stats']['battles_count'] = 0;
                    $info['battle_stats']['battles_win_count'] = 0;
                    $info['battle_stats']['battles_win_rate'] = "0.00%";
                    if ($teamId != -1) {
                        $teamJsonRes = RedisBase::valueGet([ApiConstant::PREFIX_API, 'Teams', $teamId]);
                        if ($teamJsonRes) {
                            $teamInfo = json_decode($teamJsonRes, true);
                            // unset不需要的字段
                            unset($teamInfo['region']);
                            unset($teamInfo['world_ranking']);
                            unset($teamInfo['total_earnings']);
                            unset($teamInfo['average_player_age']);
                            unset($teamInfo['history_players']);
                            unset($teamInfo['introduction']);
                            unset($teamInfo['introduction_cn']);
                            unset($teamInfo['players']);
                            $info['team'] = $teamInfo;
                        }
                    }
                    // 条件
                    $conditions = [];
                    // 筛选
                    $conditions[] = ['field' => 'content.begin_time', 'type' => '<', 'value' => $matchScheduledBeginAt];
                    $conditions[] = ['field' => 'content.team.team_id', 'type' => 'filter', 'value' => $teamId];
                    $order = ['field'=>'content.begin_time','sort'=>'desc'];
                    $currentData = $EsService->search($index_info,$conditions,0,1,[],$order);
                    if (!empty($currentData['list'])) {
                        $teamStatsInfo = $currentData['list'][0]['content'];
                        $info['total_number_of_matches'] = $teamStatsInfo['total_number_of_matches'];
                        $info['total_number_of_battles'] = $teamStatsInfo['total_number_of_battles'];
                        $info['number_of_matches'] = $teamStatsInfo['number_of_matches'];
                        $info['match_stats']['matches_count'] = $teamStatsInfo['match_stats']['matches_count'];
                        $info['match_stats']['matches_win_count'] = $teamStatsInfo['match_stats']['matches_win_count'];
                        $info['match_stats']['matches_win_rate'] = $teamStatsInfo['match_stats']['matches_win_rate'];
                        $info['battle_stats']['battles_count'] = $teamStatsInfo['battle_stats']['battles_count'];
                        $info['battle_stats']['battles_win_count'] = $teamStatsInfo['battle_stats']['battles_win_count'];
                        $info['battle_stats']['battles_win_rate'] = $teamStatsInfo['battle_stats']['battles_win_rate'];
                    }
                    $resData['list'][] = $info;
                }
                $resData['count'] = count($resData['list']);
            }
        }

        return $resData;
    }

    public static function getMatchHistoryList($params)
    {
        if(!isset($params['match_id']) && !$params['match_id']){
            return [];
        }
        $indexName = "stats-v2-analytic-common-history-team";
        // 检测索引
        $index = self::issetIndexKey($indexName);
        if(!$index){
            return [
                'list' => [],
                'count' => 0
            ];
        }
        // 取出当前比赛信息
        $currentMatchJson = RedisBase::valueGet([ApiConstant::PREFIX_API,'Matchs',$params['match_id']]);
        // 战队信息
        $teams = [];
        // 处理当前比赛数据信息
        if ($currentMatchJson) {
            $currentMatchInfo = json_decode($currentMatchJson,true);
            // 比赛状态
            $matchStatus = $currentMatchInfo['status'];
            // 比赛时间
            $matchScheduledBeginAt = $currentMatchInfo['scheduled_begin_at'];
            $matchScheduledBeginTime = strtotime($matchScheduledBeginAt);
            // 战队数据
            $teamsData = $currentMatchInfo['teams'];
        } else {
            return [
                'list' => [],
                'count' => 0
            ];
        }
        // es连接
        $EsService = EsService::initConnect();
        // 条件
        $conditions = [];
        // 筛选
        $conditions[] = ['field' => "{$params['match_id']}.match_id", 'type' => '=', 'value' => $params['match_id']];
        // 搜索match_id搜索的结果数据
        $res = $EsService->search($indexName,$conditions,0,5);
        // 结果数据
        $matchHistoryArray = [];
        // 处理逻辑
        foreach ($teamsData as $k=>$teamCre) {
            $info = [
                'team' => null,
                'matches' => [],
            ];
            // 拼接team结构
            $info['team'] = $teamsData[$k];
            // 文档数据
            $esTeams = $EsService->getStrIdFile($indexName,$teamCre['team_id']);
            if ($esTeams) {
                unset($esTeams['dt']);
            }
            // 判断未来match
            if (in_array($matchStatus,['ongoing','upcoming'])) {
                // es文档数据是否存在
                if ($esTeams) {
                    // 存在的情况

                    // 根据时间排序
                    $field = array_column($esTeams, 'begin_at');
                    array_multisort($field, SORT_DESC, $esTeams);
                    // 取出前10场结果数据
                    $currentMatchIds = array_slice($esTeams,0,10);
                    // 拼接matchs结构
                    foreach ($currentMatchIds as &$currentMatchIdInfo) {
                        $matchJson = RedisBase::valueGet([ApiConstant::PREFIX_API,'Matchs',$currentMatchIdInfo['match_id']]);
                        if ($matchJson) {
                            $matchInfo = json_decode($matchJson, true);
                            unset($matchInfo['pbpdata']);
                            unset($matchInfo['streams']);
                            unset($matchInfo['team_version']);
                            $info['matches'][] = $matchInfo;
                        }
                    }
                    // 根据计划开始时间排序
                    $field = array_column($info['matches'],'scheduled_begin_at');
                    array_multisort($field, SORT_DESC, $info['matches']);
                } else {
                    //  返回之后teams数据 matchs数据为空
                }
            } else {
                if ($esTeams) {
                    // 绝对历史数据
                    $matchIds = $esTeams;
                    // 根据时间排序
                    $field = array_column($matchIds, 'begin_at');
                    array_multisort($field, SORT_DESC, $matchIds);
                    // 处理当前matchid之前的数据保留，之后的数据不要
                    $newMatchIds = [];
                    if (!empty($res['list'])) {
                        // 数据结束标识
                        $isYo = false;
                        foreach ($matchIds as $nk => &$nMatchIdInfo) {
                            if ($nMatchIdInfo['match_id'] == $params['match_id']) {
                                $isYo = true;
                                continue;
                            }
                            if ($isYo) {
                                if (count($newMatchIds) < 10) {
                                    $newMatchIds[] = $nMatchIdInfo;
                                } else {
                                    break;
                                }
                            }
                        }
                        if (empty($newMatchIds)) {
                            continue;
                        }
                        // 拼接matchs数据
                        foreach ($newMatchIds as &$currentMatchIdInfo) {
                            $matchJson = RedisBase::valueGet([ApiConstant::PREFIX_API, 'Matchs', $currentMatchIdInfo['match_id']]);
                            if ($matchJson) {
                                $matchInfo = json_decode($matchJson, true);
                                unset($matchInfo['pbpdata']);
                                unset($matchInfo['streams']);
                                unset($matchInfo['team_version']);
                                $info['matches'][] = $matchInfo;
                            }
                        }
                        // 根据计划开始时间排序
                        $field = array_column($info['matches'], 'scheduled_begin_at');
                        array_multisort($field, SORT_DESC, $info['matches']);
                    } else {
                        // es统计结果遗漏情况
                        foreach ($matchIds as $nk => &$nMatchIdInfo) {
                            if (count($newMatchIds) < 10) {
                                if ($matchScheduledBeginTime > strtotime($nMatchIdInfo['begin_at'])) {
                                    $newMatchIds[] = $nMatchIdInfo;
                                }
                            } else {
                                break;
                            }
                        }
                        // 拼接matchs数据
                        foreach ($newMatchIds as &$currentMatchIdInfo) {
                            $matchJson = RedisBase::valueGet([ApiConstant::PREFIX_API, 'Matchs', $currentMatchIdInfo['match_id']]);
                            if ($matchJson) {
                                $matchInfo = json_decode($matchJson, true);
                                unset($matchInfo['pbpdata']);
                                unset($matchInfo['streams']);
                                unset($matchInfo['team_version']);
                                $info['matches'][] = $matchInfo;
                            }
                        }
                    }
                } else {
                    // 如出现此处情况，就是matchs没数据
                }
            }
            // 结果数据
            $matchHistoryArray[$k] = $info;
        }
        return [
            'list' => $matchHistoryArray,
            'count' => count($matchHistoryArray)
        ];
    }

    public static function getMatchHeadToHeadList($params)
    {
        if(!isset($params['match_id']) && !$params['match_id']){
            return [];
        }
        $indexName = "stats-v2-analytic-common-head-team";
        // 检测索引
        $index = self::issetIndexKey($indexName);
        if(!$index){
            return [
                'list' => []
            ];
        }
        // 取出当前比赛信息
        $currentMatchJson = RedisBase::valueGet([ApiConstant::PREFIX_API,'Matchs',$params['match_id']]);
        // 战队信息
        $teams = [];
        // 处理当前比赛数据信息,把当前比赛team信息摘出来
        if ($currentMatchJson) {
            $currentMatchInfo = json_decode($currentMatchJson,true);
            // 战队数据
            $teamsData = $currentMatchInfo['teams'];
            // 比赛状态
            $matchStatus = $currentMatchInfo['status'];
            // 比赛时间
            $matchScheduledBeginAt = $currentMatchInfo['scheduled_begin_at'];
            $matchScheduledBeginTime = strtotime($matchScheduledBeginAt);
            foreach ($teamsData as $teamInfo) {
                $teamInfo['win_count'] = 0;
                $teamInfo['draw_count'] = 0;
                $teamInfo['lose_count'] = 0;
                $teams[$teamInfo['team_id']] = $teamInfo;
            }
        } else {
            return [
                'list' => []
            ];
        }
        // es连接
        $EsService = EsService::initConnect();
        // 条件
        $conditions = [];
        // 筛选
        $conditions[] = ['field' => "{$params['match_id']}.match_id", 'type' => '=', 'value' => $params['match_id']];
        $res = $EsService->search($indexName,$conditions,0,5);
        // 结果数据
        $headToHeadArray = [
            'teams' => [],
            'matches' => [],
        ];
        // 拼接teamid作为docid
        if ($teamsData[0]['team_id'] > $teamsData[1]['team_id']) {
            $docId = "{$teamsData[0]['team_id']}_{$teamsData[1]['team_id']}";
        } else {
            $docId = "{$teamsData[1]['team_id']}_{$teamsData[0]['team_id']}";
        }
        // 文档数据
        $esTeams = $EsService->getStrIdFile($indexName,$docId);
        if ($esTeams) {
            // 根据时间排序
            $field = array_column($esTeams, 'begin_at');
            array_multisort($field, SORT_DESC, $esTeams);

            unset($esTeams['dt']);
        }
        // 判断是否是未来match
        if (in_array($matchStatus,['ongoing','upcoming'])) {
            // 未来数据
            if (!empty($esTeams)) {
                $esTeams = array_values($esTeams);
                // 统计数倒序第一条
                foreach ($esTeams as $k=>&$matchEsInfo) {
                    if ($params['match_id'] == $matchEsInfo['match_id']) {
                        break;
                    } else {
                        $matchRedisJson = RedisBase::valueGet([ApiConstant::PREFIX_API,'Matchs',$matchEsInfo['match_id']]);
                        if ($matchRedisJson) {
                            $matchInfo = json_decode($matchRedisJson,true);
                            unset($matchInfo['pbpdata']);
                            unset($matchInfo['streams']);
                            unset($matchInfo['team_version']);
                            $headToHeadArray['matches'][] = $matchInfo;
                        }
                    }
                    if ($k == 0) {
                        $team_b_win_count = $matchEsInfo['cntAll'] - $matchEsInfo['teamAwincnt']-$matchEsInfo['cntDraw'];
                        if (isset($teams[$matchEsInfo['teamA']])) {
                            $teams[$matchEsInfo['teamA']]['win_count'] = $matchEsInfo['teamAwincnt'];
                            $teams[$matchEsInfo['teamA']]['draw_count'] = $matchEsInfo['cntDraw'];
                            $teams[$matchEsInfo['teamA']]['lose_count'] = $team_b_win_count;
                        }
                        if (isset($teams[$matchEsInfo['teamB']])) {
                            $teams[$matchEsInfo['teamB']]['win_count'] = $team_b_win_count;
                            $teams[$matchEsInfo['teamB']]['draw_count'] = $matchEsInfo['cntDraw'];
                            $teams[$matchEsInfo['teamB']]['lose_count'] = $matchEsInfo['teamAwincnt'];
                        }
                        $headToHeadArray['teams'] = array_values($teams);
                    }
                }
            } else {
                // 不做处理 走默认
                $headToHeadArray['teams'] = array_values($teams);
            }
        } else {
            // 绝对历史数据

            if (!empty($esTeams)) {
                if (!empty($res['list'])) {
                    foreach ($res['list'] as $key => &$matchEsArray) {
                        unset($matchEsArray['dt']);
                        // 根据时间排序
                        $field = array_column($matchEsArray, 'begin_at');
                        array_multisort($field, SORT_ASC, $matchEsArray);
                        // 上一场比赛id
                        $esMatchid = null;
                        // 输出这场比赛的上一场统计
                        foreach ($matchEsArray as $matchId => &$matchEsInfo) {
                            if ($params['match_id'] == $matchEsInfo['match_id']) {
                                if (isset($matchEsArray[$esMatchid])) {
                                    $matchEsData = $matchEsArray[$esMatchid];
                                    $team_b_win_count = $matchEsData['cntAll'] - $matchEsData['teamAwincnt'] - $matchEsData['cntDraw'];
                                    if (isset($teams[$matchEsData['teamA']])) {
                                        $teams[$matchEsData['teamA']]['win_count'] = $matchEsData['teamAwincnt'];
                                        $teams[$matchEsData['teamA']]['draw_count'] = $matchEsData['cntDraw'];
                                        $teams[$matchEsData['teamA']]['lose_count'] = $team_b_win_count;
                                    }
                                    if (isset($teams[$matchEsData['teamB']])) {
                                        $teams[$matchEsData['teamB']]['win_count'] = $team_b_win_count;
                                        $teams[$matchEsData['teamB']]['draw_count'] = $matchEsData['cntDraw'];
                                        $teams[$matchEsData['teamB']]['lose_count'] = $matchEsData['teamAwincnt'];
                                    }
                                    $headToHeadArray['teams'] = array_values($teams);
                                }
                                break;
                            } else {
                                // 记录上一场比赛id
                                $esMatchid = $matchId;
                                // 比赛数据
                                $matchRedisJson = RedisBase::valueGet([ApiConstant::PREFIX_API, 'Matchs', $matchEsInfo['match_id']]);
                                if ($matchRedisJson) {
                                    $matchInfo = json_decode($matchRedisJson, true);
                                    unset($matchInfo['pbpdata']);
                                    unset($matchInfo['streams']);
                                    unset($matchInfo['team_version']);
                                    $headToHeadArray['matches'][] = $matchInfo;
                                }
                            }
                        }
                        if (empty($headToHeadArray['teams'])) {
                            $headToHeadArray['teams'] = array_merge($teams);
                        }
                    }
                } else {
                    // es - matchid 不存在
                    $isYo = false;
                    // 拼接match数据
                    foreach ($esTeams as $matchId=>&$matchEsInfo) {
                        // 时间对比
                        if ($matchScheduledBeginTime > strtotime($matchEsInfo['begin_at'])) {
                            if (!$isYo) {
                                $isYo = true;
                                if (isset($esTeams[$matchId])) {
                                    $matchEsData = $esTeams[$matchId];
                                    $team_b_win_count = $matchEsData['cntAll'] - $matchEsData['teamAwincnt'] - $matchEsData['cntDraw'];
                                    if (isset($teams[$matchEsData['teamA']])) {
                                        $teams[$matchEsData['teamA']]['win_count'] = $matchEsData['teamAwincnt'];
                                        $teams[$matchEsData['teamA']]['draw_count'] = $matchEsData['cntDraw'];
                                        $teams[$matchEsData['teamA']]['lose_count'] = $team_b_win_count;
                                    }
                                    if (isset($teams[$matchEsData['teamB']])) {
                                        $teams[$matchEsData['teamB']]['win_count'] = $team_b_win_count;
                                        $teams[$matchEsData['teamB']]['draw_count'] = $matchEsData['cntDraw'];
                                        $teams[$matchEsData['teamB']]['lose_count'] = $matchEsData['teamAwincnt'];
                                    }
                                    $headToHeadArray['teams'] = array_values($teams);
                                }
                            }
                            // 比赛数据
                            $matchRedisJson = RedisBase::valueGet([ApiConstant::PREFIX_API, 'Matchs', $matchEsInfo['match_id']]);
                            if ($matchRedisJson) {
                                $matchInfo = json_decode($matchRedisJson, true);
                                unset($matchInfo['pbpdata']);
                                unset($matchInfo['streams']);
                                unset($matchInfo['team_version']);
                                $headToHeadArray['matches'][] = $matchInfo;
                            }
                        }
                    }
                }
            } else {
                // doc数据不存在
                // 不做处理 走默认
                $headToHeadArray['teams'] = array_values($teams);
            }
        }

        return [
            'list' => $headToHeadArray
        ];
    }

    // 检测索引
    public static function issetIndexKey($index_info)
    {
        $client = EsService::initConnect();
        return $client->searchIndex($index_info);
    }
}
