<?php
namespace app\modules\analytics\controllers;

use app\controllers\RestController;
use app\modules\analytics\services\MatchesService;

class MatchesController extends RestController
{
    // 战队统计数据详情
    public function actionTeams_stats()
    {
        $params = $this->pGet();
        $info = MatchesService::getTeamStatsDetail($params);
        return $info;
    }
    // 战队近期战绩列表
    public function actionMatch_history()
    {
        $params = $this->pGet();
        $info = MatchesService::getMatchHistoryList($params);
        return $info;
    }
    // 战队历史交锋列表
    public function actionHead_to_head()
    {
        $params = $this->pGet();
        $info = MatchesService::getMatchHeadToHeadList($params);
        return $info;
    }
}
