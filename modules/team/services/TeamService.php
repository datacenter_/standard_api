<?php

namespace app\modules\team\services;

use app\modules\common\services\ApiBase;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\RedisBase;

class TeamService
{
    public static function getTeamList($params) {
        // game_id筛选 根据用户游戏套餐进行处理
        if (ApiBase::getSetMealTypeId()) {
            if (isset($params['filter']['game_id'])) {
                // 根据版本组验证套餐游戏
                $params['filter']['game_id'] = ApiBase::groupToSetUpGameId($params['filter']['game_id']);
            } else {
                // 没有筛选game_id（走默认）
                $params['filter']['game_id'] = implode(ApiBase::getAuthGameIds(),',');
            }
        }
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_team_list';
        $conditions = [];
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'team_id', 'sort' => 'asc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Teams',$data['team_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getTeamDetail($teamId){
        if(!$teamId){
            return [];
        }
        // 验证套餐组
        if(!empty(\Yii::$app->params['group'])) {
            $EsService = EsService::initConnect();
            $teamInfo = $EsService->getFile('resource_team_list',$teamId);
            if($teamInfo){
                // 验证游戏权限
                ApiBase::verifyTheGame($teamInfo['game_id']);
            }
        }
        // redis取值
        $teamJsonRes = RedisBase::valueGet([ApiConstant::PREFIX_API,'Teams',$teamId]);
        $teamInfo = [];
        if($teamJsonRes){
            $teamInfo = json_decode($teamJsonRes,true);
        }
        return $teamInfo;
    }
}