<?php

namespace app\modules\team\controllers;

use app\controllers\RestController;
use app\modules\team\services\TeamService;

class TeamController extends RestController
{

    public function actionGet(){
        $params = $this->pGet();
        $info = TeamService::getTeamList($params);
        return $info;
    }

    public function actionDetail(){
        $team_id = $this->pGet('team_id');
        $info = TeamService::getTeamDetail($team_id);
        return $info;
    }
}
