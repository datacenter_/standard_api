<?php

namespace app\modules\team\models;

use Yii;

/**
 * This is the model class for table "team_snapshot".
 *
 * @property int $id
 * @property int|null $team_id
 * @property int|null $relation_id
 * @property string|null $type 战队类型
 * @property string|null $name 战队名称
 * @property int|null $organization 俱乐部id
 * @property int|null $game 游戏id
 * @property int|null $country 战队所属地
 * @property string|null $full_name 全称
 * @property string|null $short_name 简称
 * @property string|null $alias 别名
 * @property string|null $slug
 * @property string|null $image logo
 * @property int $cuser
 * @property int $deleted
 * @property string $created_at
 * @property string $modified_at
 * @property string|null $deleted_at
 */
class TeamSnapshot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_snapshot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'relation_id', 'organization', 'game', 'country', 'cuser', 'deleted'], 'integer'],
            [['cuser'], 'required'],
            [['created_at', 'modified_at', 'deleted_at'], 'safe'],
            [['type'], 'string', 'max' => 20],
            [['name', 'full_name', 'short_name'], 'string', 'max' => 100],
            [['alias'], 'string', 'max' => 180],
            [['slug'], 'string', 'max' => 255],
            [['image'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => 'Team ID',
            'relation_id' => 'Relation ID',
            'type' => 'Type',
            'name' => 'Name',
            'organization' => 'Organization',
            'game' => 'Game',
            'country' => 'Country',
            'full_name' => 'Full Name',
            'short_name' => 'Short Name',
            'alias' => 'Alias',
            'slug' => 'Slug',
            'image' => 'Image',
            'cuser' => 'Cuser',
            'deleted' => 'Deleted',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
