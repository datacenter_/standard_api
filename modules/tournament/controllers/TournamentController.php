<?php

namespace app\modules\tournament\controllers;

use app\controllers\RestController;
use app\modules\tournament\services\TournamentService;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use Elasticsearch\ClientBuilder;

class TournamentController extends RestController
{

    public function actionGet(){
        $params = $this->pGet();
        $info = TournamentService::getTournamentList($params);
        return $info;
    }
    public function actionUpcoming(){
        $params = $this->pGet();
        $status = 'upcoming';
        $info = TournamentService::getTournamentList($params,$status);
        return $info;
    }
    public function actionOngoing(){
        $params = $this->pGet();
        $status = 'ongoing';
        $info = TournamentService::getTournamentList($params,$status);
        return $info;
    }
    public function actionPast(){
        $params = $this->pGet();
        $status = 'completed';
        $info = TournamentService::getTournamentList($params,$status);
        return $info;
    }
    public function actionDetail(){
        $tournament_id = $this->pGet('tournament_id');
        $info = TournamentService::getTournamentDetail($tournament_id);
        return $info;
    }

    // 5e 赛事统计数据
    public function actionCsgoDetail(){
        $params = $this->pGet();
        $info = TournamentService::getTournamentCsgoDetail($params);
        return $info;
    }
    // 5e 赛事下所有选手统计数据
    public function actionCsgoPlayersDetail(){
        $params = $this->pGet();
        $info = TournamentService::getTournamentCsgoPlayersDetail($params);
        return $info;
    }
}