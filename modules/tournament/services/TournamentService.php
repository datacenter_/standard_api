<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/31
 * Time: 22:00
 */
namespace app\modules\tournament\services;

use app\modules\common\services\ApiBase;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\RedisBase;

class TournamentService
{
    public static function getTournamentList($params,$status=null)
    {
        // game_id筛选 根据用户游戏套餐进行处理
        if (ApiBase::getSetMealTypeId()) {
            if (isset($params['filter']['game_id'])) {
                // 根据版本组验证套餐游戏
                $params['filter']['game_id'] = ApiBase::groupToSetUpGameId($params['filter']['game_id']);
            } else {
                // 没有筛选game_id（走默认）
                $params['filter']['game_id'] = implode(ApiBase::getAuthGameIds(),',');
            }
        }
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_tournament_list';
        // 是否为所有数据列表
        if ($status){
            $conditions = [
                ['field' => 'status', 'type' => '=', 'value' => $status]
            ];
        }else{
            $conditions = [];
        }
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'tournament_id', 'sort' => 'desc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Tournaments',$data['tournament_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getTournamentDetail($tournament_id)
    {
        if(!$tournament_id){
            return [];
        }
        // 验证套餐组
        if(!empty(\Yii::$app->params['group'])) {
            $EsService = EsService::initConnect();
            $tournamentInfo = $EsService->getFile('resource_tournament_list',$tournament_id);
            if($tournamentInfo){
                // 验证游戏权限
                ApiBase::verifyTheGame($tournamentInfo['game_id']);
            }
        }
        // redis取值
        $jsonRes = RedisBase::valueGet([ApiConstant::PREFIX_API,'Tournaments',$tournament_id]);
        $tournament = [];
        if($jsonRes){
            $tournament = json_decode($jsonRes,true);
        }
        return $tournament;
    }


    // 5e赛事统计
    public static function getTournamentCsgoDetail($params)
    {
        // 输出格式
        if(isset($params['format'])){
            ApiBase::getFormatType($params['format']);
        }
        $tournament_id = isset($params['tournament_id']) ? $params['tournament_id']:null;
        if(!$tournament_id){
            return [];
        }
        // redis取值
        $jsonRes = RedisBase::valueGet([ApiConstant::PREFIX_API,'Tournaments','5E',$tournament_id]);
        $tournament = [];
        if($jsonRes){
            $tournament = json_decode($jsonRes,true);
        }
        return $tournament;
    }

    // 5e赛事players统计
    public static function getTournamentCsgoPlayersDetail($params)
    {
        // 输出格式
        if(isset($params['format'])){
            ApiBase::getFormatType($params['format']);
        }
        $tournament_id = isset($params['tournament_id']) ? $params['tournament_id']:null;
        if(!$tournament_id){
            return [];
        }
        // redis取值
        $jsonRes = RedisBase::valueGet([ApiConstant::PREFIX_API,'Tournaments','5E',$tournament_id]);
        $tournamentPlayers = [];
        if($jsonRes){
            $tournament = json_decode($jsonRes,true);
            $players = $tournament['players'];
            foreach ($players as $key=>$player){
                $playerCre['player_id'] = $player['player']['player_id'];
                $playerCre['nick_name'] = $player['player']['nick_name'];
                unset($player['player']);
                unset($player['adr_total']);
                unset($player['kast_total']);
                unset($player['rating_total']);
                unset($player['total_rounds']);
                $tournamentPlayers[] = array_merge($playerCre,$player);
            }
        }
        return $tournamentPlayers;
    }
}