<?php

namespace app\modules\tournament\models;

use Yii;

/**
 * This is the model class for table "tournament_group".
 *
 * @property int $id
 * @property int|null $tournament_id 赛事id
 * @property int|null $tree_id
 * @property string|null $name 分组名称
 * @property string|null $name_cn 分组名称英文
 * @property int|null $game 游戏id
 * @property string|null $scheduled_begin_at 计划开始时间	
 * @property string|null $scheduled_end_at 计划结束时间	
 * @property string|null $number_of_teams 队伍数量	
 * @property string|null $advance 晋级队伍数量	
 * @property string|null $eliminate 淘汰队伍数量	
 * @property string|null $win_points 获胜积分	
 * @property string|null $draw_ponits 平局积分	
 * @property string|null $lose_points 失败积分	
 * @property int|null $order 排序
 * @property int|null $deleted
 * @property string|null $modified_at
 * @property string|null $created_at
 * @property string|null $deleted_at
 */
class TournamentGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tournament_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tournament_id', 'tree_id', 'game', 'order', 'deleted'], 'integer'],
            [['scheduled_begin_at', 'scheduled_end_at', 'modified_at', 'created_at', 'deleted_at'], 'safe'],
            [['name', 'name_cn'], 'string', 'max' => 50],
            [['number_of_teams', 'advance', 'eliminate', 'win_points', 'draw_ponits', 'lose_points'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tournament_id' => 'Tournament ID',
            'tree_id' => 'Tree ID',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'game' => 'Game',
            'scheduled_begin_at' => 'Scheduled Begin At',
            'scheduled_end_at' => 'Scheduled End At',
            'number_of_teams' => 'Number Of Teams',
            'advance' => 'Advance',
            'eliminate' => 'Eliminate',
            'win_points' => 'Win Points',
            'draw_ponits' => 'Draw Ponits',
            'lose_points' => 'Lose Points',
            'order' => 'Order',
            'deleted' => 'Deleted',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
