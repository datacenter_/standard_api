<?php

namespace app\modules\tournament\models;

use Yii;

/**
 * This is the model class for table "tournament".
 *
 * @property int $id
 * @property string $name 赛事名称英文
 * @property string|null $name_cn 赛事名称中文
 * @property int|null $game 游戏id
 * @property int|null $status
 * @property string|null $scheduled_begin_at 计划开始日期	
 * @property string|null $scheduled_end_at 计划结束日期	
 * @property string|null $slug
 * @property string|null $short_name 赛事简称
 * @property string|null $short_name_cn 赛事简称中文
 * @property string|null $son_match_sort 子赛事排序
 * @property string|null $image
 * @property int|null $type 1-赛事-2子赛事3-目录
 * @property int|null $tree_id
 * @property int|null $cuser
 * @property string|null $match_condition 参赛条件
 * @property int|null $deleted 1表示删除，2表示正常
 * @property string $created_at
 * @property string $modified_at
 * @property string|null $deleted_at
 */
class Tournament extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tournament';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['game', 'status', 'type', 'tree_id', 'cuser', 'deleted'], 'integer'],
            [['scheduled_begin_at', 'scheduled_end_at', 'created_at', 'modified_at', 'deleted_at'], 'safe'],
            [['match_condition'], 'string'],
            [['name', 'name_cn', 'slug', 'short_name', 'short_name_cn', 'son_match_sort'], 'string', 'max' => 255],
            [['image'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_cn' => 'Name Cn',
            'game' => 'Game',
            'status' => 'Status',
            'scheduled_begin_at' => 'Scheduled Begin At',
            'scheduled_end_at' => 'Scheduled End At',
            'slug' => 'Slug',
            'short_name' => 'Short Name',
            'short_name_cn' => 'Short Name Cn',
            'son_match_sort' => 'Son Match Sort',
            'image' => 'Image',
            'type' => 'Type',
            'tree_id' => 'Tree ID',
            'cuser' => 'Cuser',
            'match_condition' => 'Match Condition',
            'deleted' => 'Deleted',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
