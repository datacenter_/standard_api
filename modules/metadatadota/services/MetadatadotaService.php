<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/31
 * Time: 22:00
 */
namespace app\modules\metadatadota\services;

use app\modules\common\services\ApiBase;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\RedisBase;

class MetadatadotaService
{
    public static function getHeroList($params)
    {
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_hero_list';
        $conditions = [];
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'hero_id', 'sort' => 'asc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Dota','hero',$data['hero_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getHeroDetail($heroId)
    {
        if(!$heroId){
            return [];
        }
        // redis取值
        $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Dota','hero',$heroId]);
        $info = [];
        if($json){
            $info = json_decode($json,true);
        }
        return $info;
    }

    public static function getItemList($params)
    {
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_dotaitem_list';
        $conditions = [];
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'item_id', 'sort' => 'asc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Dota','item',$data['item_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getItemDetail($itemId)
    {
        if(!$itemId){
            return [];
        }
        // redis取值
        $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Metadata','Dota','item',$itemId]);
        $info = [];
        if($json){
            $info = json_decode($json,true);
        }
        return $info;
    }
}