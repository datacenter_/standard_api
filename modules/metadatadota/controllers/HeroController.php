<?php

namespace app\modules\metadatadota\controllers;

use app\controllers\RestController;
use app\modules\common\controllers\CommonController;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use app\modules\metadatadota\services\MetadatadotaService;
use Elasticsearch\ClientBuilder;

class HeroController extends RestController
{
    public function actionGet(){
        $params = $this->pGet();
        $info = MetadatadotaService::getHeroList($params);
        return $info;
    }

    public function actionDetail(){
        $hero_id = $this->pGet('hero_id');
        $info = MetadatadotaService::getHeroDetail($hero_id);
        return $info;
    }
}
