<?php

namespace app\modules\metadatadota\controllers;

use app\controllers\RestController;
use app\modules\common\controllers\CommonController;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use app\modules\metadatadota\services\MetadatadotaService;
use Elasticsearch\ClientBuilder;

class ItemController extends RestController
{
    public function actionGet(){
        $params = $this->pGet();
        $info = MetadatadotaService::getItemList($params);
        return $info;
    }

    public function actionDetail(){
        $item_id = $this->pGet('item_id');
        $info = MetadatadotaService::getItemDetail($item_id);
        return $info;
    }
}
