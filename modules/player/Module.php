<?php

namespace app\modules\player;

/**
 * admin module definition class
 * @property \yii\web\user user
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\player\controllers';
}
