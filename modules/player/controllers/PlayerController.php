<?php

namespace app\modules\player\controllers;

use app\controllers\RestController;
use app\modules\player\services\PlayerService;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use Elasticsearch\ClientBuilder;

class PlayerController extends RestController
{

    public function actionGet(){
        $params = $this->pGet();
        $info = PlayerService::getPlayerList($params);
        return $info;
    }

    public function actionDetail(){
        $player_id = $this->pGet('player_id');
        $info = PlayerService::getPlayerDetail($player_id);
        return $info;
    }
}
