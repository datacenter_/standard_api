<?php
/**
 * Created by PhpStorm.
 * User: GaoYu
 * Date: 2020/10/31
 * Time: 22:00
 */
namespace app\modules\player\services;

use app\modules\common\services\ApiBase;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\RedisBase;

class PlayerService
{

    public static function getPlayerList($params)
    {
        // game_id筛选 根据用户游戏套餐进行处理
        if (ApiBase::getSetMealTypeId()) {
            if (isset($params['filter']['game_id'])) {
                // 根据版本组验证套餐游戏
                $params['filter']['game_id'] = ApiBase::groupToSetUpGameId($params['filter']['game_id']);
            } else {
                // 没有筛选game_id（走默认）
                $params['filter']['game_id'] = implode(ApiBase::getAuthGameIds(),',');
            }
        }
        $params = ApiBase::issetParameterFilter($params);
        $index_info = 'resource_player_list';
        $conditions = [];
        // 筛选
        if (isset($params['conditions'])){
            $conditions = array_merge($conditions,$params['conditions']);
        }
        // 排序
        if (isset($params['sort']) && !empty($params['sort'])){
            $order = $params['sort'];
        }else{
            $order = array('field' => 'player_id', 'sort' => 'asc');
        }
        $offset  = ($params['page'] > 0) ? ($params['page'] - 1) * $params['per_page'] : 0;
        $columns = [];//['match_id', 'description'];
        $EsService = EsService::initConnect();
        $res = $EsService->search($index_info,$conditions, $offset, $params['per_page'], $columns, $order);
        $info['list'] = [];
        $info['count'] = $res['count'];
        if(!empty($res['list'])) {
            foreach ($res['list'] as $key => $data) {
                // redis取值
                $json = RedisBase::valueGet([ApiConstant::PREFIX_API,'Players',$data['player_id']]);
                if ($json) {
                    $info['list'][] = json_decode($json, true);
                }
            }
        }
        return $info;
    }

    public static function getPlayerDetail($playerId)
    {
        if(!$playerId){
            return [];
        }
        // 验证套餐组
        if(!empty(\Yii::$app->params['group'])) {
            $EsService = EsService::initConnect();
            $playerInfo = $EsService->getFile('resource_player_list',$playerId);
            if($playerInfo){
                // 验证游戏权限
                ApiBase::verifyTheGame($playerInfo['game_id']);
            }
        }
        // redis取值
        $playerJsonRes = RedisBase::valueGet([ApiConstant::PREFIX_API,'Players',$playerId]);
        $playerInfo = [];
        if($playerJsonRes){
            $playerInfo = json_decode($playerJsonRes,true);
        }
        return $playerInfo;
    }
}