<?php

namespace app\modules\v1;

/**
 * v1 module definition class
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();

        $this->modules = [
            'match' => [
                'class' => \app\modules\match\Module::class,
            ],
            'battles' => [
                'class' => \app\modules\battles\Module::class,
            ],
            'game' => [
                'class' => \app\modules\game\Module::class,
            ],
            'organization' => [
                'class' => \app\modules\organization\Module::class,
            ],
            'team' => [
                'class' => \app\modules\team\Module::class,
            ],
            'player' => [
                'class' => \app\modules\player\Module::class,
            ],
            'metadatalol' => [
                'class' => \app\modules\metadatalol\Module::class,
            ],
            'metadatacsgo' => [
                'class' => \app\modules\metadatacsgo\Module::class,
            ],
            'metadatadota' => [
                'class' => \app\modules\metadatadota\Module::class,
            ],
            'tournament' => [
                'class' => \app\modules\tournament\Module::class,
            ],
            'statsteam' => [
                'class' => \app\modules\statsteam\Module::class,
            ],
            'statslol' => [
                'class' => \app\modules\statslol\Module::class,
            ],
            'statscsgo' => [
                'class' => \app\modules\statscsgo\Module::class,
            ],
            'analytics' => [
                'class' => \app\modules\analytics\Module::class,
            ],
            'task' => [
                'class' => \app\modules\task\Module::class,
            ],
        ];
    }
}
