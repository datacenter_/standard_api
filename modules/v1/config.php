<?php
return [
    'components' => [
        'urlManager' => [
            'rules' => [
                [
                    'class' => 'yii\web\GroupUrlRule',
                    'prefix' => 'v1',
                    'rules' => [
                        'matches/list' => 'match/match/get',
                        'matches/upcoming' => 'match/upcoming/get',
                        'matches/ongoing' => 'match/ongoing/get',
                        'matches/past' => 'match/past/get',
                        'matches/detail' => 'match/match/detail',

                        'tournaments/list' => 'tournament/tournament/get',
                        'tournaments/upcoming' => 'tournament/tournament/upcoming',
                        'tournaments/ongoing' => 'tournament/tournament/ongoing',
                        'tournaments/past' => 'tournament/tournament/past',
                        'tournaments/detail' => 'tournament/tournament/detail',

                        'battles/list' => 'battles/battle/get',
                        'battles/detail' => 'battles/battle/detail',
                        'battles/events' => 'battles/battle/events',
                        'battles/abilities' => 'battles/battle/abilities',
                        'battles/items' => 'battles/battle/items',

                        '5e/battles/list' => 'battles/battle/csgo-get',
                        '5e/battles/players/list' => 'battles/battle/csgo-players-detail',
//                        '5e/battles/detail' => 'battles/battle/csgo-detail',
                        '5e/tournaments/detail' => 'tournament/tournament/csgo-detail',
                        '5e/tournaments/players/list' => 'tournament/tournament/csgo-players-detail',

                        'games/list' => 'game/game/get',
                        'games/detail' => 'game/game/detail',

                        'players/list' => 'player/player/get',
                        'players/detail' => 'player/player/detail',

                        'teams/list' => 'team/team/get',
                        'teams/detail' => 'team/team/detail',

                        'organizations/list' => 'organization/organization/get',
                        'organizations/detail' => 'organization/organization/detail',

                        'metadata/lol/champions/list' => 'metadatalol/champion/get',
                        'metadata/lol/champions/detail' => 'metadatalol/champion/detail',
                        'metadata/lol/items/list' => 'metadatalol/item/get',
                        'metadata/lol/items/detail' => 'metadatalol/item/detail',
                        'metadata/lol/summonerspells/list' => 'metadatalol/summonerspell/get',
                        'metadata/lol/summonerspells/detail' => 'metadatalol/summonerspell/detail',
                        'metadata/lol/runes/list' => 'metadatalol/rune/get',
                        'metadata/lol/runes/detail' => 'metadatalol/rune/detail',

                        'metadata/dota/heroes/list' => 'metadatadota/hero/get',
                        'metadata/dota/heroes/detail' => 'metadatadota/hero/detail',
                        'metadata/dota/items/list' => 'metadatadota/item/get',
                        'metadata/dota/items/detail' => 'metadatadota/item/detail',

                        'metadata/csgo/maps/list' => 'metadatacsgo/map/get',
                        'metadata/csgo/maps/detail' => 'metadatacsgo/map/detail',
                        'metadata/csgo/weapons/list' => 'metadatacsgo/weapon/get',
                        'metadata/csgo/weapons/detail' => 'metadatacsgo/weapon/detail',

                        // 战队统计数据详情
                        'stats/teams/detail' => 'statsteam/teams/detail',
                        // lol计算统计
                        'stats/lol/teams/detail' => 'statslol/statslol/team-lol-detail',
                        'stats/lol/teams/most_banned_champions' => 'statslol/statslol/team-most-banned-champions',
                        'stats/lol/teams/most_picked_champions' => 'statslol/statslol/team-most-picked-champions',
                        'stats/lol/players/detail' => 'statslol/statslol/player-detail',
                        'stats/lol/players/most_picked_champions' => 'statslol/statslol/most-picked-champions',
                        // csgo计算统计
                        'stats/csgo/players/detail'=>'statscsgo/stats-csgo/player-detail',
                        // 删除es索引
                        'index-del' => 'task/task/index-del',

                        'GET <module>/<controller>' => '<module>/<controller>/index',
                        '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                    ]
                ]
            ]
        ],
    ],
];
