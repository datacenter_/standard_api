<?php
namespace app\modules\log\services;

use app\modules\log\forms\UserBehaviorListForm;
use app\modules\log\models\UserBehavior;
use app\components\Pagination;

/**
 * Class UserBehaviorService
 * @package app\modules\log\services
 */
class UserBehaviorService
{
    /**
     * @param array $attributes
     * @param bool $isDesc
     * @return Pagination
     */
    public static function page(array $attributes = [], $isDesc = true)
    {
        $userBehaviorListForm = new UserBehaviorListForm();
        $userBehaviorListForm->setAttributes($attributes, false);

        $activeQuery = UserBehavior::find();

        if (!empty($userBehaviorListForm->userId)) {
            $activeQuery->andWhere(['user_id' => $userBehaviorListForm->userId]);
        }

        if (!empty($userBehaviorListForm->scheduleId)) {
            $activeQuery->andWhere(['schedule_id' => $userBehaviorListForm->scheduleId]);
        }

        if (!empty($userBehaviorListForm->kolId)) {
            $activeQuery->andWhere(['kol_id' => $userBehaviorListForm->kolId]);
        }

        if (!empty($userBehaviorListForm->startTime)) {
            $activeQuery->andWhere(['>=', 'created_at', $userBehaviorListForm->startTime]);
        }

        if (!empty($userBehaviorListForm->type)) {
            $activeQuery->andWhere(['type' => $userBehaviorListForm->type]);
        }

        if (!empty($userBehaviorListForm->endTime)) {
            $activeQuery->andWhere(['<=', 'created_at', $userBehaviorListForm->endTime]);
        }

        if ($isDesc) {
            $activeQuery->orderBy(['created_at' => SORT_DESC, 'id' => SORT_DESC]);
        } else {
            $activeQuery->orderBy(['created_at' => SORT_ASC, 'id' => SORT_ASC]);
        }

        $pagination = new Pagination(['totalCount' => $activeQuery->count(), 'page' => $userBehaviorListForm->getPage(), 'pageSize' => $userBehaviorListForm->pageSize]);
        $activeQuery->offset($pagination->getOffset())
            ->limit($pagination->getLimit())
            ->select(['id', 'created_at', 'content', 'type', 'type_name']);

        $pagination->activeQuery = $activeQuery;

        return $pagination;
    }
}