<?php

namespace app\modules\log;

/**
 * log module definition class
 * @property \yii\web\user $user
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
    }
}
