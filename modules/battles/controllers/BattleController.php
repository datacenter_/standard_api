<?php

namespace app\modules\battles\controllers;

use app\controllers\RestController;
use app\modules\battles\services\CsgoBattlePlayesService;
use app\modules\common\controllers\CommonController;
use app\modules\common\services\EsService;
use app\modules\match\services\Common;
use Elasticsearch\ClientBuilder;
use app\modules\battles\services\BattleService;
use app\modules\battles\services\CsgoBattleService;

class BattleController extends RestController
{
    public function actionGet(){
        $match_id = $this->pGet('match_id');
        $info = BattleService::getBattleList($match_id);
        return $info;
    }
    public function actionDetail(){
        $battleId = $this->pGet('battle_id');
        $info = BattleService::getBattleDetail($battleId);
        return $info;
    }
    public function actionEvents(){
        $battleId = $this->pGet('battle_id');
        $info = BattleService::getBattleEvents($battleId);
        return $info;
    }
    public function actionAbilities(){
        $battleId = $this->pGet('battle_id');
        $info = BattleService::getBattleAbilities($battleId);
        return $info;
    }
    public function actionItems(){
        $battleId = $this->pGet('battle_id');
        $info = BattleService::getBattleItems($battleId);
        return $info;
    }

    // 5e
    public function actionCsgoGet(){
        $params = $this->pGet();
        $info = CsgoBattleService::getBattleList($params);
        return $info;
    }
    public function actionCsgoPlayersDetail(){
        $params = $this->pGet();
        $info =  CsgoBattlePlayesService::getBattlePlayersList($params);
        return $info;
    }
    public function actionCsgoDetail(){
        $params = $this->pGet();
        $info = CsgoBattleService::getBattleDetail($params);
        return $info;
    }
}
