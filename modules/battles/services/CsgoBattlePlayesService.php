<?php

namespace app\modules\battles\services;

use app\modules\common\services\ApiBase;
use app\modules\common\services\ApiConstant;
use app\modules\common\services\RedisBase;

class CsgoBattlePlayesService
{
    public static function  getBattlePlayersList($params) {
        // 输出格式
        if(isset($params['format'])){
            ApiBase::getFormatType($params['format']);
        }
        // matchId
        $matchId = isset($params['match_id']) ? $params['match_id']:null;
        $match = [
            'match' => [],
            'battle1' => [],
            'battle2' => [],
            'battle3' => [],
        ];
        if(!$matchId){
            return $match;
        }
        // redis取值
        $battlesJson = RedisBase::hashGetAll([ApiConstant::PREFIX_API,'Match',$matchId,'Battles-5E']);
        $battleInfos = [];
        if($battlesJson){
            foreach ($battlesJson as $battleId=>$battleJson){
                if(!is_numeric($battleJson)){
                    $battleInfo = json_decode($battleJson,true);
                    array_unshift($battleInfos, $battleInfo);
                }
            }
            $battleInfosCre = ApiBase::sortArrayByField($battleInfos,'order');
            foreach ($battleInfosCre as $key=>$battle){
                    $battleCount = $key+1;
                    if (isset($battle['match']['stats'])){
                        $data = self::getTransformationData($battle['match']['stats']);
                        $match['match']['teams'] = $data['teams'];
                        $matchPlayers = ApiBase::sortArrayByField($data['players'],'rating',SORT_DESC);
                        $match['match']['players'] = $matchPlayers;
                    }
                    $battleDeta= self::getTransformationData($battleInfo['battle_detail']['teams']);
                    $battlePlayers = ApiBase::sortArrayByField($battleDeta['players'],'rating',SORT_DESC);
                    $match["battle{$battleCount}"]['teams'] =  $battleDeta['teams'];
                    $match["battle{$battleCount}"]['players'] =  $battlePlayers;
            }
        }
        return $match;
    }

    public static function getTransformationData($surrceData) {
        $players = [];
        $teams = [];
        foreach ($surrceData as $statsKey => $statsVal){
            if (!empty($statsVal['players'])){
                foreach ($statsVal['players'] as $playersKey => $playersVal){
                    $player['player_id'] = $playersVal['player']['player_id'];
                    $player['nick_name'] = $playersVal['player']['nick_name'];
                    unset($playersVal['player']);
                    unset($playersVal['adr_total']);
                    unset($playersVal['kast_total']);
                    unset($playersVal['rating_total']);
                    unset($playersVal['total_rounds']);
                    $players[] = array_merge($player,$playersVal);
                }
            }
            unset($statsVal['players']);
            $teams[$statsKey] = $statsVal;
        }
        return [
            'teams' => $teams,
            'players' => $players,
        ];
    }
}