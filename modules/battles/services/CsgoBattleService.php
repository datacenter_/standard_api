<?php

namespace app\modules\battles\services;

use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\ApiBase;
use app\modules\common\services\RedisBase;

class CsgoBattleService
{
    // 5e csgo matchBattles
    public static function getBattleList($params) {
        // 输出格式
        if(isset($params['format'])){
            ApiBase::getFormatType($params['format']);
        }
        // matchId
        $matchId = isset($params['match_id']) ? $params['match_id']:null;
        $battlesInfo['list'] = [];
        $battlesInfo['count'] = 0;
        if(!$matchId){
            return $battlesInfo;
        }
        // redis取值
        $battlesJson = RedisBase::hashGetAll([ApiConstant::PREFIX_API,'Match',$matchId,'Battles-5E']);
        if($battlesJson){
            $battlesInfos = [];
            foreach ($battlesJson as $battleId=>$battleJson){
                if(!is_numeric($battleJson)){
                    $battlesInfos[] = json_decode($battleJson,true);
                }
            }
            $battlesInfo['list'] = ApiBase::sortArrayByField($battlesInfos,'order');
            $battlesInfo['count'] = count($battlesInfo['list']);
        }
        return $battlesInfo;
    }

    // 暂时不用
    public static function getBattleDetail($params) {
        // 输出格式
        if(isset($params['format'])){
            ApiBase::getFormatType($params['format']);
        }
        // battleId
        $battleId = isset($params['battle_id']) ? $params['battle_id']:null;
        if(!$battleId){
            return [];
        }
        $index_info = 'resource_battle_5e_list';
        $conditions = [
            ['field' => 'battle_id', 'type' => '=', 'value' => $battleId],
        ];
        $columns = [];//['match_id', 'description'];
        $order   = array('field' => 'battle_id', 'sort' => 'asc');
        $EsService = EsService::initConnect();
        $res     = $EsService->search($index_info,$conditions, 0, 1, $columns, $order);
        $info = [];
        if(!empty($res['list'])){
            $battleInfo = $res['list'][0];
            // redis取值
            $battleJson = RedisBase::hashGet([ApiConstant::PREFIX_API,'Match',$battleInfo['match_id'],'Battles-5E'],$battleId);
            if($battleJson){
                $info = json_decode($battleJson,true);
            }
        }
        return $info;
    }

}