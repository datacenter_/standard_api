<?php

namespace app\modules\battles\services;

use app\modules\common\services\ApiConstant;
use app\modules\common\services\EsService;
use app\modules\common\services\ApiBase;
use app\modules\common\services\RedisBase;

class BattleService
{
    const BATTLE_ES_INDEX = 'resource_battle_list';
    const MATCH_ES_INDEX = 'resource_matches_list';
    // battle列表
    public static function getBattleList($matchId) {
        $battlesInfo['list'] = [];
        $battlesInfo['count'] = 0;
        if(!$matchId){
            return $battlesInfo;
        }

        // 根据不同套餐取延迟数据
        $redisKey = $isBasic = null;
        if (ApiBase::getSetMealTypeId()) {
            $EsService = EsService::initConnect();
            $matchInfo = $EsService->getFile(self::MATCH_ES_INDEX,$matchId);
            if ($matchInfo) {
                // 验证游戏权限
                ApiBase::verifyTheGame($matchInfo['game_id']);
                // 验证私有赛事
                $sportsResultInfo = ApiBase::dealPrivateTournament([
                    'match_delay' => $matchInfo['delay'],
                    'tournament_id' => $matchInfo['tournament_id'],
                    'is_private' => $matchInfo['is_private'],
                    'is_private_delay' => $matchInfo['is_private_delay'],
                    'private_delay_seconds' => $matchInfo['private_delay_seconds'],
                    'public_delay' => $matchInfo['public_delay'],
                    'public_data_devel' => $matchInfo['public_data_devel'],
                ]);
                // 是否为基础数据
                if (isset($sportsResultInfo['is_basic'])) {
                    $isBasic = $sportsResultInfo['is_basic'];
                }
                // 获取延迟数
                if (isset($sportsResultInfo['match_delay'])) {
                    $currentDelay = $sportsResultInfo['match_delay'];
                } else {
                    $currentDelay = ApiBase::getUserDelaySecond($matchInfo['delay']);
                }
                $redisKey = self::getIsDelayRedisKey($matchInfo['status'],$currentDelay,$matchId,$matchInfo['end_at']);
            }
        } else {
            $redisKey = [ApiConstant::PREFIX_API, 'Match', $matchId, 'Battles'];
        }

        // redis取值
        $battlesJson = RedisBase::hashGetAll($redisKey);
        if($battlesJson){
            $battleInfos = [];
            foreach ($battlesJson as $battleId=>&$battleJson){
                if(!is_numeric($battleJson)){
                    $battleInfo = json_decode($battleJson,true);
                    $gameId = $battleInfo['game']['game_id'];
                    if($battleInfo['is_battle_detailed']) {
                        switch ($gameId) {
                            case ApiBase::GAME_TYPE_CSGO:
                                $rounds = $battleInfo['battle_detail']['rounds_detail'];
                                foreach ($rounds as $key => &$round) {
                                    unset($battleInfo['battle_detail']['rounds_detail'][$key]['round_log']);
                                    // 判断round是否有高级数据权限
                                    if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                        $sides = $round['side'];
                                        foreach ($sides as $sKey=>&$side){
                                            unset($battleInfo['battle_detail']['rounds_detail'][$key]['side'][$sKey]['advanced']);
                                            $roundPlayers = $side['players'];
                                            foreach ($roundPlayers as $pKey=>&$player){
                                                unset($battleInfo['battle_detail']['rounds_detail'][$key]['side'][$sKey]['players'][$pKey]['advanced']);
                                            }
                                        }
                                    }
                                }
                                // 判断是否有高级数据权限
                                if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                    $battleTeams = $battleInfo['battle_detail']['teams'];
                                    foreach ($battleTeams as $key=>&$team){
                                        unset($battleInfo['battle_detail']['teams'][$key]['advanced']);
                                        $battlePlayers = $team['players'];
                                        foreach ($battlePlayers as $pKey=>&$player){
                                            unset($battleInfo['battle_detail']['teams'][$key]['players'][$pKey]['advanced']);
                                        }
                                    }
                                }
                                break;
                            case ApiBase::GAME_TYPE_LOL:
                                unset($battleInfo['battle_detail']['events_timeline']);
                                unset($battleInfo['battle_detail']['team_factions']);
                                // 判断是否有高级数据权限
                                    $battleFactions = $battleInfo['battle_detail']['factions'];
                                    foreach ($battleFactions as $key=>&$faction){
                                        // 判断是否有高级数据权限
                                        if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                            unset($battleInfo['battle_detail']['factions'][$key]['advanced']);
                                        }
                                        $battlePlayers = $faction['players'];
                                        foreach ($battlePlayers as $pKey=>&$player){
                                            // 技能升级时间线
                                            unset($battleInfo['battle_detail']['factions'][$key]['players'][$pKey]['advanced']['abilities_timeline']);
                                            // 道具装备更新时间线
                                            unset($battleInfo['battle_detail']['factions'][$key]['players'][$pKey]['advanced']['items_timeline']);
                                            // 判断是否有高级数据权限
                                            if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                                unset($battleInfo['battle_detail']['factions'][$key]['players'][$pKey]['advanced']);
                                            }
                                        }
                                    }
                                break;
                            case ApiBase::GAME_TYPE_DOTA:
                                unset($battleInfo['battle_detail']['events']);
                                // 判断是否有高级数据权限
                                    $battleFactions = $battleInfo['battle_detail']['factions'];
                                    foreach ($battleFactions as $key=>&$faction) {
                                        // 判断是否有高级数据权限
                                        if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                            unset($battleInfo['battle_detail']['factions'][$key]['advanced']);
                                        }
                                        $battlePlayers = $faction['players'];
                                        foreach ($battlePlayers as $pKey=>&$player) {
                                            // 技能升级时间线
                                            unset($battleInfo['battle_detail']['factions'][$key]['players'][$pKey]['advanced']['abilities_timeline']);
                                            // 道具装备更新时间线
                                            unset($battleInfo['battle_detail']['factions'][$key]['players'][$pKey]['advanced']['items_timeline']);
                                            // 判断是否有高级数据权限
                                            if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                                unset($battleInfo['battle_detail']['factions'][$key]['players'][$pKey]['advanced']);
                                            }
                                        }
                                    }
                                break;
                            default :
                                continue;
                        }
                    }else{
                        $battleInfo['battle_detail'] = null;
                    }
                    $battleInfos[] = $battleInfo;
                }
            }
            $battlesInfo['list'] = ApiBase::sortArrayByField($battleInfos,'order');
            $battlesInfo['count'] = count($battlesInfo['list']);
        }
        return $battlesInfo;
    }
    // 单个battle详情
    public static function getBattleDetail($battleId) {
        if(!$battleId){
            return [];
        }
        $info = [];
        $isBasic = null;
        $EsService = EsService::initConnect();
        $battleInfo = $EsService->getFile(self::BATTLE_ES_INDEX,$battleId);
        if ($battleInfo) {
            $matchId = $battleInfo['match_id'];
            // 根据不同套餐取延迟数据
            if (ApiBase::getSetMealTypeId()) {
                // 暂时取match
                $matchInfo = $EsService->getFile(self::MATCH_ES_INDEX,$matchId);
                // 验证游戏权限
                ApiBase::verifyTheGame($matchInfo['game_id']);
                // 验证私有赛事
                $sportsResultInfo = ApiBase::dealPrivateTournament([
                    'match_delay' => $matchInfo['delay'],
                    'tournament_id' => $matchInfo['tournament_id'],
                    'is_private' => $matchInfo['is_private'],
                    'is_private_delay' => $matchInfo['is_private_delay'],
                    'private_delay_seconds' => $matchInfo['private_delay_seconds'],
                    'public_delay' => $matchInfo['public_delay'],
                    'public_data_devel' => $matchInfo['public_data_devel'],
                ]);
                // 是否为基础数据
                if (isset($sportsResultInfo['is_basic'])) {
                    $isBasic = $sportsResultInfo['is_basic'];
                }
                // 获取延迟数
                if (isset($sportsResultInfo['match_delay'])) {
                    $currentDelay = $sportsResultInfo['match_delay'];
                } else {
                    $currentDelay = ApiBase::getUserDelaySecond($battleInfo['match_delay']);
                }
                $redisKey = self::getIsDelayRedisKey($battleInfo['match_status'],$currentDelay,$matchId,$battleInfo['match_end_at']);
            } else {
                $redisKey = [ApiConstant::PREFIX_API, 'Match', $matchId, 'Battles'];
            }
        } else {
            return [];
        }
        // redis取值
        $battleJson = RedisBase::hashGet($redisKey,$battleId);
        if($battleJson){
            $info = json_decode($battleJson,true);
            $gameId = $info['game']['game_id'];
            if($info['is_battle_detailed']) {
                switch ($gameId) {
                    case ApiBase::GAME_TYPE_CSGO:
                        $rounds = isset($info['battle_detail']['rounds_detail']) ? $info['battle_detail']['rounds_detail'] : [];
                        foreach ($rounds as $key => &$round) {
                            unset($info['battle_detail']['rounds_detail'][$key]['round_log']);
                            // 判断round是否有高级数据权限
                            if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                $sides = $round['side'];
                                foreach ($sides as $sKey=>&$side){
                                    unset($info['battle_detail']['rounds_detail'][$key]['side'][$sKey]['advanced']);
                                    $roundPlayers = $side['players'];
                                    foreach ($roundPlayers as $pKey=>&$player){
                                        unset($info['battle_detail']['rounds_detail'][$key]['side'][$sKey]['players'][$pKey]['advanced']);
                                    }
                                }
                            }
                        }
                        // 判断是否有高级数据权限
                        if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                            $battleTeams = $info['battle_detail']['teams'];
                            foreach ($battleTeams as $key=>&$team){
                                unset($info['battle_detail']['teams'][$key]['advanced']);
                                $battlePlayers = $team['players'];
                                foreach ($battlePlayers as $pKey=>&$player){
                                    unset($info['battle_detail']['teams'][$key]['players'][$pKey]['advanced']);
                                }
                            }
                        }
                        break;
                    case ApiBase::GAME_TYPE_LOL:
                        unset($info['battle_detail']['events_timeline']);
                        unset($info['battle_detail']['team_factions']);
                        // 判断是否有高级数据权限
                            $battleFactions = $info['battle_detail']['factions'];
                            foreach ($battleFactions as $key=>&$faction){
                                // 判断是否有高级数据权限
                                if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                    unset($info['battle_detail']['factions'][$key]['advanced']);
                                }
                                $battlePlayers = $faction['players'];
                                foreach ($battlePlayers as $pKey=>&$player){
                                    // 技能升级时间线
                                    unset($info['battle_detail']['factions'][$key]['players'][$pKey]['advanced']['abilities_timeline']);
                                    // 道具装备更新时间线
                                    unset($info['battle_detail']['factions'][$key]['players'][$pKey]['advanced']['items_timeline']);
                                    // 判断是否有高级数据权限
                                    if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                        unset($info['battle_detail']['factions'][$key]['players'][$pKey]['advanced']);
                                    }
                                }
                            }
                        break;
                    case ApiBase::GAME_TYPE_DOTA:
                        unset($info['battle_detail']['events']);
                        // 判断是否有高级数据权限
                            $battleFactions = $info['battle_detail']['factions'];
                            foreach ($battleFactions as $key=>&$faction) {
                                // 判断是否有高级数据权限
                                if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                    unset($info['battle_detail']['factions'][$key]['advanced']);
                                }
                                $battlePlayers = $faction['players'];
                                foreach ($battlePlayers as $pKey=>&$player) {
                                    // 技能升级时间线
                                    unset($info['battle_detail']['factions'][$key]['players'][$pKey]['advanced']['abilities_timeline']);
                                    // 道具装备更新时间线
                                    unset($info['battle_detail']['factions'][$key]['players'][$pKey]['advanced']['items_timeline']);
                                    // 判断是否有高级数据权限
                                    if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                        unset($info['battle_detail']['factions'][$key]['players'][$pKey]['advanced']);
                                    }
                                }
                            }
                        break;
                    default :
                        continue;
                }
            }else{
                $info['battle_detail'] = null;
            }
        }
        return $info;
    }
    // battle事件
    public static function getBattleEvents($battleId) {
        if(!$battleId){
            return [];
        }
        $info = [];
        $isBasic= null;
        $EsService = EsService::initConnect();
        $battleInfo = $EsService->getFile(self::BATTLE_ES_INDEX,$battleId);
        if ($battleInfo) {
            $matchId = $battleInfo['match_id'];

            // 根据不同套餐取延迟数据
            if (ApiBase::getSetMealTypeId()) {
                // 暂时在这里取 match
                $matchInfo = $EsService->getFile(self::MATCH_ES_INDEX,$matchId);
                // 验证游戏权限
                ApiBase::verifyTheGame($matchInfo['game_id']);
                // 验证私有赛事
                $sportsResultInfo = ApiBase::dealPrivateTournament([
                    'match_delay' => $matchInfo['delay'],
                    'tournament_id' => $matchInfo['tournament_id'],
                    'is_private' => $matchInfo['is_private'],
                    'is_private_delay' => $matchInfo['is_private_delay'],
                    'private_delay_seconds' => $matchInfo['private_delay_seconds'],
                    'public_delay' => $matchInfo['public_delay'],
                    'public_data_devel' => $matchInfo['public_data_devel'],
                ]);
                // 是否为基础数据
                if (isset($sportsResultInfo['is_basic'])) {
                    $isBasic = $sportsResultInfo['is_basic'];
                }
                // 获取延迟数
                if (isset($sportsResultInfo['match_delay'])) {
                    $currentDelay = $sportsResultInfo['match_delay'];
                } else {
                    $currentDelay = ApiBase::getUserDelaySecond($battleInfo['match_delay']);
                }
                $redisKey = self::getIsDelayRedisKey($battleInfo['match_status'],$currentDelay,$matchId,$battleInfo['match_end_at']);
            } else {
                $redisKey = [ApiConstant::PREFIX_API, 'Match', $matchId, 'Battles'];
            }

            // redis取值
            $battleJson = RedisBase::hashGet($redisKey,$battleId);
            if($battleJson){
                $info = json_decode($battleJson,true);
                $gameId = $info['game']['game_id'];
                if($info['is_battle_detailed']) {
                    switch ($gameId) {
                        case ApiBase::GAME_TYPE_CSGO:
                            $teams = [];
                            foreach ($info['teams'] as $team) {
                                $teams[$team['team_id']] = $team['opponent_order'];
                            }
                            $info['starting_sides'] = [];
                            foreach ($info['battle_detail']['teams'] as $key => $side) {
                                $info['starting_sides'][$key]['starting_side'] = $side['starting_side'];
                                $info['starting_sides'][$key]['team_id'] = $side['team_id'];
                                $info['starting_sides'][$key]['opponent_order'] = $teams[$side['team_id']];
                            }
                            $rounds = isset($info['battle_detail']['rounds_detail']) ? $info['battle_detail']['rounds_detail'] : [];
                            foreach ($rounds as $key => $round) {
                                $sides = $round['side'];
                                $rounds[$key]['side'] = [];
                                foreach ($sides as $k => $side) {
                                    $sideInfo['side'] = $side['side'];
                                    $sideInfo['team_id'] = $side['team_id'];
                                    $rounds[$key]['side'][$k] = $sideInfo;
                                }
                                unset($rounds[$key]['special_events']);
                            }
                            $info['rounds_detail'] = $rounds;
                            break;
                        case ApiBase::GAME_TYPE_LOL:
                            $info['factions'] = $info['battle_detail']['team_factions'];
                            $lolEventsTimeline = $info['battle_detail']['events_timeline'];
                            // 判断是否有高级数据权限
                            if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                $lolBasicEventsTimeline = [];
                                foreach ($lolEventsTimeline as $event) {
                                    if (in_array($event['event_type'],['battle_start','battle_end','player_kill','player_suicide','building_kill','elite_kill'])) {
                                        $lolBasicEventsTimeline[] = $event;
                                    }
                                }
                                $info['events_timeline'] = $lolBasicEventsTimeline;
                            } else {
                                $info['events_timeline'] = $lolEventsTimeline;
                            }
                            break;
                        case ApiBase::GAME_TYPE_DOTA:
                            $events = $info['battle_detail']['events'];
                            unset($info['battle_detail']);
                            $info['factions'] = $events['factions'];
                            $dotaEventsTimeline = $events['events_timeline'];
                            // 判断是否有高级数据权限
                            if (ApiBase::isBasicsResult($gameId,$isBasic)) {
                                $dotaBasicEventsTimeline = [];
                                foreach ($dotaEventsTimeline as $k => $event) {
                                    if (in_array($event['event_type'],['battle_start','battle_end','player_kill','player_suicide','player_denied','building_kill','building_denied','building_captured','elite_kill'])) {
                                        $dotaBasicEventsTimeline[] = $event;
                                    }
                                }
                                $info['events_timeline'] = $dotaBasicEventsTimeline;
                            } else {
                                $info['events_timeline'] = $dotaEventsTimeline;
                            }
                            break;
                        default :
                            continue;
                    }
                }else {
                    switch ($gameId) {
                        case ApiBase::GAME_TYPE_CSGO:
                            $info['starting_sides'] = [];
                            $info['rounds_detail'] = [];
                            break;
                        case ApiBase::GAME_TYPE_LOL:
                        case ApiBase::GAME_TYPE_DOTA:
                            $info['factions'] = [];
                            $info['events_timeline'] = [];
                            break;
                        default :
                            continue;
                    }
                }
                unset($info['battle_detail']);
                $modified_at = $info['modified_at'];
                $created_at = $info['created_at'];
                $deleted_at = $info['deleted_at'];
                unset($info['modified_at']);
                unset($info['created_at']);
                unset($info['deleted_at']);
                $info['modified_at'] = $modified_at;
                $info['created_at'] = $created_at;
                $info['deleted_at'] = $deleted_at;
            }
        }
        return $info;
    }
    // 技能升级时间线
    public static function getBattleAbilities($battleId) {
        if(!$battleId){
            return [];
        }
        $info = [];
        $isBasic = null;
        $EsService = EsService::initConnect();
        $battleInfo = $EsService->getFile(self::BATTLE_ES_INDEX,$battleId);
        if ($battleInfo) {
            $matchId = $battleInfo['match_id'];

            // 根据不同套餐取延迟数据
            if (ApiBase::getSetMealTypeId()) {
                // 暂时在这里取 match
                $matchInfo = $EsService->getFile(self::MATCH_ES_INDEX,$matchId);
                // 验证游戏权限
                ApiBase::verifyTheGame($matchInfo['game_id']);
                // 验证私有赛事
                $sportsResultInfo = ApiBase::dealPrivateTournament([
                    'match_delay' => $matchInfo['delay'],
                    'tournament_id' => $matchInfo['tournament_id'],
                    'is_private' => $matchInfo['is_private'],
                    'is_private_delay' => $matchInfo['is_private_delay'],
                    'private_delay_seconds' => $matchInfo['private_delay_seconds'],
                    'public_delay' => $matchInfo['public_delay'],
                    'public_data_devel' => $matchInfo['public_data_devel'],
                ]);
                // 是否为基础数据
                if (isset($sportsResultInfo['is_basic'])) {
                    $isBasic = $sportsResultInfo['is_basic'];
                }
                // 获取延迟数
                if (isset($sportsResultInfo['match_delay'])) {
                    $currentDelay = $sportsResultInfo['match_delay'];
                } else {
                    $currentDelay = ApiBase::getUserDelaySecond($battleInfo['match_delay']);
                }
                $redisKey = self::getIsDelayRedisKey($battleInfo['match_status'],$currentDelay,$matchId,$battleInfo['match_end_at']);
            } else {
                $redisKey = [ApiConstant::PREFIX_API, 'Match', $matchId, 'Battles'];
            }

            // redis取值
            $battleJson = RedisBase::hashGet($redisKey,$battleId);
            if($battleJson){
                $info = json_decode($battleJson,true);
                $gameId = $info['game']['game_id'];
                if ($info['is_battle_detailed']) {
                    switch ($gameId) {
                        case ApiBase::GAME_TYPE_LOL:
                            $battleFactions = $info['battle_detail']['factions'];
                            $info['factions'] = [];
                            foreach ($battleFactions as $key=>&$faction) {
                                $battlePlayersArray = [];
                                // battle选手
                                $battlePlayers = $faction['players'];
                                foreach ($battlePlayers as $pKey=>&$player) {
                                    $currentBattlePlayersArray['seed'] = $player['seed'];
                                    $currentBattlePlayersArray['faction'] = $player['faction'];
                                    $currentBattlePlayersArray['role'] = $player['role'];
                                    $currentBattlePlayersArray['lane'] = $player['lane'];
                                    $currentBattlePlayersArray['player'] = $player['player'];
                                    $currentBattlePlayersArray['champion'] = $player['champion'];
                                    // 判断是否有高级数据权限
                                    if (! ApiBase::isBasicsResult($gameId,$isBasic)) {
                                        $currentBattlePlayersArray['advanced']['abilities_timeline'] = $player['advanced']['abilities_timeline'];
                                    }
                                    $battlePlayersArray[] = $currentBattlePlayersArray;
                                }

                                $info['factions'][] = [
                                    'faction' => $faction['faction'],
                                    'team_id' => $faction['team_id'],
                                    'players' => $battlePlayersArray,
                                ];
                            }
                            break;
                        case ApiBase::GAME_TYPE_DOTA:
                            unset($info['battle_detail']['events']);
                            $battleFactions = $info['battle_detail']['factions'];
                            $info['factions'] = [];
                            foreach ($battleFactions as $key=>&$faction) {
                                $battlePlayersArray = [];
                                // battle选手
                                $battlePlayers = $faction['players'];
                                foreach ($battlePlayers as $pKey=>&$player) {
                                    $currentBattlePlayersArray['seed'] = $player['seed'];
                                    $currentBattlePlayersArray['faction'] = $player['faction'];
                                    $currentBattlePlayersArray['role'] = $player['role'];
                                    $currentBattlePlayersArray['lane'] = $player['lane'];
                                    $currentBattlePlayersArray['player'] = $player['player'];
                                    $currentBattlePlayersArray['hero'] = $player['hero'];
                                    // 判断是否有高级数据权限
                                    if (! ApiBase::isBasicsResult($gameId,$isBasic)) {
                                        $currentBattlePlayersArray['advanced']['abilities_timeline'] = $player['advanced']['abilities_timeline'];
                                    }
                                    $battlePlayersArray[] = $currentBattlePlayersArray;
                                }

                                $info['factions'][] = [
                                    'faction' => $faction['faction'],
                                    'team_id' => $faction['team_id'],
                                    'players' => $battlePlayersArray,
                                ];
                            }
                            break;
                        default :
                            return [];
                    }
                } else {
                    switch ($gameId) {
                        case ApiBase::GAME_TYPE_LOL:
                        case ApiBase::GAME_TYPE_DOTA:
                            $info['factions'] = [];
                            break;
                        default :
                            return [];
                    }
                }
                unset($info['battle_detail']);
                $modified_at = $info['modified_at'];
                $created_at = $info['created_at'];
                $deleted_at = $info['deleted_at'];
                unset($info['modified_at']);
                unset($info['created_at']);
                unset($info['deleted_at']);
                $info['modified_at'] = $modified_at;
                $info['created_at'] = $created_at;
                $info['deleted_at'] = $deleted_at;
            }
        }
        return $info;
    }
    // 道具装备更新时间线
    public static function getBattleItems($battleId) {
        if(!$battleId){
            return [];
        }
        $info = [];
        $isBasic = null;
        $EsService = EsService::initConnect();
        $battleInfo = $EsService->getFile(self::BATTLE_ES_INDEX,$battleId);
        if ($battleInfo) {
            $matchId = $battleInfo['match_id'];

            // 根据不同套餐取延迟数据
            if (ApiBase::getSetMealTypeId()) {
                // 暂时在这里取 match
                $matchInfo = $EsService->getFile(self::MATCH_ES_INDEX, $matchId);
                // 验证游戏权限
                ApiBase::verifyTheGame($matchInfo['game_id']);
                // 验证私有赛事
                $sportsResultInfo = ApiBase::dealPrivateTournament([
                    'match_delay' => $matchInfo['delay'],
                    'tournament_id' => $matchInfo['tournament_id'],
                    'is_private' => $matchInfo['is_private'],
                    'is_private_delay' => $matchInfo['is_private_delay'],
                    'private_delay_seconds' => $matchInfo['private_delay_seconds'],
                    'public_delay' => $matchInfo['public_delay'],
                    'public_data_devel' => $matchInfo['public_data_devel'],
                ]);
                // 是否为基础数据
                if (isset($sportsResultInfo['is_basic'])) {
                    $isBasic = $sportsResultInfo['is_basic'];
                }
                // 获取延迟数
                if (isset($sportsResultInfo['match_delay'])) {
                    $currentDelay = $sportsResultInfo['match_delay'];
                } else {
                    $currentDelay = ApiBase::getUserDelaySecond($battleInfo['match_delay']);
                }
                $redisKey = self::getIsDelayRedisKey($battleInfo['match_status'], $currentDelay, $matchId, $battleInfo['match_end_at']);
            } else {
                $redisKey = [ApiConstant::PREFIX_API, 'Match', $matchId, 'Battles'];
            }

            // redis取值
            $battleJson = RedisBase::hashGet($redisKey, $battleId);
            if ($battleJson) {
                $info = json_decode($battleJson, true);
                $gameId = $info['game']['game_id'];
                if ($info['is_battle_detailed']) {
                    switch ($gameId) {
                        case ApiBase::GAME_TYPE_LOL:
                            $battleFactions = $info['battle_detail']['factions'];
                            $info['factions'] = [];
                            foreach ($battleFactions as $key => &$faction) {
                                $battlePlayersArray = [];
                                // battle选手
                                $battlePlayers = $faction['players'];
                                foreach ($battlePlayers as $pKey => &$player) {
                                    $currentBattlePlayersArray['seed'] = $player['seed'];
                                    $currentBattlePlayersArray['faction'] = $player['faction'];
                                    $currentBattlePlayersArray['role'] = $player['role'];
                                    $currentBattlePlayersArray['lane'] = $player['lane'];
                                    $currentBattlePlayersArray['player'] = $player['player'];
                                    $currentBattlePlayersArray['champion'] = $player['champion'];
                                    // 判断是否有高级数据权限
                                    if (! ApiBase::isBasicsResult($gameId,$isBasic)) {
                                        $currentBattlePlayersArray['advanced']['items_timeline'] = $player['advanced']['items_timeline'];
                                    }
                                    $battlePlayersArray[] = $currentBattlePlayersArray;
                                }

                                $info['factions'][] = [
                                    'faction' => $faction['faction'],
                                    'team_id' => $faction['team_id'],
                                    'players' => $battlePlayersArray,
                                ];
                            }
                            break;
                        case ApiBase::GAME_TYPE_DOTA:
                            unset($info['battle_detail']['events']);
                            $battleFactions = $info['battle_detail']['factions'];
                            $info['factions'] = [];
                            foreach ($battleFactions as $key => &$faction) {
                                $battlePlayersArray = [];
                                // battle选手
                                $battlePlayers = $faction['players'];
                                foreach ($battlePlayers as $pKey => &$player) {
                                    $currentBattlePlayersArray['seed'] = $player['seed'];
                                    $currentBattlePlayersArray['faction'] = $player['faction'];
                                    $currentBattlePlayersArray['role'] = $player['role'];
                                    $currentBattlePlayersArray['lane'] = $player['lane'];
                                    $currentBattlePlayersArray['player'] = $player['player'];
                                    $currentBattlePlayersArray['hero'] = $player['hero'];
                                    // 判断是否有高级数据权限
                                    if (! ApiBase::isBasicsResult($gameId,$isBasic)) {
                                        $currentBattlePlayersArray['advanced']['items_timeline'] = $player['advanced']['items_timeline'];
                                    }
                                    $battlePlayersArray[] = $currentBattlePlayersArray;
                                }

                                $info['factions'][] = [
                                    'faction' => $faction['faction'],
                                    'team_id' => $faction['team_id'],
                                    'players' => $battlePlayersArray,
                                ];
                            }
                            break;
                        default :
                            return [];
                    }
                } else {
                    switch ($gameId) {
                        case ApiBase::GAME_TYPE_LOL:
                        case ApiBase::GAME_TYPE_DOTA:
                            $info['factions'] = [];
                            break;
                        default :
                            return [];
                    }
                }
                unset($info['battle_detail']);
                $modified_at = $info['modified_at'];
                $created_at = $info['created_at'];
                $deleted_at = $info['deleted_at'];
                unset($info['modified_at']);
                unset($info['created_at']);
                unset($info['deleted_at']);
                $info['modified_at'] = $modified_at;
                $info['created_at'] = $created_at;
                $info['deleted_at'] = $deleted_at;
            }
        }
        return $info;
    }
    // 处理延时
    public static function getIsDelayRedisKey($match_status,$delayTime,$match_id=null,$match_end_at=null) {
        if($delayTime != 0) {
            // 获取match状态和结束时间
            if ($match_status == 'completed') {
                $nowTime = time();
                $matchDelayEndAt = strtotime($match_end_at) + $delayTime;
                if ($nowTime >= $matchDelayEndAt) {
                    $redisKey = [ApiConstant::PREFIX_API, 'Match', $match_id, 'Battles'];
                } else {
                    $redisKey = ApiBase::getDelayDate($match_id, $delayTime);
                }
            } else if ($match_status == 'ongoing') {
                $redisKey = ApiBase::getDelayDate($match_id, $delayTime);
            } else {
                $redisKey = [ApiConstant::PREFIX_API, 'Match', $match_id, 'Battles'];
            }
        }else{
            $redisKey = [ApiConstant::PREFIX_API, 'Match', $match_id, 'Battles'];
        }
        return $redisKey;
    }
}
