<?php

namespace app\controllers;

use app\modules\common\services\AuthorityDetection;
use app\rest\exceptions\UnauthorizedException;
use yii\rest\Controller;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\web\Response;

/**
 * Class RestController
 * @package app\controllers
 */
class RestController extends Controller
{
    public function beforeAction($action)
    {
        // log - 开始时间
        \Yii::$app->params['log_begin_at'] = msectime();
        // token
        $token = \Yii::$app->request->get('token');
        // 打开检测token
        if(!$token){
            // 未经授权
            throw new UnauthorizedException();
        }
        // 旧版权限
//        AuthorityDetection::authorityProv($token);
        // 新版权限
        AuthorityDetection::authorityProvPro($token);
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'corsFilter' => [
                'class' => Cors::class,
                'cors' => [
                    'Access-Control-Requ est-Method' => ['*'],
                    'Access-Control-Request-Headers'=> ['*'],
                    'Access-Control-Allow-Origin' => ['*'],
                    'Access-Control-Max-Age' => 86400,
                    'Access-Control-Expose-Headers' => [],
                    'Access-Control-Allow-Headers' => ['*']
                ],
            ],
        ];
    }

    public function accessRules()
    {
        return [
            'allow' => true
        ];
    }

    public function pGet($key=null,$val=null)
    {
        if(!$key){
            return \Yii::$app->request->get();
        }else{
            return \Yii::$app->request->get($key,$val);
        }
    }

    public function pPost($key=null,$val=null)
    {
        if(!$key){
            return \Yii::$app->request->post();
        }else{
            return \Yii::$app->request->post($key,$val);
        }
    }
}
